const axios = require('axios')

const appModel = require('../app/appModel')
const misc = require('../lib/misc')

const bookSearch = require('../app/bookSearch/bookSearch')
const languages = require('../app/textAnalysis/languages')
const bookItems = require('../app/textAnalysis/bookItems')
const authors = require('../app/textAnalysis/authors')
const scanners = require('../app/textAnalysis/scanners')
const amazonBookSearch = require('../app/bookSearch/bookSearch_amazon')

isbnNotFoundError = { isExternalError: false, message: "No se encontró información para el ISBN indicado" }
externalError = { isExternalError: true, message: "Error en la búsqueda por ISBN" }




// 9788484893868 9788448931223 9788807016738 9788389291677
// 9780573605161 9788420636269 9781409592778 9786077451730
// 8408160109 9788414002162
// 9788420655475 9788420602868 9789505111831 9789500437608
// 9789505111268 9788420479774 9788420423920 9789507318627
// 9789500752442 9789500427197 9788432086175 
// 9788420633114 9788499089515 0307950948 9789500738651
// 9780140170986 9781447289593 9788420426464 9788433968678 9788433973184 0312429215

// not found in WorldCat
// 9789870713593
// invalid
// 9789871263195

// 9789871772186 9789507318252 9789505470679 9789500427197 9789875780811 9782070389049 9789876298216
// amazonBookSearch.amazonFetchBookInfo("9789870713593")
//     .then(bookInfo => { console.log(bookInfo) })
//     .catch(error => { console.log(error) })


bookSearch.fetchBookInfoByIsbnFromHtmlPage("9789871263195")
    .then(bookInfo => { console.log(bookInfo) })
    .catch(error => { console.log(error) })


// const rawText = "fdfafa 34pq fdfafa"
// const analyzer = new bookItems.HtmlPageCountAnalyzer(rawText)
// console.log(rawText)
// console.log(analyzer.pageCount())
