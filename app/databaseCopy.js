const appModel = require('./appModel')
const firebase = require("firebase");
const database = firebase.database();

function copyEntities(entities, newDatabaseDomain, entityFirebaseReference, entityPrettyName, dataTransformer) {
    let entitiesJson = {}
    entities.forEach(entity => {
        const id = entity.persistentId()
        let data = entity.persistenceJSON()
        // transform data
        data = dataTransformer(data, entity)
        // clean undefined fields that are not liked by Firebase
        Object.keys(data).forEach(key => { if (data[key] == undefined) { delete data[key] } })
        entitiesJson[id] = data
    })  
    return database.ref(newDatabaseDomain + '/' + entityFirebaseReference).set(entitiesJson).then(() => {
        console.log(entityPrettyName + ' guardados en ' + newDatabaseDomain)
    })
}

function copyNamedEntityStore(store, newDatabaseDomain) {
    const dataIdentity = (data, entity) => data;
    return copyEntities(store.entities(), newDatabaseDomain, store.entityName(), store.capitalizedEntityName(), dataIdentity)
}

function copyBooks(newDatabaseDomain) {
    const bookTransformer = function(bookJson, book) {
        bookJson["authors"] = book.rawAuthors()
        bookJson["languages"] = book.rawLanguages()
        bookJson["originalLanguages"] = book.rawOriginalLanguages()
        return bookJson
    }
    return copyEntities(appModel.bookStore.books(), newDatabaseDomain, 'books', 'Books', bookTransformer)
}

function copyLastlyEditedBookId(newDatabaseDomain) {
    return appModel.bookStore.fetchLastlyEditedBookId().then(function(value) {
        database.ref(newDatabaseDomain + '/lastlyEditedBookId').set(value)
    })
}

module.exports.copyNamedEntityStore = copyNamedEntityStore
module.exports.copyBooks = copyBooks
module.exports.copyLastlyEditedBookId = copyLastlyEditedBookId
