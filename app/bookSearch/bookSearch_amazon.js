const axios = require('axios')
const _ = require('lodash')

const appModel = require('../appModel')
const misc = require('../../lib/misc')

const scanners = require('../textAnalysis/scanners')
const languages = require('../textAnalysis/languages')
const authors = require('../textAnalysis/authors')
const bookSearchLib = require('./bookSearch_lib')
const c = require('config')


// https://www.amazon.com/s/field-keywords=9789505470679  -- dejó de andar al 2020.11.01

// https://www.amazon.com/s?k=9788425433986
function amazonFetchBookInfo(isbn) {
    const domain = "https://www.amazon.com"
    const url = domain + "/s?k=" + isbn
    return axios.get(url)
        .then(response => {
            const wholePage = response.data
            const secondPageSignal = '<a class="a-link-normal s-no-outline" href="'
            let secondPageUrlBeginPos = wholePage.indexOf(secondPageSignal)
            if (secondPageUrlBeginPos < 0) {
                return Promise.reject(bookSearchLib.isbnNotFoundError)
            }
            secondPageUrlBeginPos += secondPageSignal.length
            const secondPageUrlEndPos = wholePage.indexOf('"', secondPageUrlBeginPos)
            if (secondPageUrlEndPos < 0) {
                return Promise.reject(bookSearchLib.isbnNotFoundError)
            }
            const secondPageUrl = wholePage.substring(secondPageUrlBeginPos, secondPageUrlEndPos)
            if (secondPageUrl.indexOf("nb_sb_noss") >= 0) {
                return Promise.reject(bookSearchLib.isbnNotFoundError)
            }
            return axios.get(domain + secondPageUrl)
        })
        .then(response => {
            const analyzer = new AmazonBookInfoAnalyzer(response.data)
            try {
                const info = analyzer.bookInfo()
                return info ? Promise.resolve(info) : Promise.reject(analyzer.pageError())
            } catch (err) {
                return Promise.reject(Object.assign({ sourceError: err }, bookSearchLib.externalError))
            }
        })
        .catch( err => Promise.reject(bookSearchLib.finalError(err)) )
}


class AmazonBookInfoAnalyzer {
    constructor(bookInfoPage) {
        this._bookInfoPage = bookInfoPage
        this._info = null; this._error = null
    }

    bookInfo() {
        this.doAnalysis()
        return this._info
    }

    pageError() {
        this.doAnalysis()
        return this._error
    }

    doAnalysis() {
        if (this._info || this._error) { return }
        const pageScanner = new scanners.HtmlPageScanner(this._bookInfoPage)

        let rawTitle = pageScanner.textInsideTagWithParams("span", 'id="productTitle"')
        if (rawTitle == null) {
            rawTitle = pageScanner.textInsideTagWithParams("span", 'id="ebooksProductTitle"')
        }

        const rawAuthors = []
        let moreAuthors = null
        while (moreAuthors = pageScanner.textInsideTagWithParams("span", 'class="author notFaded"')) {
            rawAuthors.push(moreAuthors)
        }

        // jump to product details zone
        let rawPublisherAndYear = null
        let rawLanguagesText = null
        let rawPageCountText = null
        let rawJustYearText = null
        pageScanner.recordNewOffset(pageScanner.followingExactTextPos("detailBullets_feature_div"))
        const limit = pageScanner.exactTextPos("</div>")
        if (limit) {
            while (pageScanner._offset && pageScanner._offset < limit) {
                pageScanner.recordNewOffset(pageScanner.followingExactTextPos('<span class="a-text-bold">'))
                if (pageScanner.isComing("Publisher") || pageScanner.isComing("Editor")) {
                    pageScanner.recordNewOffset(pageScanner.followingExactTextPos('<span>'))
                    rawPublisherAndYear = pageScanner.textUpTo("</span>")            
                } else if (pageScanner.isComing("Language") || pageScanner.isComing("Idioma")) {
                    pageScanner.recordNewOffset(pageScanner.followingExactTextPos('<span>'))
                    rawLanguagesText = pageScanner.textUpTo("</span>")            
                } else if (pageScanner.isComing("Print length") || pageScanner.isComing("Número de páginas") || pageScanner.isComing("Paperback")) {
                    pageScanner.recordNewOffset(pageScanner.followingExactTextPos('<span>'))
                    rawPageCountText = pageScanner.textUpTo("</span>")            
                } else if (pageScanner.isComing("Publication date") || pageScanner.isComing("Fecha de publicación")) {
                    pageScanner.recordNewOffset(pageScanner.followingExactTextPos('<span>'))
                    rawJustYearText = pageScanner.textUpTo("</span>")            
                }
            }
        }
                    
        let publisherAndYear = this.getPublisherAndYear(rawPublisherAndYear)
        let justYear = this.getYear(rawJustYearText);
        let theLanguages = this.getLanguages(rawLanguagesText);
        let pageCount = this.getPageCount(rawPageCountText);
        let theTitle = this.getTitle(rawTitle)
        this._info = {
            authors: this.getAuthors(rawAuthors),
        }
        if (theLanguages) { this._info.languages = theLanguages }
        if (theTitle != null) { this._info.title = theTitle }
        if (publisherAndYear && publisherAndYear.publisher) { this._info.publisher = publisherAndYear.publisher }
        if (justYear) {
            this._info.year = publisherAndYear.year
        } else if (publisherAndYear && publisherAndYear.year) { 
            this._info.year = publisherAndYear.year 
        }
        if (pageCount) { this._info.pageCount = pageCount }
    }

    getTitle(rawTitle) {
        if (rawTitle == null) { return null }

        const wordMatchesLanguage = word => languages.languageDefinitions.some(lang => lang.matches(word))
        let theTitle = rawTitle.trim()
        if (theTitle[theTitle.length - 1] == ")") {
            const openParPos = theTitle.lastIndexOf("(")
            if (openParPos >= 0) {
                const wordsBetweenPar = theTitle.substring(openParPos + 1, theTitle.length - 1).split(" ")
                if (wordsBetweenPar.some(wordMatchesLanguage)) {
                    theTitle = theTitle.substring(0, openParPos)
                }
            }
        }
        return misc.htmlStringToStandardString(theTitle).trim()
    }

    getAuthors(rawAuthors) {
        const rawAuthorList = rawAuthors.reduce((list, author) => {
            const thisAuthorText = this.getRawAuthorText(author)
            return (thisAuthorText == null)
                ? list
                : list.concat(new authors.RawAuthorAnalyzer(thisAuthorText).rawAuthorNames())
        }, [])

        const authorNameMatcher = new authors.AuthorNameMatcher(
            appModel.authorStore.entities().map(author => author.name())
        )
        return rawAuthorList.map(
            authorName => authorNameMatcher.bestMatchFor(authorName)
        )
    }

    getRawAuthorText(rawAuthors) {
        const authorScanner = new scanners.HtmlPageScanner(rawAuthors)
        const bigAuthorRef = authorScanner.textInsideTagWithParams("span", 'class="a-size-medium"')
        // console.log(bigAuthorRef)
        if (bigAuthorRef) {
            const nextOpeningTag = bigAuthorRef.indexOf("<")
            return (nextOpeningTag >= 0) ? bigAuthorRef.substring(0, nextOpeningTag) : bigAuthorRef
        }
        // console.log(authorScanner.textInsideTagWithParams("a", 'class="a-link-normal"'))
        return authorScanner.textInsideTagWithParams("a", 'class="a-link-normal"')
    }

    getPublisherAndYear(rawText) {
        if (!rawText || rawText.length === 0) {
            return null;
        }
        const textScanner = new scanners.TextScanner(rawText)

        const endPublisherPos = textScanner.jumpToAnyOf(";(")
        const thePublisher = textScanner.textUpTo(endPublisherPos).trim()
        textScanner.advanceToPosition(endPublisherPos)

        textScanner.advanceToPosition(textScanner.jumpToAnyOf("("))
        textScanner.advanceToPosition(textScanner.jumpSeparators())

        const yearText = textScanner.textUpToEnd();
        const theYear = this.getYear(yearText);

        return { publisher: thePublisher, year: theYear }
    }

    getYear(rawText) {
        if (!rawText || rawText.length === 0) {
            return null;
        }
        const textScanner = new scanners.TextScanner(rawText)

        while (!textScanner.isAtEnd() && !textScanner.isOnYearNumber()) {
            textScanner.advanceToNextWord()
            textScanner.advanceToPosition(textScanner.jumpSeparators())
        }
        return textScanner.isAtEnd() ? null : Number(textScanner.word().trim())
    }

    getLanguages(rawText) {
        if (!rawText || rawText.length === 0) {
            return null;
        }
        const rawLanguages = rawText.split(",").map(lang => lang.trim())
        return rawLanguages
            .map(lang => languages.languageMatching(lang))
            .filter(lang => lang)
            .map(lang => lang.longSpa())
    }

    getPageCount(rawText) {
        if (!rawText || rawText.length === 0) {
            return null;
        }
        const textScanner = new scanners.TextScanner(rawText)

        return (!textScanner.isAtEnd() && textScanner.isOnNumber(2)) ? Number(textScanner.word().trim()) : null
    }

}


module.exports.amazonFetchBookInfo = amazonFetchBookInfo