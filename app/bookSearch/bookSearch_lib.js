isbnNotFoundError = { isExternalError: false, message: "No se encontró información para el ISBN indicado" }
externalError = { isExternalError: true, message: "Error en la búsqueda por ISBN" }

function finalError(err) {
    return ((err.isExternalError != false) && (!err.sourceError)) 
        ? Object.assign({ sourceError: err }, externalError)
        : err
}

module.exports.isbnNotFoundError = isbnNotFoundError
module.exports.externalError = externalError
module.exports.finalError = finalError

