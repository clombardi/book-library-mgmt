const axios = require('axios')
const _ = require('lodash')

const appModel = require('../appModel')
const scanners = require('../textAnalysis/scanners')
const languages = require('../textAnalysis/languages')
const authorAnalysis = require('../textAnalysis/authors')
const bookItems = require('../textAnalysis/bookItems')
const bookSearchLib = require('./bookSearch_lib')
const amazonBookSearch = require('./bookSearch_amazon')


/**
 * Fetch book from external server WorldCat
 * 
 * Returns a Promise that resolves on book data including:
 *      title / authors / publisher / year / language / isTranslation / originalLanguage
 */
function fetchBookInfoByIsbn(isbn) {
    // http://xisbn.worldcat.org/webservices/xid/isbn/9789875665552?method=getEditions&format=json&fl=*&ai=clombardi
    const url = "http://xisbn.worldcat.org/webservices/xid/isbn/" + isbn
    return axios.get(url, {
        params: {
            method: "getEditions", format: "json", fl: "*", ai: "clombardi"
        }
    })
        .then(response => {
            if (response.data.stat == "ok") {
                const bookData = response.data.list[0]
                console.log(bookData)
                // título, autor(es), editorial, idioma, esTraduccion, idiomaOriginal, año
                const processedBookData = {
                    title: bookData.title,
                    publisher: bookData.publisher,
                    year: Number(bookData.year)
                }

                // authors
                const rawAuthorNames = new authorAnalysis.RawAuthorAnalyzer(bookData.author).rawAuthorNames()
                const matcher = new authorAnalysis.AuthorNameMatcher(
                    appModel.authorStore.entities().map(author => author.name())
                )
                processedBookData.authors = rawAuthorNames.map(rawName => matcher.bestMatchFor(rawName))

                // languages
                const theLang = longLanguageName(bookData.lang)
                const theOriginalLang = bookData.originalLang ? languages.longLanguageName(bookData.originalLang) : null
                processedBookData.language = theLang
                if (bookData.originalLang && (theLang != theOriginalLang)) {
                    processedBookData.isTranslation = true
                    processedBookData.originalLanguage = theOriginalLang
                } else {
                    processedBookData.isTranslation = false
                }
                return Promise.resolve(processedBookData)
            } else {
                console.log('no encontro el libro')
                return Promise.resolve(bookSearchLib.isbnNotFoundError)
            }
        })
        .catch(error => {
            console.log(error)
            return Promise.reject(bookSearchLib.externalError) 
        })
        .then(result => {
            if (result.isExternalError == null) {
                return Promise.resolve(result)
            } else {
                return Promise.reject(result)
            }
        })
}


function fetchBookInfoByIsbnFromHtmlPage(isbn) {
    const fetchBookFromWorldcat = fetchBookInfoByIsbnFromWorldcatHtmlPage(isbn).catch(err => Promise.resolve({ theError: err }))
    const fetchBookFromAmazon = amazonBookSearch.amazonFetchBookInfo(isbn).catch(err => Promise.resolve({ theError: err }))
    return Promise.all([fetchBookFromWorldcat, fetchBookFromAmazon]).then(results => {
        const worldcatResult = results[0];
        const amazonResult = results[1];
        if (worldcatResult.theError && amazonResult.theError) {
            return Promise.reject(amazonResult.theError)
        } else if (worldcatResult.theError) {
            return Promise.resolve(amazonResult)
        } else if (amazonResult.theError) {
            return Promise.resolve(worldcatResult)
        } else {
            // both services obtained results
            const result = worldcatResult;
            ["title", "authors", "publisher", "year", "languages", "pageCount"].forEach(field => {
                if (worldcatResult[field] == null && amazonResult[field] != null) {
                    result[field] = amazonResult[field];
                }
            });
            return result;
        }
    })
}


/**
 * Fetch book from HTML page from WorldCat 
 * 
 * Returns a Promise that resolves on book data including:
 *      title / authors / publisher / year / language / pageCount
 */
function fetchBookInfoByIsbnFromWorldcatHtmlPage(isbn) {
    return fetchBookPage(isbn)
        .then(bookInfoPage => {
            const analyzer = new BookInfoAnalyzer(bookInfoPage)
            try {
                const info = analyzer.bookInfo()
                return info ? Promise.resolve(info) : Promise.reject(analyzer.pageError())
            } catch (err) {
                return Promise.reject(Object.assign({ sourceError: err }, bookSearchLib.externalError))
            }
        })
}



function fetchBookPage(isbn) {
    const domain = "http://www.worldcat.org"
    const languageIndicator = "lang=en"
    const url = domain + "/isbn/" + isbn + "&referer=brief_results&" + languageIndicator
    return axios.get(url).then(response => Promise.resolve(response.data))
}


function oldGetBookInfo(isbn) {
    const domain = "http://www.worldcat.org"
    const languageIndicator = "lang=en"
    const url = domain + "/search?qt=worldcat_org_all&q=" + isbn + "&" + languageIndicator
    axios.get(url)
        .then(response => {
            const wholePage = response.data
            const secondPageUrlBeginPos = wholePage.indexOf('href="/title/') + 6
            const secondPageUrlEndPos = wholePage.indexOf('"', secondPageUrlBeginPos)
            const secondPageUrl = domain + wholePage.substring(secondPageUrlBeginPos, secondPageUrlEndPos) + "&" + languageIndicator
            console.log(secondPageUrl)
            return axios.get(secondPageUrl)
        })
        .then(response => {
            getBookInfoFromPage(response.data)
        })
}



class BookInfoAnalyzer {
    constructor(bookInfoPage) {
        this._bookInfoPage = bookInfoPage
        this._info = null ; this._error = null
        this._title = null; this._publisherAndYearAnalyzer = null
        this._languageAnalyzer = null; this._authorsAnalyzer = null
        this._pageCountAnalyzer = null
    }

    bookInfo() {
        this.doAnalysis()
        return this._info
    }

    pageError() { 
        this.doAnalysis()
        return this._error
    }

    setupFinders() {
        const scanner = new scanners.HtmlPageScanner(this._bookInfoPage)
        // title
        this._title = scanner.textBetweenExactDelimiters('<h1 class="title">', '</h1>')
        // publisher and year
        this._publisherAndYearAnalyzer = new bookItems.HtmlPublisherAndYearAnalyzer(
            scanner.tdInsideSpecificTr("bib-publisher-row")
        )
        // languages
        this._languageAnalyzer = new languages.HtmlLanguageAnalyzer(scanner.tdInsideSpecificTr("bib-itemType-row"))
        // authors - 2nd choice (fetch for later)
        const allAuthorsText = scanner.tdInsideSpecificTr("details-allauthors")
        // page count
        this._pageCountAnalyzer = new bookItems.HtmlPageCountAnalyzer(
            scanner.tdInsideSpecificTr("details-description")
        )
        // authors - 1st choice (fetch for later)
        const mainAuthorsText = scanner.tdInsideSpecificTr("details-respon")

        // only now the authors analyzer can be created
        this._authorsAnalyzer = new authorAnalysis.HtmlAuthorsAnalyzer(mainAuthorsText, allAuthorsText)
    }

    checkRightPage() {
        if (this._bookInfoPage.indexOf('"details-doctype"') < 0) {
            this._error = bookSearchLib.isbnNotFoundError
        }
    }

    doAnalysis() {
        if (this._info || this._error) { return }

        this.checkRightPage() 
        if (this._error) { return }

        this.setupFinders()
        // language prefetch
        const lang = this._languageAnalyzer.language()

        this._info = {
            title: this._title,
            authors: this._authorsAnalyzer.authors(),
            publisher: this._publisherAndYearAnalyzer.publisher(),
            year: this._publisherAndYearAnalyzer.year(),
            languages: lang ? [lang] : []
        }
        const possiblePageCount = this._pageCountAnalyzer.pageCount();
        if (possiblePageCount) {
            this._info.pageCount = possiblePageCount;
        }
    }
}

module.exports.fetchBookInfoByIsbn = fetchBookInfoByIsbn
module.exports.fetchBookInfoByIsbnFromHtmlPage= fetchBookInfoByIsbnFromHtmlPage
module.exports.fetchBookInfoByIsbnFromWorldcatHtmlPage= fetchBookInfoByIsbnFromWorldcatHtmlPage

