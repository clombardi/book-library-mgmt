var _ = require('lodash');
const Promise = require('bluebird')

/*
    NamedEntity
************** */
class NamedEntity {
	constructor(name) {
		this._name = name
	}

	name() { return this._name }
	persistentId() { return this._persistentId }

	setPersistentId(id) { this._persistentId = id }

	basicUIJSON() { 
		return { name: this.name() }
	}

	persistenceJSON() { return this.basicUIJSON() }
}
/*******   NamedEntity - end */


/*
    NamedEntityStore
************************** */
class NamedEntityStore {

	// e.g. 'authors'
	constructor(entityName)	{
		this._entityName = entityName
		this._entities = []
		this._databaseDomain = null
		this._database = null
	}

	setDatabaseDomain(domain) { this._databaseDomain = domain }
	setDatabase(db) { this._database = db }

	databaseReference() { 
		return this._databaseDomain 
			? (this._databaseDomain + '/' + this._entityName) 
			: this._entityName
	}
	database() { return this._database }
	entityName() { return this._entityName }
	capitalizedEntityName() {
		return this._entityName[0].toUpperCase() + this._entityName.slice(1)
	}

	entities() { return this._entities }

	/*  the stored entity having the specified name, 
	    null if there no such entity
	 */
	entityNamed(someName) { 
		return this._entities.find( (enti) => enti.name() == someName ) 
	}

	/*  the stored entity having the specified id, 
	    null if there no such entity
	 */
	entityHavingId(someId) { 
		return this._entities.find( (enti) => enti.persistentId() == someId ) 
	}

	/* 
	 * returns a Promise on the new NamedEntity 
	 */
	addNewEntity(name) {
		const self  = this
		const newEntity = new NamedEntity(name)
		// database
		return (
			this.database().ref(this.databaseReference()).push(newEntity.persistenceJSON())
			.then(function(dbRef) {
				// store state
				newEntity.setPersistentId(dbRef.key)
				self._entities.push(newEntity)
				return Promise.resolve(newEntity)
			})
		)
	}

	/* 
	 * returns a Promise on the entity having the given name, whether existing or new
	 */
	forceEntity(name) {
		let theEntity = this.entityNamed(name)
		return (theEntity) ? Promise.resolve(theEntity) : this.addNewEntity(name)
	}

	/* 
	 * reads all entities
	 */
	readFromDisk(silent) {
		const self = this
		return this.database().ref(this.databaseReference()).once("value").then(function(snapshot) {
			if (!silent) { console.log('about to process read ' + self._entityName) }
	      	snapshot.forEach((enti) => {
	      		self.addReadEntity( {name: enti.val().name, persistentId: enti.key } )
	     	})
			if (!silent) { console.log(self.capitalizedEntityName() + ' read OK') }
			return Promise.resolve(self._entities.length)
	    }).catch(err => {
			console.log(err)
			throw err
		})
	}

	/* 
	   entitySpec: { name, persistentId } 
	   returns the new NamedEntity
	 */
	addReadEntity(entitySpec) {
		const newEntity = new NamedEntity(entitySpec.name)
		newEntity.setPersistentId(entitySpec.persistentId)
		this._entities.push(newEntity)
		return newEntity
	}
}
/*******   NamedEntityStore - end */



module.exports.NamedEntity = NamedEntity
module.exports.NamedEntityStore = NamedEntityStore

