const _ = require('lodash')

// package for XLSX file generation
const xlsx = require('xlsx')
const streamify = require('stream-converter')
const exceljs = require('exceljs')

const fileProcesses = require('./fileProcesses')
const bookApp = require('./appModel')

function isNumericValue(value) {
    return typeof(value) === "number" && isFinite(value)
}


/**************************************************
  Stuff related to xlsx library
 **************************************************/

/*
  Utility class for xlsx library
  Check spreadsheet format used by xlsx package.
  https://github.com/SheetJS/js-xlsx
 */
class WorksheetBuilder {
    constructor() { 
        this._worksheet = {} 
        this._currentRow = 0
        this._computedRangeEnd = {c: 0, r: 0}
        this._explicitRangeEnd = null
        this._avoidRangeEndComputation = false
    }

    addStringCell(ref, value, otherCellInfo) { this.addCellWithType(ref, value, "s", otherCellInfo) }
    addNumericCell(ref, value, otherCellInfo) { this.addCellWithType(ref, value, "n", otherCellInfo) }
    addCellWithType(ref, value, type, otherCellInfo) { 
        this._worksheet[ref] = Object.assign({ v: value, t: type }, otherCellInfo)
        this.updateComputedRangeEnd(ref)
    }
    addCell(ref, value, otherCellInfo = {}) {
        if (value === null || value === undefined) {
            this.addCellWithType(ref, null, "z", otherCellInfo)
        } else if (isNumericValue(value)) {
            this.addNumericCell(ref, value, otherCellInfo)
        } else {
            this.addStringCell(ref, value, otherCellInfo)
        }
    }
    addRow(values) {
        this._avoidRangeEndComputation = true
        let address = null   // will hold the address of the last added cell
        values.forEach((value, column) => {
            address = {c: column, r: this._currentRow}
            const ref = xlsx.utils.encode_cell(address)
            this.addCell(ref, value)
        })
        this._avoidRangeEndComputation = false
        this.updateComputedRangeEnd(address)
        this._currentRow++
    }
    addTitleRow(values) {
        this.addRow(values)   // xlsx does not support bold
    }
    addSparseRow(values, columnCount) {
        const completeValues = new Array(columnCount)
        values.forEach((value) => completeValues[value.column] = value.value)
        this.addRow(completeValues)
    }

    setRangeEnd(ref) { this._explicitRangeEnd = xlsx.utils.decode_cell(ref) }
    // sheetBuilder.setColumnWidths([70, 50, 20, 30, 40, 6, 6, 4])
    setColumnWidths(widths) {
        this._worksheet["!cols"] = widths.map(theWidth => { return {width: theWidth} })
    }

    updateComputedRangeEnd(address) {
        if (address && !this._avoidRangeEndComputation && !this._explicitRangeEnd) {
            let decodedAddress = (typeof address === "string") ? xlsx.utils.decode_cell(address) : address
            this._computedRangeEnd = {
                c: Math.max(this._computedRangeEnd.c, decodedAddress.c),
                r: Math.max(this._computedRangeEnd.r, decodedAddress.r)
            }
        }
    }
    setRangeToWorksheet() {
        const decodedRangeEnd = _.defaultTo(this._explicitRangeEnd, this._computedRangeEnd)
        const range = { s: { c: 0, r: 0 }, e: decodedRangeEnd }
        const encodedRange = xlsx.utils.encode_range(range)
        this._worksheet["!ref"] = encodedRange

    }

    worksheet() { 
        this.setRangeToWorksheet()
        return this._worksheet 
    }
}

/*
  Utility class for xlsx library
 */
class WorkbookBuilder {
    constructor() { 
        this._workbook = xlsx.utils.book_new() 
    }

    appendSheet(worksheet, worksheetName) {
        xlsx.utils.book_append_sheet(this._workbook, worksheet, worksheetName)
        return this
    }

    workbook() { return this._workbook }
}

function createSingleSheetWorkbook(worksheet) {
    return new WorkbookBuilder().appendSheet(worksheet, 'Unico').workbook()
}

function createSingleSheetWorkbookFromBuilder(worksheetBuilder) {
    return createSingleSheetWorkbook(worksheetBuilder.worksheet())
}




/*
   Convenience superclass for XLSX file generation
 */
class XlsxFileGenerator extends fileProcesses.NumberedFileGenerator {
    streamMetadata() { return { contentType: "application/octet-stream" } }

    fillStream(stream) {
        const workbook = this.buildWorkbook()

        // write the workbook to a Node buffer
        const buffer = xlsx.write(workbook, { type: "buffer" })

        const bufferAsStream = streamify(buffer)
        bufferAsStream.on('error', (theError) => { this.finishWithError(theError) })

        bufferAsStream.pipe(stream)
    }

    buildWorkbook() { return this.buildSingleSheetWorkbook() }
    buildSingleSheetWorkbook() {
        const sheetBuilder = this.createWorksheetBuilder();
        return createSingleSheetWorkbookFromBuilder(sheetBuilder)
    }
    createWorksheetBuilder() { 
        throw "Abstract method createWorksheetBuilder must be defined in concrete class" 
    }
}


/*
     Test to create a XLSX file
 */
class ExampleXlsxFileGenerator extends XlsxFileGenerator {
    constructor(continuation) {
        super('excel', 'xlsx', continuation)
    }

    createWorksheetBuilder() {
        const sheetBuilder = new WorksheetBuilder();
        sheetBuilder.addStringCell('A1', 'Hola')
        sheetBuilder.addStringCell('A2', 'muchachos')
        sheetBuilder.addStringCell('B1', 'Esta planilla')
        sheetBuilder.addStringCell('B2', 'fue generada')
        sheetBuilder.addStringCell('B3', 'con mucho amor')
        sheetBuilder.setRangeEnd('B3')

        return sheetBuilder
    }
}


/*
     Generation of an XLSX file including the data of all books.
     separateMultipleValues: whether a book having multiple authors would occupy just one row, where the author names
     are given in the same column, separated by commas; or several rows, where all rows except the first include values
     for the author column only. Multiple languages are treated similarly. 
 */
 class BookXlsxFileGenerator extends XlsxFileGenerator {
    constructor(continuation, separateMultipleValues) {
        super('libros', 'xlsx', continuation)
        Object.assign(this, bookExcelGeneratorMixin)
        this._books = this.computeBooks()
        this._separateMultipleValues = separateMultipleValues

        console.log("Constructing a BookXlsxFileGenerator")
        console.log(separateMultipleValues)
        console.log(this._separateMultipleValues)
    }

    createWorksheetBuilder() {
        const sheetBuilder = new WorksheetBuilder();
        sheetBuilder.addTitleRow(["Título", "Autor", "Género", "Idioma", "Editorial", "Año", "Págs", "Copias"])
        this.books().forEach((book) => this.addBook(book, sheetBuilder))
        sheetBuilder.setColumnWidths([70, 50, 20, 30, 40, 8, 8, 8])
        return sheetBuilder
    }

    addBook(book, sheetBuilder) {
        sheetBuilder.addRow(this.firstBookRow(book))
        if (this._separateMultipleValues) {
            const rowCount = Math.max(book.authors().length, book.languages().length)
            for (let row = 1; row < rowCount; row++) {
                const rowData = []
                if (book.authors().length > row && book.authors()[row]) { 
                    rowData.push({column: 1, value: book.authors()[row].name()}) 
                }
                if (book.languages().length > row && book.languages()[row]) { 
                    rowData.push({ column: 3, value: book.languages()[row].name()}) 
                }
                sheetBuilder.addSparseRow(rowData, 8)
            }
        }
    }

}


/**************************************************
     Stuff related to ExcelJS library
 **************************************************/

/*
  Convenience superclass for XLSX file generation
 */
class ExcelJsFileGenerator extends fileProcesses.NumberedFileGenerator {
    streamMetadata() { return { contentType: "application/octet-stream" } }

    fillStream(stream) {
        const workbook = this.buildWorkbook()
        return workbook.xlsx.write(stream).then(() => stream.end())
    }
}

/*
     Generation of an example XLSX file, using the ExcelJS library.
 */
class ExampleExcelJsFileGenerator extends ExcelJsFileGenerator {
    constructor(continuation) {
        super('exceljs', 'xlsx', continuation)
    }

    buildWorkbook() {
        const workbook = new exceljs.Workbook();
        const worksheet = workbook.addWorksheet('Unico');
        worksheet.addRow(["Hola", "Esta planilla"])
        worksheet.addRow(["muchachos", "fue generada"])
        // third row is sparse
        const thirdRowValues = []
        thirdRowValues[2] = "con mucho amor"
        worksheet.addRow(thirdRowValues)

        return workbook
    }

}


/*
     Generation of an XLSX file including the data of all books, using the ExcelJS library.
     separateMultipleValues: whether a book having multiple authors would occupy just one row, where the author names
     are given in the same column, separated by commas; or several rows, where all rows except the first include values
     for the author column only. Multiple languages are treated similarly. 
 */
class BookExcelJsFileGenerator extends ExcelJsFileGenerator {
    constructor(continuation, separateMultipleValues) {
        super('libros', 'xlsx', continuation)
        Object.assign(this, bookExcelGeneratorMixin)
        this._books = this.computeBooks()
        this._separateMultipleValues = separateMultipleValues
    }

    buildWorkbook() {
        const workbook = new exceljs.Workbook();
        const worksheet = workbook.addWorksheet('Unico');

        // title row
        worksheet.addRow(["Título", "Autor", "Género", "Idioma", "Editorial", "Año", "Págs", "Copias"])
        const titleRow = worksheet.getRow(1);
        titleRow.font = { bold: true, name: 'Calibri' }
        const numberColumns = [6,7,8]
        numberColumns.forEach(colNumber => titleRow.getCell(colNumber).alignment = { horizontal: "right" })

        // each book
        this.books().forEach((book) => this.addBook(book, worksheet))

        // column widths
        const widths = [70, 50, 20, 30, 40, 8, 8, 8]
        widths.forEach((theWidth, index) => {
            worksheet.getColumn(index + 1).width = theWidth
        })

        return workbook
    }

    addBook(book, worksheet) {
        worksheet.addRow(this.firstBookRow(book))
        if (this._separateMultipleValues) {
            const rowCount = Math.max(book.authors().length, book.languages().length)
            for (let row = 1; row < rowCount; row++) {
                const rowData = []
                if (book.authors().length > row && book.authors()[row]) {
                    rowData[2] = book.authors()[row].name() 
                }
                if (book.languages().length > row && book.languages()[row]) {
                    rowData[4] = book.languages()[row].name() 
                }
                worksheet.addRow(rowData)
            }
        }
    }

}


/**************************************************
     Stuff related to book spreadsheet generation
 **************************************************/
const bookExcelGeneratorMixin = { 
    computeBooks: function() {
        return _.sortBy(
            bookApp.bookStore.books(),
            [(book) => book.title(), (book) => book.authors()[0], (book) => book.year()]
        )
    },

    books: function() { return this._books },

    firstBookRow: function(book) { return [
        book.title(),
        this.authorTextForFirstBookRow(book),
        book.genre() ? book.genre().name() : "",
        this.languageTextForFirstBookRow(book),
        book.publisher() ? book.publisher().name() : "",
        book.year(),
        book.pages(),
        book.copyCount()
    ]},

    authorTextForFirstBookRow: function(book) {
        if (this._separateMultipleValues) {
            return book.authors().length ? book.authors()[0].name() : ""
        } else {
            return book.authorNames().join(", ")
        }
    },

    languageTextForFirstBookRow: function(book) {
        if (this._separateMultipleValues) {
            return book.languages().length ? book.languages()[0].name() : ""
        } else {
            return book.languages().map(lang => lang.name()).join(", ")
        }
    }
}



module.exports.ExampleXlsxFileGenerator = ExampleXlsxFileGenerator
module.exports.BookXlsxFileGenerator = BookXlsxFileGenerator
module.exports.ExampleExcelJsFileGenerator = ExampleExcelJsFileGenerator
module.exports.BookExcelJsFileGenerator = BookExcelJsFileGenerator
