class NonExixtentObjectException extends Error { 
    constructor(kind, id) { 
        super()
        this._kind = kind
        this._id = id 
    }
    statusCode() { return 404 }
    message() { return `There is no ${this._kind} having id ${this._id}` }
}

module.exports = { NonExixtentObjectException }