const appModel = require('../appModel')
const scanners = require('./scanners')
const dataAnalyzer = require('./dataAnalyzer')
const misc = require('../../lib/misc')

/****************************************************************************
 *****       Author analysis
 ****************************************************************************/

function isUpperCase(char) { return "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚÇÀÈÌÒÙÃÕÄËÏÖÜÂÊÔÛÅ".includes(char) }


/**
 * Obtains an array of author names given the text supplied by WorldCat
 */
class RawAuthorAnalyzer {
    constructor(rawString) {
        this._scanner = new scanners.TextScanner(rawString)
        this._authorNames = []
    }

    // public method
    rawAuthorNames() {
        this.doAnalysis()
        return this._authorNames
    }

    doAnalysis() {
        this._authorNames = []

        // if the string to decode starts with a "[", then there are no authors
        if (this._scanner.currentChar() == "[") { return null; }

        this._currentAuthorStartPos = 0
        this._isDone = false

        while (!this._isDone && this._scanner.hasText()) {
            const currentChar = this._scanner.currentChar()
            // comma, semicolon, slash:
            // - current author name ends
            // - a subsequent uppercase char indicates more authors coming, 
            //   otherwise the remainder of the string denotes stuff other than authors (translators, etc.)
            if (currentChar == ";" || currentChar == "," || currentChar == "/") {
                if (this.upperCaseComing()) {
                    this.nextAuthorComes()
                } else {
                    this.endWithCurrentAuthor()
                }
                // dot - if it is not an initial (like J.R.R.Tolkien or J. R. R. Tolkien)
                //       then the remainder of the string denotes stuff other than authors
            } else if (currentChar == ".") {
                if (this._scanner.isPastInitial()) {
                    this.continuesCurrentAuthor()
                } else {
                    this.endWithCurrentAuthor()
                }
                // square bracket or parenthesis - remainder of the string denotes stuff other than authors
            } else if (currentChar == "[" || currentChar === "(") {
                this.endWithCurrentAuthor()
                // "y" 
                //  - more than one word starting with uppercase indicates another author
                //    (Carol Dunlop y Julio Cortázar)
                //  - otherwise is part of a name
                //    (José Ortega y Gasset)
            } else if (this._scanner.wordThatStarts() === "y") {
                if (this.twoWordsUpperCaseComing()) {
                    this.nextAuthorComes()
                } else {
                    this.continuesCurrentAuthor()
                }
                // "con" / "et" / "and" / "und" - indicates another author (Carol Dunlop con Julio Cortázar)
            } else if (["con", "et", "and", "und"].includes(this._scanner.wordThatStarts())) {
                this.nextAuthorComes()
                // "author" and variants - just jump over word
            } else if (this._scanner.wordThatStarts() && this._scanner.wordThatStarts().startsWith("author")) {
                this.discardCurrentWord()
                // "translator" / "editor" or variants: done!
            } else if (
                this._scanner.wordThatStarts() &&
                (this._scanner.wordThatStarts().startsWith("translator") ||
                    this._scanner.wordThatStarts().startsWith("editor"))
            ) {
                this._isDone = true
                // number (presumably a year) divide authors
            } else if (this._scanner.isOnNumber(2)) {
                this.nextAuthorComes()
            } else {
                this.continuesCurrentAuthor()
            }
        }
        // after the search: maybe there is an additional author to be taken
        if (!this._isDone) { this.takeCurrentAuthor() }
    }



    //   conditions and actions
    /////////////////////////////////////////////////

    /**
     * the following word starts with an uppercase
     */
    upperCaseComing() {
        let nextWordPos = this._scanner.jumpSpaces(this._scanner.currentPos() + 1)
        return nextWordPos < this._scanner.sourceText().length && isUpperCase(this._scanner.charAtPosition(nextWordPos))
    }

    /**
     * the following two word start with an uppercase
     */
    twoWordsUpperCaseComing() {
        let localPos = this._scanner.jumpNonSeparators()
        let firstWordAfter = this._scanner.word(localPos)

        if (!firstWordAfter || !isUpperCase(firstWordAfter[0])) { return false }

        localPos = this._scanner.jumpWord(localPos)
        if (" " != this._scanner.charAtPosition(localPos)) { return false }

        let secondWordAfter = this._scanner.word(localPos)
        return (secondWordAfter && isUpperCase(secondWordAfter[0]))
    }

    /** 
     * get the author up to (but excluding) current position, 
     * and continue searching after the current word
     */
    nextAuthorComes() {
        this.takeCurrentAuthor()
        const nextWordStart = this._scanner.jumpSeparators(this._scanner.jumpNonSeparators())
        this._scanner.advanceToPosition(nextWordStart)
        this._currentAuthorStartPos = nextWordStart
        // setting _currentAuthorStartPos past end-of-string is harmless
    }

    /** 
     * get the author up to (but excluding) current position, and end the analysis 
     * (I concluded that the remainder of the string contains info other than author names)
     */
    endWithCurrentAuthor() {
        this.takeCurrentAuthor()
        this._isDone = true
    }

    /**
     * the end of the current author' name lies ahead the current position
     */
    continuesCurrentAuthor() { this._scanner.advanceToNextChar() }

    discardCurrentWord() {
        this._scanner.advanceToNextWord()
        this._currentAuthorStartPos = this._currentPos
    }


    //   internal methods
    /////////////////////////////////////////////////

    takeCurrentAuthor() {
        if (this._currentAuthorStartPos < this._scanner.sourceText().length) {
            const currentAuthorName = this._scanner.textUpTo(this._scanner.currentPos(), this._currentAuthorStartPos).trim()
            if (currentAuthorName.length > 0) { this._authorNames.push(new TextFixer(currentAuthorName).finalText()) }
        }
    }
}


class TextFixer {
    constructor(rawText) {
        this._rawText = rawText;
        this._replacements = [
            { key: '&aacute;', value: 'á' }, { key: '&eacute;', value: 'é' },
            { key: '&iacute;', value: 'í' }, { key: '&oacute;', value: 'ó' },
            { key: '&uacute;', value: 'ú' }, { key: '&ntilde;', value: 'ñ' }
        ]
    }

    finalText() {
        return this._replacements.reduce(
            (text, repl) => text.replace(new RegExp(repl.key, "g"), repl.value),
            this._rawText
        );

    }
    
}

/**
 * Obtains the name in an array which best matches a given name
 */
class AuthorNameMatcher {
    constructor(baseNames) {
        this._baseNames = baseNames.map(name => {
            return { original: name, split: splitName(name) }
        })
    }

    // public method
    bestMatchFor(externalName) {
        const bestMatch = bestMatchForBaseNames(splitName(externalName), this._baseNames)
        return bestMatch ? bestMatch.original : externalName
    }
}

/**
 * Main auxiliary function for AuthorNameMatcher,
 * namely, the implementation of the best-match algorithm
 */
function bestMatchForBaseNames(splitExternalName, baseNames) {
    const reversedExternalSplit = splitExternalName.reverse()
    const externalAllSingleLetter = splitExternalName.every(word => word.length == 1)

    // #1 backward analysis
    let candidates = []
    baseNames.forEach(baseName => {
        if (externalAllSingleLetter || baseName.split.some(word => word.length > 1)) {
            // need to create a new àrray since reverse() works in place
            const reversedBaseName = Array.from(baseName.split).reverse()
            const backCoincidences = splitCoincidences(reversedBaseName, reversedExternalSplit)
            if (backCoincidences > 0) {
                candidates.push(Object.assign({ coincidenceCount: backCoincidences }, baseName))
            }
        }
    })

    /*
     * #2: The analysis concludes in some cases:
     * - there are no candidates
     * - there is just one candidate
     * - there are candidates having coincidence in all pieces of both base and external names
     */
    if (candidates.length == 0) {
        return null
    } else if (candidates.length == 1) {
        return candidates[0]
    } else {
        const fullCandidates = candidates.filter(candidate => {
            return candidate.coincidenceCount == candidate.split.length
                && candidate.coincidenceCount == splitExternalName.length
        })
        if (fullCandidates.length > 0) {
            return bestCandidate(fullCandidates)
        } else {
            return bestCandidate(candidates)
        }
    }
}

/**
 * E.g. "Adolfo J. de P. Bioy-Casares"  =>  [ 'Adolfo', 'J', 'de', 'P', 'Bioy', 'Casares' ]
 */
function splitName(wholeName) {
    const rawPieces = wholeName.split(/( |-|\.)/)
    const pieces = rawPieces.filter(piece => piece.trim().length > 0 && piece != "-" && piece != ".")
    // si no incluimmos el "." en la línea de arriba, y se agrega el código comentado, nos queda así:
    //    E.g. "Adolfo J. de P. Bioy-Casares"  =>  [ 'Adolfo', 'J.', 'de', 'P.', 'Bioy', 'Casares' ]
    // let pieces = almostPieces
    // while (pieces.includes(".")) {
    //     const ix = pieces.indexOf(".")
    //     if (ix == 0) {
    //         pieces.shift()
    //     } else {
    //         pieces[ix - 1] = pieces[ix - 1] + "."
    //         pieces.splice(ix, 1)
    //     }
    // }
    return pieces.map(piece => misc.stringToSpanishCompareValue(piece))
}

/**
 * E.g. 'c' is considered as coincident with 'carlos', in order to take 
 * ('juan carlos perez', 'juan c. perez') as a match
 */
function splitCoincidences(split1, split2) {
    const isCoincidence = function (string1, string2) {
        return (string1 == string2) || (string1 == string2[0]) || (string2 == string1[0])
    }
    let pos1 = 0
    let pos2 = 0
    let nextStartPos2 = 0
    let fullCoincidences = 0 ; let coincidences = 0
    let matchPresentInLastElement = false
    while (pos1 < split1.length && pos2 < split2.length) {
        while (pos2 < split2.length && !(isCoincidence(split1[pos1], split2[pos2]))) {
            pos2++
        }
        if (pos2 < split2.length) {
            if (fullCoincidences == 0 && pos1 > 0 && pos2 > 0) {
                return 0
            }
            coincidences++
            if (split1[pos1] == split2[pos2]) { fullCoincidences++ }
            if (pos1 == (split1.length - 1) || pos2 == (split2.length - 1)) {
                matchPresentInLastElement = true
            }
            pos1++
            pos2++
            nextStartPos2 = pos2
        } else {
            pos1++
            pos2 = nextStartPos2
        }
    }
    return matchPresentInLastElement && (fullCoincidences > 0) ? coincidences : 0
}


/**
 * Lexicographic order on 
 * - coincidence count, bigger
 * - length, bigger
 * - alphabetic order
 */
function bestCandidate(candidates) {
    const isBetterCandidate = function (challenger, champion) {
        if (challenger.coincidenceCount > champion.coincidenceCount) {
            return challenger
        } else if (challenger.coincidenceCount < champion.coincidenceCount) {
            return champion
        } else {  // challenger.coincidenceCount == champion.coincidenceCount
            if (challenger.original.length > champion.original.length) {
                return challenger
            } else if (challenger.original.length < champion.original.length) {
                return champion
            } else { // challenger.original.length == champion.original.length
                if (challenger.original < champion.original) {
                    return challenger
                } else {
                    return champion
                }
            }
        }
    }
    return candidates.reduce((champion, challenger) => isBetterCandidate(challenger, champion))
}


class HtmlAuthorsAnalyzer extends dataAnalyzer.DataAnalyzer {
    constructor(responsibilityText, allAuthorsText) {
        super(responsibilityText)
        this._allAuthorsText = allAuthorsText
        this._authors = []
    }

    authors() { this.analyze(); return this._authors }

    doAnalysis() {
        let rawAuthors = []
        if (this.sourceText()) {
            rawAuthors = new RawAuthorAnalyzer(this.sourceText()).rawAuthorNames()
        } else if (this._allAuthorsText) {
            rawAuthors = this.findAuthorsFromAllAuthorsText()
        }
        // match the obtained author names wrt the authors already registered by the app
        const authorMatcher = new AuthorNameMatcher(appModel.authorStore.entities().map(entity => entity.name()))
        this._authors = rawAuthors.map(authorName => authorMatcher.bestMatchFor(authorName))
    }

    //     <td>
    //         <a href='/search?q=au%3AI%CC%81n%CC%83iguez+Barrena%2C+Ma.+Lourdes&amp;qt=hot_author' title='Search for more by this author'>Ma  Lourdes Íñiguez Barrena</a>; 
    //         <a href='/search?q=au%3ABarrenetxea%2C+Iban%2C&amp;qt=hot_author' title='Search for more by this author'>Iban Barrenetxea</a>; 
    //         <a href='/search?q=au%3ACarroll%2C+Lewis%2C&amp;qt=hot_author' title='Search for more by this author'>Lewis Carroll</a>
    //         <div class="inline-search">
    //             ... stuff that does not include additional anchor tags ...
    //         </div>
    //     </td>
    findAuthorsFromAllAuthorsText() {
        let theAuthors = []; let author = null
        let authorScanner = new scanners.HtmlPageScanner(this._allAuthorsText)
        while (author = authorScanner.textInsideTag("a")) { theAuthors.push(author) }
        return theAuthors
    }
}



module.exports.RawAuthorAnalyzer = RawAuthorAnalyzer
module.exports.AuthorNameMatcher = AuthorNameMatcher
module.exports.HtmlAuthorsAnalyzer = HtmlAuthorsAnalyzer
