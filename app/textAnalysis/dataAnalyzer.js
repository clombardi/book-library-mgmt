class DataAnalyzer {
    constructor(rawText) { this._rawText = rawText ; this._analysisDone = false }

    sourceText() { return this._rawText ? this._rawText.trim() : null }

    analyze() {
        if (!this._analysisDone) { 
            this.doAnalysis() ; this._analysisDone = true
        } 
    }

    // doAnalysis abstract
}

module.exports.DataAnalyzer = DataAnalyzer