const dataAnalyzer = require('./dataAnalyzer')
const scanners = require('./scanners')


/************************************
 *     Publisher and year
 ************************************/

class HtmlPublisherAndYearAnalyzer extends dataAnalyzer.DataAnalyzer {
    constructor(rawText) {
        super(rawText)
        this._info = null
    }

    publisher() { this.analyze(); return this._info.publisher }
    year() { this.analyze(); return this._info.year }

    doAnalysis() {
        const textScanner = new scanners.TextScanner(this.sourceText())

        // there are colons: publisher name starts right after their first one
        // no colons, there are closed square brackets: publisher name starts right after their first one
        if (!textScanner.advanceOverOccurrenceOf(":") && !textScanner.advanceOverOccurrenceOf("]")) {
            // no colons, no closing square brackets
            // if the first word is followed by a comma, then we must jump an additional word after the comma
            // otherwise, we jump just one word
            const firstSpace = textScanner.nextOccurrenceOf(" ")
            const firstComma = textScanner.nextOccurrenceOf(",")
            if (!firstSpace && !firstComma) {
                textScanner.jumpToEnd()
            } else {
                const jumpWordAfterComma = firstComma && ((firstComma < firstSpace)
                    || (textScanner.textUpTo(firstComma, firstSpace).trim().length == 0)
                )
                if (jumpWordAfterComma) {
                    textScanner.advanceToPosition(textScanner.jumpWord(firstComma + 1))
                } else {
                    textScanner.advanceToPosition(firstSpace + 1)
                }
            }
        }

        // at this point, the scanner is at the start of publisher name (or past the end of source)
        // publisher name ends with either: a year (i.e. 4 digits), a comma, an open square bracket, a colon, a semicolon, or a slash
        const publisherEndPos = Math.min(textScanner.jumpToAnyOf(",[:;/"), textScanner.jumpToYearNumber())
        const publisherName = textScanner.hasText() ? textScanner.textUpTo(publisherEndPos) : null
        textScanner.advanceToPosition(publisherEndPos)

        // the year is the first 4-digit substring after publisher end position
        textScanner.advanceToPosition(textScanner.jumpToYearNumber())
        const yearNumber = textScanner.hasText() ? textScanner.textUpTo(textScanner.jumpWord()) : null

        this._info = {
            publisher: publisherName ? publisherName.trim() : null,
            year: yearNumber ? Number(yearNumber.trim()) : null
        }
    }
}


/************************************
 *     Page count
 ************************************/

class HtmlPageCountAnalyzer extends dataAnalyzer.DataAnalyzer {
    constructor(rawText) {
        super(rawText)
        this._pageCount = null
        this._scanner = null
    }

    pageCount() { this.analyze(); return this._pageCount }

    doAnalysis() {
        const isAcceptableAsPageCount = (word) => scanners.isDigit(word[0])
        this._scanner = new scanners.TextScanner(this._rawText)
        let numberWord = null
        while ((numberWord = this._scanner.word()) && !this.isAcceptableAsPageCount(numberWord)) { 
            this._scanner.advanceToNextWord() 
        }
        if (numberWord) {
            let ix
            for (ix = 0; ix < numberWord.length && scanners.isDigit(numberWord[ix]); ix++) {}
            this._pageCount = Number(numberWord.substring(0, ix))
        }
    }

    isAcceptableAsPageCount(word) {
        if (!scanners.isDigit(word[0])) { return false }
        const nextWordPos = this._scanner.jumpWord()
        const followingWord = this._scanner.word(nextWordPos)
        if (followingWord && 
            (followingWord.toLowerCase().startsWith("vol") || followingWord.toLowerCase().startsWith("online"))) {
            return false
        }
        return true
    }
}

module.exports.HtmlPublisherAndYearAnalyzer = HtmlPublisherAndYearAnalyzer
module.exports.HtmlPageCountAnalyzer = HtmlPageCountAnalyzer