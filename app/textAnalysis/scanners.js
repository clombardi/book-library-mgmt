const _ = require('lodash')

function isUpperCase(char) { return "ABCDEFGHIJKLMNÑOPQRSTUVWXYZÁÉÍÓÚÇÀÈÌÒÙÃÕÄËÏÖÜÂÊÔÛÅ".includes(char) }
function isWordSeparator(char) { return " ;,.[(])/".includes(char) }
function isSpace(char) { return char == " " }
function isDigit(char) { return "0123456789".includes(char) }

class TextScanner {
    constructor(sourceText) {
        this._sourceText = _.defaultTo(sourceText, '')
        this._currentPos = 0
        this._replacements = [
            { key: '&aacute;', value: 'á' }, { key: '&eacute;', value: 'é' },
            { key: '&iacute;', value: 'í' }, { key: '&oacute;', value: 'ó' },
            { key: '&uacute;', value: 'ú' }, { key: '&ntilde;', value: 'ñ' }
        ]
    }

    sourceText() { return this._sourceText }
    currentPos() { return this._currentPos }

    currentChar() { 
        if (this._currentPos >= this._sourceText.length) { return null }
        const applyingReplacement = this._replacements.find(repl => this.isComing(repl.key))
        return applyingReplacement ? applyingReplacement.value : this._sourceText[this._currentPos]
    }
    previousChar() { return this.charBackwards(1) }
    charBackwards(distance) {
        return (this._currentPos >= distance) ? this._sourceText[this._currentPos - distance] : null
    }
    charAtPosition(pos) { return this._sourceText[pos] }

    advanceToNextChar() { 
        const applyingReplacement = this._replacements.find(repl => this.isComing(repl.key))
        this._currentPos += (applyingReplacement ? applyingReplacement.key.length : 1)
    }
    advanceToNextWord() { this._currentPos = this.jumpWord() }
    advanceToPosition(pos) { this._currentPos = pos }

    consumeWord() {
        const theWord = this.word()
        this.advanceToNextWord()
        return theWord
    }

    /**
     * Advance over the first occurrence of the given substring.
     * If there is not such occurrence, then do nothing.
     * Return value: whether the current position actually changed
     */
    advanceOverOccurrenceOf(substr, initialPos = this._currentPos) {
        const targetPos = this.nextOccurrenceOf(substr, initialPos)
        if (targetPos != null) { this.advanceToPosition(targetPos + 1) }
        return (targetPos != null)
    }

    isAtEnd() { return this._currentPos >= this._sourceText.length }
    hasText() { return !this.isAtEnd() }

    /**
     * Whether the following chars coincide with the given string
     */
    isComing(string) {
        return this._sourceText.substr(this._currentPos, string.length) === string
    }

    properWord(initialPos = this._currentPos) {
        let wordEndPos = this.jumpNonSeparators(initialPos)
        return this._sourceText.substring(initialPos, wordEndPos)
    }

    word(initialPos = this._currentPos) {
        let wordStartPos = this.jumpSeparators(initialPos)
        return (wordStartPos < this._sourceText.length) ? this.properWord(wordStartPos) : null
    }

    /** 
     * word that starts at the current position; 
     * null if the current position is not the beginning of a word
     */
    wordThatStarts() {  
        return (this._currentPos == 0 || isSpace(this.previousChar())) ? this.properWord() : null 
    }
    
    /**
     * true if current position follows an initial (as the dots in J.R.R.Tolkien or J. R. R. Tolkien)
     */
    isPastInitial() {
        return isUpperCase(this.previousChar()) && (this._currentPos == 1 || " .".includes(this.charBackwards(2)))
    }
    
    isOnNumber(minLength = 1, initialPos = this._currentPos) {
        return (this._currentPos + minLength <= this._sourceText.length)
            && _.range(minLength).every(ix => isDigit(this._sourceText[initialPos + ix]))
    }

    isOnYearNumber(initialPos = this._currentPos) {
        return this.isOnNumber(4, initialPos) 
            && ((this._sourceText.length == initialPos + 4) || isWordSeparator(this._sourceText[initialPos + 4]))
    }

    nextOccurrenceOf(substr, initialPos = this._currentPos) {
        const pos = this._sourceText.indexOf(substr, initialPos)
        return (pos >= 0) ? pos : null
    }

    textUpTo(endPos, initialPos = this._currentPos) {
        return this._sourceText.substring(initialPos, endPos)
    }

    textUpToEnd(initialPos = this._currentPos) {
        return this._sourceText.substring(initialPos, this._sourceText.length)
    }

    jumpSeparators(initialPos = this._currentPos) { return this.jumpByFunction(initialPos, isWordSeparator) }
    jumpNonSeparators(initialPos = this._currentPos) {
        return this.jumpByFunction(initialPos, (char) => !isWordSeparator(char))
    }
    jumpSpaces(initialPos = this._currentPos) { return this.jumpByFunction(initialPos, isSpace) }
    jumpToAnyOf(chars, initialPos = this._currentPos) { 
        return this.jumpByFunction(initialPos, char => !chars.includes(char))
    }
    jumpToEnd(initialPos = this._currentPos) { return this._sourceText.length }

    jumpByFunction(initialPos = this._currentPos, fn) {
        let targetPos = initialPos
        while (targetPos < this._sourceText.length && (fn(this._sourceText[targetPos]))) { targetPos++ }
        return targetPos
    }

    jumpWord(initialPos = this._currentPos) {
        const wordStart = isWordSeparator(this._sourceText[initialPos]) 
                ? this.jumpSeparators(initialPos) : initialPos
        return this.jumpNonSeparators(wordStart)
    }

    jumpToYearNumber(initialPos = this._currentPos) {
        let targetPos = initialPos
        while ((targetPos < this._sourceText.length) && (!this.isOnYearNumber(targetPos))) { targetPos++ }
        return targetPos
    }

    jumpOverOpeningTag(tagName, initialPos = this._currentPos) {
        let startTagPos = this._sourceText.indexOf('<' + tagName, initialPos)
        return (startTagPos >= 0) ? this._sourceText.indexOf('>', startTagPos) + 1 : null
    }
}


class HtmlPageScanner {
    constructor(rawData, offset = 0) {
        this._rawData = rawData
        this._offset = offset
        this._previousOffset = 0
    }

    reset() { this._offset = 0 }
    rewind() { this._offset = this._previousOffset }
    recordNewOffset(offset) {
        this._previousOffset = this._offset
        this._offset = offset
    }

    exactTextPos(text, startPos = this._offset) {
        const pos = this._rawData.indexOf(text, startPos)
        return (pos >= 0) ? pos : null
    }
    followingExactTextPos(text, startPos = this._offset) {
        const startOfText = this.exactTextPos(text, startPos)
        return (startOfText != null) ? startOfText + text.length : null
    }

    textBetweenExactDelimiters(startDelimiter, endDelimiter, startPos = this._offset) {
        const beginPos = this.followingExactTextPos(startDelimiter, startPos)
        const endPos = this.exactTextPos(endDelimiter, startPos)
        if (beginPos != null && endPos != null) {
            this.recordNewOffset(endPos + endDelimiter.length)
            return this._rawData.substring(beginPos, endPos)
        } else {
            return null
        }
    }

    tdInsideSpecificTr(idOfTr, startPos = this._offset) {
        const trPos = this._rawData.indexOf('<tr id="' + idOfTr + '">', startPos)
        if (trPos >= 0) {
            const beginPos = this._rawData.indexOf('>', this._rawData.indexOf('<td', trPos) + 1) + 1
            const endPos = this._rawData.indexOf('</td>', beginPos)
            if (beginPos >= 0 && endPos >= 0) {
                this.recordNewOffset(endPos + 5)
                return this._rawData.substring(beginPos, endPos)
            }
        }
        // if either the specific tr or a following td are not found, then the search is not successful
        return null
    }

    textInsideTag(tagName, startPos = this._offset) {
        return this.textInsideTagWithParams(tagName, null, startPos)
    }

    textInsideTagWithParams(tagName, paramsText = null, startPos = this._offset) {
        const tagToSearch = (paramsText == null || paramsText == "") ? tagName : tagName + " " + paramsText
        let startTagPos = this._rawData.indexOf('<' + tagToSearch, startPos)
        if (startTagPos >= 0) {
            const beginPos = this._rawData.indexOf('>', startTagPos) + 1
            let endPos = this._rawData.indexOf('</' + tagName + '>', beginPos)
            if (endPos < 0) { endPos = this._rawData.length }
            this.recordNewOffset(endPos + tagName.length + 3)
            return this._rawData.substring(beginPos, endPos)
        } else {
            return null
        }
    }

    textUpTo(exactText, startPos = this._offset) {
        let endPos = this._rawData.indexOf(exactText, startPos)
        if (endPos < 0) { endPos = this._rawData.length }
        this.recordNewOffset(endPos)
        return this._rawData.substring(startPos, endPos)
    }

    /**
     * Whether the following chars coincide with the given string
     */
    isComing(string) {
        return this._rawData.substr(this._offset, string.length) === string
    }

    nextChars(count) {
        return this._rawData.substr(this._offset, count)
    }
    
}


module.exports.TextScanner = TextScanner
module.exports.HtmlPageScanner = HtmlPageScanner
module.exports.isDigit = isDigit