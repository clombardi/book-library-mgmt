const dataAnalyzer = require('./dataAnalyzer')
const scanners = require('./scanners')

//  <span class='itemType'>Print book</span> : Biography : Fiction : Spanish
//    <a href="/oclc/960410623/editions?editionsView=true&referer=di" class="vieweditions">View all editions and formats</a>
//  </span>
class HtmlLanguageAnalyzer extends dataAnalyzer.DataAnalyzer {
    constructor(rawText) {
        super(rawText)
        this._language = null
    }

    language() { this.analyze(); return this._language }

    doAnalysis() { this._language = this.findLanguageFromDataChunks(this.findDataChunks()) }

    findDataChunks() {
        if (this.sourceText() == null) { return [] }
        const textScanner = new scanners.TextScanner(this.sourceText())

        // establish starting point 
        const afterImgPos = textScanner.jumpOverOpeningTag("img")
        if (afterImgPos) { textScanner.advanceToPosition(afterImgPos) }

        // chunk search from estabished position
        const comesBefore = (pos1, pos2) => pos1 != null && (pos2 == null || pos1 < pos2)
        const theChunks = []
        let isDone = false
        while (!isDone && (textScanner.nextOccurrenceOf(':') != null)) {
            textScanner.advanceToPosition(textScanner.nextOccurrenceOf(':') + 1)
            const subsequentColon = textScanner.nextOccurrenceOf(':')
            const subsequentLt = textScanner.nextOccurrenceOf('<')
            if (comesBefore(subsequentColon, subsequentLt)) {
                theChunks.push(textScanner.textUpTo(subsequentColon))
                textScanner.advanceToPosition(subsequentColon)
            } else if (comesBefore(subsequentLt, subsequentColon)) {
                theChunks.push(textScanner.textUpTo(subsequentLt))
                isDone = true
            } else {
                isDone = true
            }
        }

        // done
        return theChunks.map(chunk => chunk.trim())
    }


    findLanguageFromDataChunks(chunks) {
        // search backwards
        for (let ix = chunks.length - 1; ix >= 0; ix--) {
            const chunk = chunks[ix]
            const lang = languageDefinitions.find(lang => lang.matches(chunk))
            if (lang) { return lang.longSpa() }
        }
        return null
    }
}

class LanguageDefinition {
    constructor(longSpa, longEng, short) {
        this._longSpa = longSpa
        this._longEng = longEng
        this._short = short
    }

    matches(string) { return [this._longSpa, this._longEng, this._short].includes(string) }

    longSpa() { return this._longSpa }
    longEng() { return this._longEng }
    short() { return this._short }
}

const languageDefinitions = [
    new LanguageDefinition("Castellano", "Spanish", "spa"),
    new LanguageDefinition("Inglés", "English", "eng"),
    new LanguageDefinition("Alemán", "German", "ger"),
    new LanguageDefinition("Polaco", "Polish", "pol"),
    new LanguageDefinition("Francés", "French", "fre"),
    new LanguageDefinition("Ruso", "Russian", "rus"),
    new LanguageDefinition("Italiano", "Italian", "ita"),
    new LanguageDefinition("Danés", "Danish", "dan"),
    new LanguageDefinition("Finés", "Finnish", "fin"),
    new LanguageDefinition("Holandés", "Dutch", "dut"),
    new LanguageDefinition("Húngaro", "Hungarian", "hun"),
    new LanguageDefinition("Portugués", "Portuguese", "por"),
    new LanguageDefinition("Catalán", "Catalan", "cat")
]

function longLanguageName(shortName) {
    const theLanguage = languages.languageDefinitions.find(lang => lang.matches(shortName))
    return theLanguage ? theLanguage.longSpa() : null
}

function languageMatching(str) {
    return languageDefinitions.find(lang => lang.matches(str))
}


module.exports.HtmlLanguageAnalyzer = HtmlLanguageAnalyzer
module.exports.languageDefinitions = languageDefinitions
module.exports.longLanguageName = longLanguageName
module.exports.languageMatching = languageMatching