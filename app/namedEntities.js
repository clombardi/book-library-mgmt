const namedEntityDef = require('./namedEntity')

module.exports.Author = namedEntityDef.NamedEntity
module.exports.Genre = namedEntityDef.NamedEntity
module.exports.Publisher = namedEntityDef.NamedEntity
module.exports.Language = namedEntityDef.NamedEntity
module.exports.Person = namedEntityDef.NamedEntity

module.exports.authorStore = new namedEntityDef.NamedEntityStore('authors')
module.exports.publisherStore = new namedEntityDef.NamedEntityStore('publishers')
module.exports.languageStore = new namedEntityDef.NamedEntityStore('languages')
module.exports.genreStore = new namedEntityDef.NamedEntityStore('genres')
module.exports.peopleStore = new namedEntityDef.NamedEntityStore('people')

