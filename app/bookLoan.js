const Promise = require('bluebird')

const bookModel = require('./book')
const namedEntities = require('./namedEntities')

class BookLoan {
    constructor(book) {
        this._persistentId = null
        this._book = book
        this._borrower = null
        this._startDate = null
        this._nextAlarmDate = null
        this._returnDate = null
    }

    persistentId() { return this._persistentId }
    book() { return this._book }
    borrower() { return this._borrower }
    startDate() { return this._startDate }
    nextAlarmDate() { return this._nextAlarmDate }
    returnDate() { return this._returnDate }
    isActive() { return this.startDate() && !this.returnDate() }
    shouldBeReturnedBy(referenceDate) {
        if (!referenceDate) { return false }
        return this.isActive() && this.nextAlarmDate() && this.nextAlarmDate().getTime() < referenceDate.getTime() 
    }

    setPersistentId(id) { this._persistentId = id; return this }
    setBook(book) { this._book = book; return this }
    setBorrower(person) { this._borrower = person; return this }
    setStartDate(date) { this._startDate = date; return this }
    setNextAlarmDate(date) { this._nextAlarmDate = date; return this }
    setReturnDate(date) { this._returnDate = date; return this }

    basicUIJSON() {
        let theJSON = {
            persistentId: this.persistentId(),
            book: this.book().title(),
            bookId: this.book().persistentId(),
            borrower: this.borrower() ? this.borrower().name() : null,
            startDate: this.startDate() ? this.startDate().getTime() : null,
            nextAlarmDate: this.nextAlarmDate() ? this.nextAlarmDate().getTime() : null,
            returnDate: this.returnDate() ? this.returnDate().getTime() : null,
            isActive: this.isActive()
        }
        return theJSON
    }

    persistenceJSON() {
        return {
            bookId: this.book() ? this.book().persistentId() : null,
            borrowerId: this.borrower() ? this.borrower().persistentId() : null,
            startDate: this.startDate() ? this.startDate().getTime() : null,
            nextAlarmDate: this.nextAlarmDate() ? this.nextAlarmDate().getTime() : null,
            returnDate: this.returnDate() ? this.returnDate().getTime() : null
        }
    }
}


/*
    bookStore
************************** */
const bookLoanStore = {
    _bookLoans: [],
    _databaseDomain: null,
    _database: null,

    bookLoans() { return this._bookLoans },
    setDatabaseDomain: function (domain) { this._databaseDomain = domain },
    setDatabase: function(db) { this._database = db },

    databaseReferenceFor: function (appRef) {
        return this._databaseDomain
            ? (this._databaseDomain + '/' + appRef)
            : appRef
    },
	database: function() { return this._database },

	/*  the stored book loan having the specified id, 
	    null if there no such book
	 */
    bookLoanHavingId(someId) {
        return this.bookLoans().find((loan) => loan.persistentId() == someId)
    },

    persistNewLoan(bookLoan) {
        return this.database().ref(this.databaseReferenceFor('bookLoans'))
        .push(bookLoan.persistenceJSON())
        .then(function(dbRef) { 
            bookLoan.setPersistentId(dbRef.key) 
            return Promise.resolve(bookLoan)
        } )
    },

    storeNewLoan(bookLoan) {
        const self = this
        return this.persistNewLoan(bookLoan).then(function(loan) { 
            self._bookLoans.push(loan) 
            return Promise.resolve(loan.persistentId())
        })
    },

    /*
      Sets the given return thate for a book loan record
      Return a Promise on the updated BookLoan object
     */
    recordReturnDate(bookLoan, date) {
        let dbReturnDateRef = this.database().ref(
            this.databaseReferenceFor('bookLoans') + '/' + bookLoan.persistentId() + '/returnDate'
        )
        return dbReturnDateRef.set(date.getTime()).then(function() {
            bookLoan.setReturnDate(date)
            return Promise.resolve(bookLoan)
        })
    },

    readFromDisk(silent) {
        const self = this
        return (this.database().ref(this.databaseReferenceFor('bookLoans')).once("value").then(
            function (snapshot) {
                if (!silent) { console.log('about to process read book loans') }
                /*
                  bookLoanSpec: key / val()
                  bookLoanSpec.val():  bookId / borrowerId / startDate / nextAlarmDate / returnDate
                 */
                snapshot.forEach((bookLoanSpec) => {
                    let book = bookModel.bookStore.bookHavingId(bookLoanSpec.val().bookId)
                    // just ignore loans for deleted books
                    if (book) {
                        let borrower = namedEntities.peopleStore.entityHavingId(bookLoanSpec.val().borrowerId)

                        let newLoan = new BookLoan(book)
                        newLoan.setPersistentId(bookLoanSpec.key)
                            .setBorrower(borrower)
                            .setStartDate(buildDateFromTime(bookLoanSpec.val().startDate))
                            .setNextAlarmDate(buildDateFromTime(bookLoanSpec.val().nextAlarmDate))
                            .setReturnDate(buildDateFromTime(bookLoanSpec.val().returnDate))
                        self._bookLoans.push(newLoan)
                        book.addLoan(newLoan)
                    } else {
                        console.log(`ignoring loan for deleted book ${bookLoanSpec.val().bookId}`)
                    }
                })
                if (!silent) { console.log('Book loans read OK') }
                return Promise.resolve(self._bookLoans.length)
            }
        ))
    }
    
}

// funcion utilitaria
function buildDateFromTime(timeOrNull) {
    let theDate = null
    if (timeOrNull) {
        theDate = new Date()
        theDate.setTime(timeOrNull)
    }    
    return theDate
}

module.exports.BookLoan = BookLoan
module.exports.bookLoanStore = bookLoanStore
