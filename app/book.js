var _ = require('lodash');
const Promise = require('bluebird')

const namedEntities = require('./namedEntities')
const loan = require('./bookLoan')
const misc = require('../lib/misc');
const { NonExixtentObjectException } = require('./error/customErrors');


/*
    Book
************** */
class Book {
	// added info 05-06/2018
	// isbn / bookcase / bookshelf / subtitle / collection / originalYear / isTranslation / originalLanguages
	constructor(title) {
		this._title = title
		this._isbn = null
		this._subtitle = null
		this._collection = null
		this._publisher = null
		this._genre = null
		this._authors = []
		this._languages = []
		this._notes = null
		this._year = null
		this._pages = null
		this._originalYear = null
		this._bookcase = null
		this._bookshelf = null
		this._isTranslation = false
		this._copyCount = 1
		this._originalLanguages = []
		this._loans = []
	}

	persistentId() { return this._persistentId }
	isbn() { return this._isbn }
	title() { return this._title }
	subtitle() { return this._subtitle }
	collection() { return this._collection }
	authors() { return this._authors }
	authorNames() { return this.authors().map((author) => author.name()) }
	languages() { return this._languages }
	language() { return this._languages.length ? this._languages[0] : null }
	publisher() { return this._publisher }
	genre() { return this._genre }
	copyCount() { return this._copyCount }
	
	notes() { return this._notes }
	year() { return this._year }
	pages() { return this._pages }
	originalYear() { return this._originalYear }
	bookcase() { return this._bookcase }
	bookshelf() { return this._bookshelf }
	isTranslation() { return this._isTranslation }
	originalLanguages() { return this._isTranslation ? this._originalLanguages : [] }
	originalLanguage() { return this.originalLanguages().length ? this.originalLanguages()[0] : null }
	
	loans() { return this._loans }
	currentLoan() { return this.loans().find(loan => loan.isActive()) }
	lastLoan() { return this.loans().length ? _.last(this.loans()) : null }
	borrower() { return this.currentLoan() ? this.currentLoan().borrower() : null }
	isLent() { return this.currentLoan() ? true : false }
	shouldBeReturnedBy(date) {
		return this.currentLoan() && this.currentLoan().shouldBeReturnedBy(date)
	}

	setPersistentId(id) { this._persistentId = id }
	setIsbn(isbn) { this._isbn = isbn }
	setTitle(title) { this._title = title }
	setSubtitle(title) { this._subtitle = title }
	setCollection(collectionName) { this._collection = collectionName }
	addAuthor(author) { this._authors.push(author) }
	clearAuthors() { this._authors = [] }
	addLanguage(language) { this._languages.push(language) }
	clearLanguages() { this._languages = [] }
	setGenre(genre) { this._genre = genre }
	setPublisher(publisher) { this._publisher = publisher }
	setCopyCount(count) { this._copyCount = count }
	
	setNotes(notes) { this._notes = notes }
	setBookcase(bookcase) { this._bookcase = bookcase }
	setBookshelf(bookshelf) { this._bookshelf = bookshelf }
	setYear(year) { this._year = year }
	setPages(pages) { this._pages = pages }
	setOriginalYear(year) { this._originalYear = year }
	setTranslation(yn) { 
		this._isTranslation = yn 
		if (!yn) { this.clearOriginalLanguages() }
	}
	addOriginalLanguage(language) { this._originalLanguages.push(language) }
	clearOriginalLanguages() { this._originalLanguages = [] }

	setSimpleFieldsFromSpec(spec) {
		this.setTitle(spec.title)
		this.setIsbn(spec.isbn)
		this.setSubtitle(spec.subtitle)
		this.setCollection(spec.collection)
		this.setYear(spec.year)
		this.setOriginalYear(spec.originalYear)
		this.setPages(spec.pages)
		this.setNotes(spec.notes)
		this.setBookcase(spec.bookcase)
		this.setBookshelf(spec.bookshelf)
		this.setTranslation(spec.isTranslation)
		if (spec.copyCount) { this.setCopyCount(spec.copyCount) }
	}

	simpleFieldsSpec() {
		return {
			// main data
			isbn: this.isbn(),
			title: this.title(),
			subtitle: this.subtitle(),
			collection: this.collection(),
			copyCount: this.copyCount(),
			// extra data
			notes: this.notes(),
			year: this.year(),
			pages: this.pages(),
			bookcase: this.bookcase(),
			bookshelf: this.bookshelf(),
			originalYear: this.originalYear(),
			isTranslation: this.isTranslation()
		}
	}

	addLoan(bookLoan) { this._loans.push(bookLoan) }

	setRawInfo(rawAuthors, rawLanguages, rawOriginalLanguages) { 
		this._rawAuthors = rawAuthors
		this._rawLanguages = rawLanguages
		this._rawOriginalLanguages = rawOriginalLanguages
	}
	rawAuthors() { return this._rawAuthors }
	rawLanguages() { return this._rawLanguages }
	rawOriginalLanguages() { return this._rawOriginalLanguages }

	basicUIJSON(referenceDate) { 
		const theJSON = this.simpleFieldsSpec()
		theJSON.persistentId = this.persistentId()

		// data from related objects
		theJSON.publisher = this.publisher() ? this.publisher().name() : null
		theJSON.genre = this.genre() ? this.genre().name() : null
		// multivalued data
		theJSON.authors = this.authorNames()
		theJSON.languages = this.languages().map((lang) => lang.name())
		theJSON.originalLanguages = this.originalLanguages().map((lang) => lang.name())

		// data about loan
		theJSON.isLent = this.isLent()
		theJSON.currentLoanId = this.currentLoan() ? this.currentLoan().persistentId() : null
		theJSON.borrower = this.borrower() ? this.borrower().name() : null
		theJSON.currentLoanStartDate = 
			(this.currentLoan() && this.currentLoan().startDate())
				? this.currentLoan().startDate().getTime() : null
		theJSON.currentLoanNextAlarmDate = 
			(this.currentLoan() && this.currentLoan().nextAlarmDate()) 
                ? this.currentLoan().nextAlarmDate().getTime() : null
        // shouldReturn makes sense only if referenceDate is given
        if (referenceDate) {
            theJSON.shouldBeReturned = this.shouldBeReturnedBy(referenceDate)
        }

		return theJSON
	}

	persistenceJSON() {
		const theJSON = this.simpleFieldsSpec()
		theJSON.publisherId = this.publisher() ? this.publisher().persistentId() : null
		theJSON.genreId = this.genre() ? this.genre().persistentId() : null
		theJSON.authors = this.authors().map((auth) => auth.persistentId())
		theJSON.languages = this.languages().map((lang) => lang.persistentId())
		theJSON.originalLanguages = this.originalLanguages().map((lang) => lang.persistentId())
		return theJSON
	}
}
/*******   Book - end */


/* 
	utility functions for bookStore
************************** */
function sortByTitle(books) {
	if (books.length > 0) {
		books.sort((b1, b2) => Intl.Collator('es').compare(b1.title(), b2.title()))
	}
	return books
}



/*
    bookStore
************************** */
const bookStore = {
	_books: [],
	_databaseDomain: null,
	_database: null,
	
	books() { return this._books },
	booksSortedByTitle() { return sortByTitle([...this.books()]) },
	setDatabaseDomain: function(domain) { this._databaseDomain = domain },
	setDatabase: function(db) { this._database = db },

	databaseReferenceFor: function(appRef) {
		return this._databaseDomain
			? (this._databaseDomain + '/' + appRef)
			: appRef
	},
	database: function() { return this._database },

	// booksThatMatch(criterion) {
	// 	const comparableCriterion = misc.stringToSpanishCompareValue(criterion)
	// 	return this.books().filter(book => 
	// 		(book.isbn() && misc.stringToSpanishCompareValue(book.isbn()).includes(comparableCriterion)) 
	// 		|| (book.title() && misc.stringToSpanishCompareValue(book.title()).includes(comparableCriterion))
	// 		|| book.authorNames().some(authorName => misc.stringToSpanishCompareValue(authorName).includes(comparableCriterion))
	// 	)
	// },

	/*
     * query
     *   queryText
     *     searchString
     *     isbn
     *     title
     *     authors
     *     other (subtitle, collection)
     *   isLent
     *   shouldBeReturned
	 */
	booksThatMatch(query, referenceDate) {
		const conditions = []
		if (query) {
			if (query.queryText != null) {
				const comparableCriterion = misc.stringToSpanishCompareValue(query.queryText.searchString)
				const buildWhereToSearch = book => {
					const whereToSearch = []
					if (query.queryText.isbn) { whereToSearch.push(book.isbn()) }
					if (query.queryText.title) { whereToSearch.push(book.title()) }
					if (query.queryText.authors) { whereToSearch.push(...book.authorNames()) }
					if (query.queryText.other) { whereToSearch.push(...[book.subtitle(), book.collection()]) }
					return whereToSearch
				}
				conditions.push(book =>
					buildWhereToSearch(book).some(str => str && misc.stringToSpanishCompareValue(str).includes(comparableCriterion))
				)
			}
			if (query.isLent != null) {
				conditions.push(book => book.isLent() === query.isLent)
			}
			if (query.shouldBeReturned != null && referenceDate != null) {
				conditions.push(book => {
					return book.shouldBeReturnedBy(referenceDate) === query.shouldBeReturned
				})
			}
		}
		const filteredBooks = this.books().filter(book => conditions.every(cond => cond(book)))
		return sortByTitle(filteredBooks)
	},

	fetchLastlyEditedBookId() {
		return this.database().ref(this.databaseReferenceFor('lastlyEditedBookId')).once("value").then(function (snapshot) {
			return new Promise(function (resolve, reject) { resolve(snapshot.val()) })
		})
	},

	fetchLastlyEditedBook() {
		const self = this
		return this.database().ref(this.databaseReferenceFor('lastlyEditedBookId')).once("value").then(function(snapshot) {
			return new Promise(function(resolve,reject) {
				resolve(self.bookHavingId(snapshot.val()))
			})
		})
	},

	/*  the stored book having the specified id, 
	    null if there no such book
	 */
	bookHavingId(someId) { 
		return this._books.find( (book) => book.persistentId() == someId ) 
	},

	/* 
	   bookSpec: as described in BookOperation
	   returns a Promise on the new Book
	 */
	addNewBook(bookSpec) { return new AddBookOperation(bookSpec).doOperation() },

	/* 
	   bookSpec: { persistentId } + as described in BookOperation
	   returns a Promise on the updated Book
	 */
	updateBook(bookSpec) { return new UpdateBookOperation(bookSpec).doOperation() },

	/* 
	   bookSpec: { persistentId } 
	   returns a Promise on the deleted Book
	   throws NonExixtentObjectException is there is no such book
	 */
	softDeleteBook(bookSpec) { 
		return new DeleteBookOperation(bookSpec).doOperation() },
	
	/* 
		loanSpec: { bookId, borrowerName, startDate, nextAlarmDate }
		returns a Promise on the created BookLoan object
	 */
	lendBook(loanSpec) {
		const self = this
		const book = this.bookHavingId(loanSpec.bookId)
		let borrowerPromise = loanSpec.borrowerName
			? namedEntities.peopleStore.forceEntity(loanSpec.borrowerName)
			: Promise.resolve(null)
		return borrowerPromise.then(function(borrower) {
			let newLoan = new loan.BookLoan(book)
			newLoan
				.setBorrower(borrower)
				.setStartDate(loanSpec.startDate).setNextAlarmDate(loanSpec.nextAlarmDate)
			return loan.bookLoanStore.storeNewLoan(newLoan)
				.then(function(loanId) { 
					book.addLoan(newLoan) 
					return Promise.resolve(newLoan)
				})
		})
	},

	/*
		record that the borrower returned the book referred by the loan
	    Returns a Promise on the updated BookLoan object
	 */
	endLoan(loanId, returnDate) {
		let theLoan = loan.bookLoanStore.bookLoanHavingId(loanId)
		if (theLoan) {
			return loan.bookLoanStore.recordReturnDate(theLoan, returnDate)
		} else {
			return Promise.reject("there is no loan having id " + loanId)
		}
	},

	bookSimpleFields: function() {
		return [
			"isbn", "title",
			"subtitle", "collection", "pages", "notes", "copyCount",
			"bookcase", "bookshelf",
			"year", "originalYear", "isTranslation"
		]
	},

	readFromDisk(silent) {
		const self = this
		return (this.database().ref(this.databaseReferenceFor('books')).once("value").then(
			function(snapshot) {
				if (!silent) { console.log('about to process read books') }
				snapshot.forEach((book) => {
					const bookSpec = Object.assign(
						{persistentId: book.key}, 
						misc.extractFields(self.bookSimpleFields().concat(["publisherId", "genreId"]), book.val())
					)
					bookSpec.authorIds = _.values(book.val().authors).map((authorRef) => authorRef.entityId)
					bookSpec.languageIds = _.values(book.val().languages).map((languageRef) => languageRef.entityId)
					bookSpec.originalLanguageIds = _.values(book.val().originalLanguages).map((languageRef) => languageRef.entityId)
					bookSpec.rawAuthors = book.val().authors
					bookSpec.rawLanguages = book.val().languages
					bookSpec.rawOriginalLanguages = book.val().originalLanguages
					self.addReadBook(bookSpec)
				})
				if (!silent) { console.log('Books read OK'); }
				return Promise.resolve(self._books.length)
	    	} 
	  	))
	},

	/* 
	   bookSpec: as in BookOperation
	   returns the new Book
	 */
	addReadBook(bookSpec) {
		const newBook = new Book(bookSpec.title)
		newBook.setPersistentId(bookSpec.persistentId)
		newBook.setSimpleFieldsFromSpec(bookSpec)
		newBook.setPublisher(namedEntities.publisherStore.entityHavingId(bookSpec.publisherId))
		newBook.setGenre(namedEntities.genreStore.entityHavingId(bookSpec.genreId))
		let entity = null
		bookSpec.authorIds.forEach(function(id) {
			entity = namedEntities.authorStore.entityHavingId(id)
			if (entity) { newBook.addAuthor(entity) }
		} )
		bookSpec.languageIds.forEach(function(id) {
			entity = namedEntities.languageStore.entityHavingId(id)
			if (entity) { newBook.addLanguage(entity) }
		} )
		bookSpec.originalLanguageIds.forEach(function (id) {
			entity = namedEntities.languageStore.entityHavingId(id)
			if (entity) { newBook.addOriginalLanguage(entity) }
		})
		newBook.setRawInfo(bookSpec.rawAuthors, bookSpec.rawLanguages, bookSpec.rawOriginalLanguages)
		this._books.push(newBook)
		return newBook
	}

}
/*******   bookStore - end */


/*
	bookSpec: {
		isbn, title, subtitle, collection,
		authorNames, publisherName, genreName, languageNames,
		year, originalYear, pages, notes, copyCount, bookcase, bookshelf,
		isTranslation, originalLanguages  }
 */
class BookOperation {
	constructor(spec)	{ this.bookSpec = spec }
	
	databaseReferenceFor(appRef) { return bookStore.databaseReferenceFor(appRef) }

	doOperation() {
		const self = this
		return (
			// #1.1 : handle named original languages.
			//        This handling must be serialized wrt that of languages
			Promise.all(this.bookSpec.originalLanguageNames.filter((name) => name)
				.map((name) => namedEntities.languageStore.forceEntity(name)))
			// #1.2 : handle named entities
			.then(function(originalLanguages) {
				return (
					Promise.all([
						Promise.all(self.bookSpec.authorNames.filter((name) => name)
							.map((name) => namedEntities.authorStore.forceEntity(name))),
						Promise.all(self.bookSpec.languageNames.filter((name) => name)
							.map((name) => namedEntities.languageStore.forceEntity(name))),
						originalLanguages, 
						self.bookSpec.publisherName
							? namedEntities.publisherStore.forceEntity(self.bookSpec.publisherName)
							: null,
						self.bookSpec.genreName
							? namedEntities.genreStore.forceEntity(self.bookSpec.genreName)
							: null
					])
				)
			}).spread(function(authors, languages, originalLanguages, publisher, genre) {
			// #2 : fill the new book object
				const theBook = new Book(self.bookSpec.title)
				theBook.setSimpleFieldsFromSpec(self.bookSpec)
				authors.forEach((author) => theBook.addAuthor(author))
				languages.forEach((language) => theBook.addLanguage(language))
				originalLanguages.forEach((language) => theBook.addOriginalLanguage(language))
				theBook.setPublisher(publisher)
				theBook.setGenre(genre)
				return theBook
			}).then(function(theBook) {
			// #3 : store new book in firebase 
				const { theJSON, additionalData } = self.retouchedPersistenceJSON(theBook)
				return Promise.all([
					theBook, self.persistBook(theJSON), additionalData
				])
			}).spread(function (theBook, dbBookRef, additionalData) {
			// #4 : store into memory cache and resolve other database references
				const consolidatedBook = self.storeBook(theBook, dbBookRef)
				return Promise.all([
					consolidatedBook,
					self.persistBookAdditionalData(additionalData, dbBookRef),
					bookStore.database().ref(self.databaseReferenceFor('lastlyEditedBookId'))
						.set(dbBookRef.key)
						.then(() => "lastly edited book ok")
				])
			}).spread(function(theBook, additionalDataPersistenceResult, message) {
			// #5 : return
				return Promise.resolve(theBook)
			})
		)
	}

	retouchedPersistenceJSON(book) {
		const strippedPersistenceJSON = book.persistenceJSON()
		// replace undefined with null, just to make firebase happy
		Object.keys(strippedPersistenceJSON).forEach(key => {
			if (strippedPersistenceJSON[key] === undefined) {
				strippedPersistenceJSON[key] = null
			}
		})
		const authorIds = strippedPersistenceJSON.authors
		const languageIds = strippedPersistenceJSON.languages
		const originalLanguageIds = strippedPersistenceJSON.originalLanguages
		delete strippedPersistenceJSON.authors
		delete strippedPersistenceJSON.languages
		delete strippedPersistenceJSON.originalLanguages
		return { 
			theJSON: strippedPersistenceJSON, additionalData: { authorIds, languageIds, originalLanguageIds } 
		}
	}

	persistBookAdditionalData(additionalData, dbBookRef) {
		const { authorIds, languageIds, originalLanguageIds } = additionalData;
		return Promise.all([
			this.persistAuthors(authorIds, dbBookRef),
			this.persistLanguages(languageIds, dbBookRef),
			this.persistOriginalLanguages(originalLanguageIds, dbBookRef),
		]).then(() => Promise.resolve())
	}

	persistAuthors(authorIds, dbBookRef) {
		let dbAuthorsRef = dbBookRef.child('authors')
		return Promise.all(authorIds.map((id) => dbAuthorsRef.push({entityId: id})))
	}

	persistLanguages(languageIds, dbBookRef) {
		let dbLanguagesRef = dbBookRef.child('languages')
		return Promise.all(languageIds.map((id) => dbLanguagesRef.push({entityId: id})))
	}

	persistOriginalLanguages(languageIds, dbBookRef) {
		let dbLanguagesRef = dbBookRef.child('originalLanguages')
		return Promise.all(languageIds.map((id) => dbLanguagesRef.push({ entityId: id })))
	}
}


class AddBookOperation extends BookOperation {
	persistBook(theJSON) { 
		return bookStore.database().ref(this.databaseReferenceFor('books')).push(theJSON) 
	}

	storeBook(theBook, dbBookRef) {
		theBook.setPersistentId(dbBookRef.key)
		bookStore._books.push(theBook)
		return theBook
	}
}


class UpdateBookOperation extends BookOperation {
	persistBook(theJSON) { 
        let dbBookRef = bookStore.database().ref(this.databaseReferenceFor('books') + '/' + this.bookSpec.persistentId)
        return dbBookRef.set(theJSON).then(() => Promise.resolve(dbBookRef))
	}

	storeBook(theBook, dbBookRef) {
		let existingBook = bookStore.bookHavingId(this.bookSpec.persistentId)
		existingBook.setSimpleFieldsFromSpec(theBook.simpleFieldsSpec())
		existingBook.setPublisher(theBook.publisher())
		existingBook.setGenre(theBook.genre())
		existingBook.clearAuthors()
		theBook.authors().forEach((author) => existingBook.addAuthor(author))
		existingBook.clearLanguages()
		theBook.languages().forEach((lang) => existingBook.addLanguage(lang))
		existingBook.clearOriginalLanguages()
		theBook.originalLanguages().forEach((lang) => existingBook.addOriginalLanguage(lang))
		return existingBook
	}

	persistAuthors(authorIds, dbBookRef) {
		let dbAuthorsRef = dbBookRef.child('authors')
		return dbAuthorsRef.remove().then(() => super.persistAuthors(authorIds, dbBookRef))
	}

	persistLanguages(languageIds, dbBookRef) {
		let dbLanguagesRef = dbBookRef.child('languages')
		return dbLanguagesRef.remove().then(() => super.persistLanguages(languageIds, dbBookRef))
	}

	persistOriginalLanguages(languageIds, dbBookRef) {
		let dbLanguagesRef = dbBookRef.child('languages')
		return dbLanguagesRef.remove().then(() => super.persistOriginalLanguages(languageIds, dbBookRef))
	}
}

class DeleteBookOperation extends BookOperation {
	doOperation() {
		const theBook = bookStore.bookHavingId(this.bookSpec.persistentId)
		if (!theBook) {
			throw new NonExixtentObjectException('book', this.bookSpec.persistentId)
		}
		const { theJSON, additionalData } = this.retouchedPersistenceJSON(theBook)
		const currentBookRef = bookStore.database().ref(this.databaseReferenceFor('books') + '/' + theBook.persistentId());
		const newBookRef = bookStore.database().ref(this.databaseReferenceFor('deletedBooks') + '/' + theBook.persistentId());
		return Promise.all([
			bookStore.database().ref(newBookRef).set(theJSON),
			bookStore.database().ref(currentBookRef).remove()
		])
		.then(() => this.persistBookAdditionalData(additionalData, newBookRef))
		.then(() => {
			_.pull(bookStore._books, theBook)
			return Promise.resolve(theBook)
		})
	}

}


module.exports.Book = Book
module.exports.bookStore = bookStore


