const _ = require('lodash');
const Promise = require('bluebird')

const namedEntitiesModel = require('./namedEntities')
const bookModel = require('./book')
const bookLoanModel = require('./bookLoan')




/*
    storeCenter
************************** */
const storeCenter = { 	
	_databaseDomain: 'test',
	_database: null,
	changeDatabaseDomainTo: function(newDatabaseDomain) {
		this._databaseDomain = newDatabaseDomain
	},
	setDatabase: function(database) { this._database = database },
	databaseDomain: function() { return this._databaseDomain },
	database: function() { return this._database },
	init: function() {
		namedEntitiesModel.authorStore.setDatabaseDomain(this.databaseDomain())
		namedEntitiesModel.authorStore.setDatabase(this.database())
		namedEntitiesModel.languageStore.setDatabaseDomain(this.databaseDomain())
		namedEntitiesModel.languageStore.setDatabase(this.database())
		namedEntitiesModel.publisherStore.setDatabaseDomain(this.databaseDomain())
		namedEntitiesModel.publisherStore.setDatabase(this.database())
		namedEntitiesModel.genreStore.setDatabaseDomain(this.databaseDomain())
		namedEntitiesModel.genreStore.setDatabase(this.database())
		namedEntitiesModel.peopleStore.setDatabaseDomain(this.databaseDomain())
		namedEntitiesModel.peopleStore.setDatabase(this.database())
		bookModel.bookStore.setDatabaseDomain(this.databaseDomain())
		bookModel.bookStore.setDatabase(this.database())
		bookLoanModel.bookLoanStore.setDatabaseDomain(this.databaseDomain())
		bookLoanModel.bookLoanStore.setDatabase(this.database())
	},
	readFromDisk: function(silent) {
		let objsRead = 0
		if (!silent) { console.log('storeCenter.readFromDisk') }
		return namedEntitiesModel.authorStore.readFromDisk(silent)
			.then((n) => { 
				objsRead += n; return namedEntitiesModel.languageStore.readFromDisk(silent) 
			})
			.then((n) => { objsRead += n; return namedEntitiesModel.publisherStore.readFromDisk(silent) })
			.then((n) => { objsRead += n; return namedEntitiesModel.genreStore.readFromDisk(silent) })
			.then((n) => { objsRead += n; return namedEntitiesModel.peopleStore.readFromDisk(silent) })
			.then((n) => { objsRead += n; return bookModel.bookStore.readFromDisk(silent) })
			.then((n) => { objsRead += n; return bookLoanModel.bookLoanStore.readFromDisk(silent) })
			.then((n) => { 
				objsRead += n; 
				if (!silent) { console.log('storeCenter.readFromDisk - end'); }
				return Promise.resolve(objsRead) 
			})
			.catch(function(error) {
				console.log(error)
				return Promise.reject(error)
			})
	}

}

module.exports.storeCenter = storeCenter
module.exports.bookStore = bookModel.bookStore
module.exports.bookLoanStore = bookLoanModel.bookLoanStore
module.exports.authorStore = namedEntitiesModel.authorStore
module.exports.languageStore = namedEntitiesModel.languageStore
module.exports.publisherStore = namedEntitiesModel.publisherStore
module.exports.genreStore = namedEntitiesModel.genreStore
module.exports.peopleStore = namedEntitiesModel.peopleStore

