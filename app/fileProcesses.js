const firebase = require("firebase");
const database = firebase.database();

const Promise = require('bluebird')

const appModel = require('./appModel')

// Storage configuration though Google Cloud services
// const storageService = require('@google-cloud/storage');
// const gcs = storageService({
//     projectId: "biblapp-ff6b8",
//     keyFilename: "./config/biblapp-ff6b8-firebase-adminsdk-yd0y2-f66e718c5b.json"
// });
const {Storage} = require('@google-cloud/storage');
const gcs = new Storage({
    projectId: "biblapp-ff6b8",
    keyFilename: "./config/biblapp-ff6b8-firebase-adminsdk-yd0y2-f66e718c5b.json"
})

// Reference the Firebase account storage bucket
let storage = gcs.bucket('gs://biblapp-ff6b8.appspot.com');

const streamify = require('stream-converter')


// internal constants
const textMetadata = { contentType: "text/plain", contentEncoding: 'utf8' }


class FileGenerator {
    constructor(filename, extension, continuation) {
        this._filename = filename
        this._fileExtension = extension
        this._continuation = continuation
        this._hasErrors = false
        this._folder = 'testTextFiles'
    }

    numberDbReferenceName() { return 'lastTestFileNumber' }

    setFolder(folder) { this._folder = folder }
    folder() { return this._folder }

    fileExtension() { return this._fileExtension }
    filename() { return this._filename }
    setFilename(name) { this._filename = name } 
    filename() { return this._filename }
    folder() { return this._folder }
    generatedFileUrl() {
        return 'http://biblapp-ff6b8.appspot.com.storage.googleapis.com/' + this.completeFilename()
    }
    completeFilename() {
        return this.folder() + '/' + this.filename() + '.' + this.fileExtension() 
    }
    hasErrors() { return this._hasErrors }

    finishProcess() { 
        return this.finishActions().then(this._continuation(this))
    }

    finishActions() { return Promise.resolve() }

    finishWithError(theError) { 
        console.log(theError.stack)
        this._hasErrors = true
        this._continuation(this)
    }

    streamMetadata() { throw "method streamMetadata must be defined" }
    fillStream(stream) { throw "method fillStream must be defined" }

    generate() { 
        const self = this
        return this.preparationActions()
        .then(function(actionData) {
            const file = storage.file(self.completeFilename())
            const stream = file.createWriteStream(
                { resumable: false, public: false, metadata: self.streamMetadata() }
            )
            stream.on('finish', () => {
                self.finishProcess()
            })
            stream.on('error', (theError) => {
                self.finishWithError(theError)
            })
            self.fillStream(stream)
            // cannot do always stream.end() since piping is asynchronous, and furthermore does end the destination 
            // upon end of the piping.
            // *must* do explicit stream.end() if fillStream does not involve piping
            // so sorry!!
        })
    }

    preparationActions() { return Promise.resolve() }
}


class NumberedFileGenerator extends FileGenerator {
    constructor(filename, extension, continuation) {
        super(filename, extension, continuation)
        this._filenumber = 0
    }

    filename() {
        const filenameSuffix = '000000' + this.filenumber()
        return super.filename() + filenameSuffix.substr(filenameSuffix.length - 6, 6)
    }

    filenumber() { return this._filenumber }

    preparationActions() { return this.computeFilenumber() }

    finishActions() { return database.ref(this.filenumberDbReference()).set(this.filenumber()) }

    computeFilenumber() {
        const self = this
        return database.ref(this.filenumberDbReference()).once("value")
            .then(function (snapshot) {
                self._filenumber = snapshot.val() ? snapshot.val() : 0
                self._filenumber++
                return Promise.resolve({ filenumber: self._filenumber })
            })
    }

    filenumberDbReference() {
        return appModel.storeCenter.databaseDomain()
            ? (appModel.storeCenter.databaseDomain() + '/' + this.numberDbReferenceName())
            : this.numberDbReferenceName()
    }
}



class FileAction {
    constructor(folder, filename, extensions) {
        this._folder = folder
        this._filename = filename
        this._extensions = Array.isArray(extensions) ? extensions : [extensions]
    }

    filename() { return this._filename }
    folder() { return this._folder }
    fileExtensions() { return this._extensions }
    completeFilename(extension) {
        return this.folder() + '/' + this.filename() + '.' + extension
    }

    fetchFileExistsWithExtension(anExtension) {
        const file = storage.file(this.completeFilename(anExtension))
        return file.exists().then(
            fileExistsResponse => { 
                return { extension: anExtension, exists: fileExistsResponse[0] } 
            }
        )
    }

    fetchExistingExtension() {
        return Promise.all(this.fileExtensions().map(ext => this.fetchFileExistsWithExtension(ext)))
            .then(results => {
                const goodExtensionResult = results.find(res => res.exists)
                return goodExtensionResult ? goodExtensionResult.extension : null
            })
    }

    fetchFileExists() { return this.fetchExistingExtension().then(ext => ext ? true : false) }

    fetchExistingCompleteFilename() {
        return this.fetchExistingExtension().then(
            goodExtension => goodExtension ? this.completeFilename(goodExtension) : null
        )
    }
}


class FileDownloader extends FileAction {
    fetchBinaryDownload() {
        return this.fetchExistingExtension()
        .then(extension => {
            if (extension) {
                const file = storage.file(this.completeFilename(extension))
                return file.download().then(data => {
                    const contents = data[0]
                    return Promise.resolve({contents, extension})
                })
                .catch(err => {
                    return Promise.reject(err)
                })
            } else {
                return Promise.reject("File not found in storage")
            }
        })
    }
}


class FileEraser extends FileAction {
    fetchDelete() {
        return this.fetchExistingExtension()
        .then(extension => {
            if (extension) {
                const file = storage.file(this.completeFilename(extension))
                const deletePromise = file.delete()
                return deletePromise.then( deleteInfo => {
                    return Promise.resolve({ fileCount: 1, fileDeleted: this.completeFilename(extension) })
                })
            } else {
                return Promise.resolve({ fileCount: 0 })
            }
        })
    }
}

class ImageFileGenerator extends FileGenerator {
    constructor(imageBuffer, filename, mimeType, continuation) {
        super(filename, imageFileExtension(mimeType), continuation)
        this._imageBuffer = imageBuffer
        this._mimeType = mimeType
    }

    streamMetadata() { return {contentType: this._mimeType, cacheControl: "private, max-age=0"} }

    fillStream(stream) {
        const bufferAsStream = streamify(this._imageBuffer)
        bufferAsStream.on('error', (theError) => {
            this.finishWithError(theError)
        })
        bufferAsStream.pipe(stream) 
    }

    finishActions() { return Promise.resolve() }
}

function imageFileExtension(mimeType) {
    if (mimeType === "image/png") {
        return "png"
    } else if (mimeType === "image/jpeg") {
        return "jpg"
    } else {
        throw "Unexpected mime type: " + mimeType
    }
}


class SimpleTextFileGenerator extends NumberedFileGenerator {
    constructor(text, continuation) {
        super('text', 'txt', continuation)
        this._text = text
    }

    streamMetadata() { return textMetadata }
    fillStream(stream) { 
        stream.write(this._text, 'utf8') 
        stream.end()
    }
}

class SimpleTextFromBufferFileGenerator extends NumberedFileGenerator {
    constructor(text, continuation) {
        super('text', 'txt', continuation)
        this._text = text
    }

    streamMetadata() { return textMetadata }
    fillStream(stream) { 
        const buffer = Buffer.from(this._text, 'utf8')

        const bufferAsStream = streamify(buffer)
        bufferAsStream.setEncoding('utf8')        
        bufferAsStream.on('error', (theError) => {
            this.finishWithError(theError)
        })

        bufferAsStream.pipe(stream) 
    }
}


module.exports.FileGenerator = FileGenerator
module.exports.NumberedFileGenerator = NumberedFileGenerator
module.exports.SimpleTextFileGenerator = SimpleTextFileGenerator
module.exports.SimpleTextFromBufferFileGenerator = SimpleTextFromBufferFileGenerator
module.exports.ImageFileGenerator = ImageFileGenerator
module.exports.FileDownloader = FileDownloader
module.exports.FileEraser = FileEraser
