const appModel = require('../../appModel')

function appendSmallEntityRetrievalEndpoints(app) {
    app.get('/authorList', (request, response) => {
        const theEntities = appModel.authorStore.entities()
        response.json(
            theEntities.map((enti) => enti.basicUIJSON())
        )
    })

    app.get('/languageList', (request, response) => {
        const theEntities = appModel.languageStore.entities()
        response.json(
            theEntities.map((enti) => enti.basicUIJSON())
        )
    })

    app.get('/publisherList', (request, response) => {
        const theEntities = appModel.publisherStore.entities()
        response.json(
            theEntities.map((enti) => enti.basicUIJSON())
        )
    })

    app.get('/genreList', (request, response) => {
        const theEntities = appModel.genreStore.entities()
        response.json(
            theEntities.map((enti) => enti.basicUIJSON())
        )
    })

    app.get('/peopleList', (request, response) => {
        const theEntities = appModel.peopleStore.entities()
        response.json(
            theEntities.map((enti) => enti.basicUIJSON())
        )
    })
}

module.exports = { appendSmallEntityRetrievalEndpoints }