const appModel = require('../../appModel')
const misc = require("../../../lib/misc")

function appendBookLoanOperationEndpoints(app) {
    // add book loan
    /* loanSpec: { bookId, borrowerName, startDate, nextAlarmDate } */
    app.post('/bookLoans', function (request, response) {
        appModel.bookStore.lendBook({
            bookId: request.body.bookId,
            borrowerName: request.body.borrowerName,
            startDate: misc.nullSafeDate(request.body.startDate),
            nextAlarmDate: misc.nullSafeDate(request.body.nextAlarmDate)
        }).then(function (newBookLoan) {
            response.json({ newBookLoanId: newBookLoan.persistentId(), lendTimes: newBookLoan.book().loans().length })
        })
    })

    // return a lent book
    /* returnSpec: { returnDate } */
    app.put('/books/:bookId/return', function (request, response) {
        const theBook = appModel.bookStore.bookHavingId(request.params.bookId)
        appModel.bookStore.endLoan(
            theBook.currentLoan().persistentId(),
            misc.nullSafeDate(request.body.returnDate)
        )
            .then(function (bookLoan) {
                response.json(
                    { bookLoanId: bookLoan.persistentId(), lendTimes: bookLoan.book().loans().length }
                )
            })
    })
}

module.exports = { appendBookLoanOperationEndpoints }