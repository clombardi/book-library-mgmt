const _ = require('lodash');
const multer = require("multer")
const upload = multer({ storage: multer.memoryStorage() })

const misc = require('../../../lib/misc')
const appModel = require('../../appModel')
const fileProcesses = require('../../fileProcesses')

const bookFields = appModel.bookStore.bookSimpleFields().concat([
    "authorNames", "publisherName", "genreName", "languageNames", "originalLanguageNames"
])

function bookRetrievalResponse(allBooks, firstIndex, lastIndex, referenceDate) {
    const totalBookCount = allBooks.length
    let theFirstIndex = 0
    let selectedBooks = allBooks
    if (firstIndex != null || lastIndex != null) {
        theFirstIndex = _.defaultTo(firstIndex, 0)
        let theLastIndex = _.defaultTo(lastIndex, selectedBooks.length)
        let responseCount = theLastIndex - theFirstIndex
        if (totalBookCount <= theFirstIndex) {
            theFirstIndex = 0
            theLastIndex = responseCount
        }
        selectedBooks = selectedBooks.slice(theFirstIndex, theLastIndex)
    }
    return { 
        data: selectedBooks.map((book) => book.basicUIJSON(referenceDate)),
        firstIndex: theFirstIndex,
        totalCount: totalBookCount
    }
}

function appendBookRetrievalEndpoints(app) {
    app.get('/books', (request, response) => {
        const referenceDate = misc.nullSafeDate(request.query.referenceDate)
        const firstIndex = misc.nullSafeNumber(request.query.from)
        const lastIndex = misc.nullSafeNumber(request.query.to)
        let theBooks = appModel.bookStore.booksSortedByTitle()
        response.status(200)
        response.json(bookRetrievalResponse(theBooks, firstIndex, lastIndex, referenceDate))
    })
    
    /*
     * body:
     *   referenceDate
     *   indexRange
     *     from
     *     to
     *   query
     *     queryText
     *       searchString
     *       isbn
     *       title
     *       authors
     *       other
     *     isLent
     *     shouldBeReturned
     * 
     * response
     *   data
     *   firstIndex
     *   totalCount
     */
    app.post('/books/search', (request, response) => {
        const referenceDate = misc.nullSafeDate(request.body.referenceDate)
        const firstIndex = misc.nullSafeNumber(request.body.indexRange.from)
        const lastIndex = misc.nullSafeNumber(request.body.indexRange.to)
        const queryParameters = request.body.query
        const theBooks = appModel.bookStore.booksThatMatch(queryParameters, referenceDate)
        response.status(200)
        response.json(bookRetrievalResponse(theBooks, firstIndex, lastIndex, referenceDate))
    })

    app.get('/books/:bookId', (request, response) => {
        const theBook = appModel.bookStore.bookHavingId(request.params.bookId)
        if (theBook) {
            response.status(200)
            response.json(theBook.basicUIJSON())
        } else {
            response.status(400)
            response.json({ errorMessage: "No se encuentra libro con id " + request.params.bookId })
        }
    })

    app.get('/books/:bookId/lastLoan', (request, response) => {
        const theBook = appModel.bookStore.bookHavingId(request.params.bookId)
        if (theBook) {
            response.status(200)
            const theLoan = theBook.lastLoan()
            if (theLoan) {
                response.json(Object.assign({ hasLoan: true }, theLoan.basicUIJSON()))
            } else {
                response.json({ hasLoan: false })
            }
        } else {
            response.status(400)
            response.json({ errorMessage: "No se encuentra libro con id " + request.params.bookId })
        }
    })
}

function appendBookOperationEndpoints(app) {
    // add book
    app.post('/booksWithImage', upload.single("image"), function (request, response) {
        new AddBook(request, response).doOperation()
    })

    // update book
    app.put('/books/:bookId', upload.single("image"), function (request, response) {
        new UpdateBook(request, response).doOperation()
    })

    app.delete('/books/:bookId', function (request, response) {
        new DeleteBook(request, response).doOperation()
    })
}

class BookOperation {
    constructor(request, response) {
        this._request = request
        this._response = response
    }

    doOperation() {
        this.persistBookData()
            .then((theBook) => {
                this._bookDataResponse = this.buildBookDataResponse(theBook)
                this._bookId = theBook.persistentId()

                if (this._request.file) {
                    const eraser = this.buildFileEraser()
                    const generator = this.buildImageFileGenerator() 
                    eraser.fetchDelete().then(() => generator.generate())
                } else if (this._request.body.deleteImage === "true") {
                    const eraser = this.buildFileEraser()
                    eraser.fetchDelete().then((fileDeleteResponse) => {
                        this._response.status(200)
                        const imageDeletedData = { 'deletedImage': fileDeleteResponse.fileDeleted }
                        this._response.json(Object.assign({}, imageDeletedData, this._bookDataResponse))
                    })
                } else {
                    this._response.status(200)
                    this._response.json(this._bookDataResponse)
                }
            })
            .catch((err) => {
                console.log(err)
                this._response.status(500)
                this._response.json({ resource: "book", errorMessage: "Error en el registro del libro" })
            })
    }

    buildImageFileGenerator() {
        const generator = new fileProcesses.ImageFileGenerator(
            this._request.file.buffer, this._bookId, this._request.file.mimetype,
            (theGenerator) => {
                if (theGenerator.hasErrors()) {
                    this._response.status(207)
                    const errorData = {
                        errors: [
                            {
                                status: 500,
                                resource: "bookImage",
                                errorMessage: 'Error en el registro de la imagen'
                            }
                        ]
                    }
                    this._response.json(Object.assign({ metadata: errorData }, this._bookDataResponse))
                } else {
                    this._response.status(200)
                    const imageSavedData = { 'imageUrl': theGenerator.generatedFileUrl() }
                    this._response.json(Object.assign({}, imageSavedData, this._bookDataResponse))
                }
            }
        )
        generator.setFolder("bookImages")
        return generator        
    }

    buildFileEraser() {
        return new fileProcesses.FileEraser("bookImages", this._bookId, ["jpg", "png"])
    }

    persistBookData() { throw "Abstract method" }
    buildBookDataResponse() { throw "Abstract method" }
}


class AddBook extends BookOperation {
    persistBookData() { 
        const bodyAsJson = JSON.parse(this._request.body.structuredData)
        const bookSpec = misc.extractFields(bookFields, bodyAsJson)
        return appModel.bookStore.addNewBook(bookSpec)
    }

    buildBookDataResponse(theBook) { 
        return ({
            newBookId: theBook.persistentId(),
            bookListSize: appModel.bookStore.books().length
        })
    }
}

class UpdateBook extends BookOperation {
    persistBookData() {
        const bodyAsJson = JSON.parse(this._request.body.structuredData)
        const bookSpec = Object.assign({ persistentId: this._request.params.bookId },
            misc.extractFields(bookFields, bodyAsJson)
        )
        return appModel.bookStore.updateBook(bookSpec)
    }

    buildBookDataResponse(theBook) {
        return theBook.basicUIJSON()
    }
}

class DeleteBook extends BookOperation {
    async doOperation() {
        try {
            console.log(`about to delete book ${this._request.params.bookId}`)
            const deletedBook = await appModel.bookStore.softDeleteBook({ persistentId: this._request.params.bookId })
            this._response.status(200)
            this._response.json(deletedBook.basicUIJSON())
        } catch (err) {
            console.log("------------------ error in book deletion")
            console.log(err)
            const theStatus = _.isFunction(err.statusCode) ? err.statusCode() : 500
            const theMessage = _.isFunction(err.message) ? err.message() : 'Unspecified error processing request'
            this._response.status(theStatus)
            this._response.json(theMessage)
        }
    }
}


module.exports.appendBookRetrievalEndpoints = appendBookRetrievalEndpoints
module.exports.appendBookOperationEndpoints = appendBookOperationEndpoints