const React = require('react')
const ReactDOM = require('react-dom')

import Table from 'react-bootstrap/lib/Table'
import FormControl from 'react-bootstrap/lib/FormControl'
import InputGroup from 'react-bootstrap/lib/InputGroup'
import Button from 'react-bootstrap/lib/Button'

/*
 *  CoolSelectHandler
 ***********************/

/*
 *  redrawController: this object is expected to understand the message forceRedraw()
 */
class CoolSelectHandler {
	constructor(controlId, updateFunction, label, allowMultiSelect, redrawController) {
		this._controlId = controlId
		this._updateFunction = updateFunction
		this._label = label
		this._allowMultiSelect = allowMultiSelect
    this._redrawController = redrawController
		this._typedValue = ''
    this._disabled = false
		this._selectedList = []
	}

  isDisabled() { return this._disabled }

	setOptionList(optionList) { this._optionList = optionList }

  setSelectedValue(value) {
    this._typedValue = value
    this._optionToBeSelected = value
  }

  beDisabled() { this._disabled = true }
  beEnabled() { this._disabled = false }

	clearValues() {
		this._typedValue = ''
		this._filteredOptions = null
		this._optionToBeSelected = ''
		this._selectedList = []
	}

	selectedList() { return this._selectedList }

  setSelectedList(list) { this._selectedList = list }

	handleTypedValueChange(event) {
  	const typedValue = event.target.value
  	if (event.type == 'change') {
	  	this._typedValue = typedValue
  	}
  	this.updateOptions()
  	if (event.type == 'change') {
			console.log('should update the list')
	  	this._updateFunction(typedValue)
	  }
	}

	updateOptions() {
  	let mustFilter = false
  	let matchingOptions = null
  	if (this._typedValue.length >= 4) {
  		matchingOptions = this._optionList.filter((opt) => opt.indexOf(this._typedValue) >= 0)
  		mustFilter = (matchingOptions.length > 0)
  	}
  	if (mustFilter) {
  		this._filteredOptions = matchingOptions
  		this._optionToBeSelected = matchingOptions[0]
  	} else {
  		this._filteredOptions = null
  		this._optionToBeSelected = ''
  	}
	}

  handleSelectionChange(event) {
  	const value = event.target.value
  	if (value) {
  		this._typedValue = value
	  	this._updateFunction(value)
  	} else {
	  	this._updateFunction(this._typedValue)   // just to force render
  	}
  	this._optionToBeSelected = value
 	}

 	addCurrentToSelectedList() {
 		this._selectedList.push(this._typedValue)
 		this._typedValue = ''
 		this.updateOptions()
  	this._updateFunction(this._typedValue)  // just to force render
 		document.getElementById(this._controlId).focus()
 	}

  buildSelectedList() {
    return this.buildSelectedListFor(this._selectedList, true)
  }

	buildDisplaySelectedList() {
    return this.buildSelectedListFor(this._selectedList, false)
  }

  buildDisplaySelectedListAdding(additionalItem) {
    return this.buildSelectedListFor(this._selectedList.concat([additionalItem]), false)
  }

	buildSelectedListFor(givenList, includeRemoveButtons) {
		const self = this
		let displayMode = 'none'
		if (givenList.length) { displayMode = 'block' }

		return (
		  <Table condensed style={{display: displayMode, marginBottom: "2px"}}>
		    <tbody>
	        {
	        	givenList.map(function(opt) {
              let columns = [<td style={{fontSize: "90%"}}>{opt}</td>]
              if (includeRemoveButtons) {
                columns.push(<td><Button bsStyle="info" bsSize="xsmall" onClick={() => self.removeFromSelected(opt)}>Eliminar</Button></td>)
              }
	        		return (
					      <tr key={opt}>
					        { columns
                    /* <td style={{fontSize: "90%"}}>{opt}</td>
                       <td><Button onClick={() => this.removeFromSelected(opt)}>Eliminar</Button></td>
                     */
                  }
					      </tr>
	        		)
	        	})
	      	}
		    </tbody>
		  </Table>
		)
	}

  removeFromSelected(opt) {
    this._selectedList.splice(this._selectedList.indexOf(opt), 1)
    if (this._redrawController) { this._redrawController.forceRedraw() }
  }

	buildTextControl() {
		return (
      <FormControl type="text"
      	name={this._controlId}
        disabled={this.isDisabled()}
      	value={this._typedValue}
      	onChange={this.handleTypedValueChange.bind(this)}
      	onFocus={this.handleTypedValueChange.bind(this)}
      />
		)
	}

  initialSelectOption() {
  	return (<option key="--" value="">... elegir {this._label} ...</option>)
  }

	buildSelectControl() {
  	let theOptions = this._filteredOptions
  	if (!theOptions) { theOptions = this._optionList }
  	if (!theOptions) { theOptions = [] }
		const theSelect = (
      <FormControl
      	componentClass="select"
      	placeholder="select"
        disabled={this.isDisabled()}
      	value={this._optionToBeSelected}
      	onChange={this.handleSelectionChange.bind(this)}
      	onFocus={this.handleSelectionChange.bind(this)}
      >
      	{ this.initialSelectOption() }
        {
        	theOptions.sort().map(function(opt) {
	        		return <option key={opt} value={opt}>{opt}</option>
        	})
      	}
      </FormControl>
		);
		const theButton = (
      <InputGroup.Button>
        <Button onClick={this.addCurrentToSelectedList.bind(this)}>+ {this._label}</Button>
      </InputGroup.Button>
		);
  	if (this._allowMultiSelect) {
			return (
	      <InputGroup>
	      	{ theSelect }
		      { theButton }
	      </InputGroup>
			)
  	} else {
      return theSelect
  	}
	}
}
// --  CoolSelectHandler - end




module.exports.CoolSelectHandler = CoolSelectHandler
