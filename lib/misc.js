function extractFields(fields, object) {
    const newObject = {}
    fields.forEach(field => newObject[field] = object[field])
    return newObject
}

function stdToday() {
    const rawToday = new Date()
    return Date.UTC(rawToday.getUTCFullYear(), rawToday.getUTCMonth(), rawToday.getUTCDate())
}

function nullSafeDate(timeOrStringOrNull) {
    if (timeOrStringOrNull == null) {
        return null
    } else {
        const newDate = new Date()
        newDate.setTime(Number(timeOrStringOrNull))
        return newDate
    }
}

function nullSafeNumber(numberOrStringOrNull) {
    return (numberOrStringOrNull == null) ? null : Number(numberOrStringOrNull)
}


// copied from public/app/utils.js
const stringToSpanishCompareValue = function (str) {
    return str.split('').map(letter => charToSpanishCompareValue(letter)).join('')
}

// copied from public/app/utils.js
const charToSpanishCompareValue = function (char) {
    const accents = 'áàäéèëíìïóòöúùü'
    const normalized = 'aaaeeeiiiooouuu'
    let compareChar = char.toLowerCase()
    const accentIx = accents.indexOf(compareChar)
    if (accentIx > -1) {
        compareChar = normalized.charAt(accentIx)
    }
    return compareChar
}

const htmlStringToStandardString = function(htmlStr) {
    return [
        ["&aacute;", "á"], ["&eacute;", "é"], ["&iacute;", "í"], ["&oacute;", "ó"], ["&uacute;", "ú"], ["&yacute;", "ý"],
        ["&Aacute;", "Á"], ["&Eacute;", "É"], ["&Iacute;", "Í"], ["&Oacute;", "Ó"], ["&Uacute;", "Ú"], ["&Yacute;", "Ý"],
        ["&agrave;", "à"], ["&egrave;", "è"], ["&igrave;", "ì"], ["&ograve;", "ò"], ["&ugrave;", "ù"],
        ["&Agrave;", "À"], ["&Egrave;", "È"], ["&Igrave;", "Ì"], ["&Ograve;", "Ò"], ["&Ugrave;", "Ù"],
        ["&acirc;", "â"], ["&ecirc;", "ê"], ["&icirc;", "î"], ["&ocirc;", "ô"], ["&ucirc;", "û"],
        ["&Acirc;", "Â"], ["&Ecirc;", "Ê"], ["&Icirc;", "Î"], ["&Ocirc;", "Ô"], ["&Ucirc;", "Û"],
        ["&atilde;", "ã"], ["&otilde;", "õ"], ["&ntilde;", "ñ"],
        ["&Atilde;", "Ã"], ["&Otilde;", "Õ"], ["&Ntilde;", "Ñ"],
        ["&auml;", "ä"], ["&euml;", "ë"], ["&iuml;", "ï"], ["&ouml;", "ö"], ["&uuml;", "ü"], ["&yuml;", "ÿ"],
        ["&Auml;", "Ä"], ["&Euml;", "Ë"], ["&Iuml;", "Ï"], ["&Ouml;", "Ö"], ["&Uuml;", "Ü"],
        ["&iexcl;", "¡"], ["&iquest;", "¿"], ["&Ccedil;", "Ç"], ["&ccedil;", "ç"], ["&szlig;", "ß"],
        ["&Oslash;", "Ø"], ["&oslash;", "ø"], ["&Aring;", "Å"], ["&aring;", "å"]
    ].reduce((processingStr, spec) => processingStr.replace(new RegExp(spec[0], "gi"), spec[1]), htmlStr)
}

module.exports.extractFields = extractFields
module.exports.stdToday = stdToday
module.exports.nullSafeDate = nullSafeDate
module.exports.nullSafeNumber = nullSafeNumber
module.exports.stringToSpanishCompareValue = stringToSpanishCompareValue
module.exports.htmlStringToStandardString = htmlStringToStandardString
