const firebase = require("firebase");
const Promise = require('bluebird')

const appModel = require('../app/appModel')

const namedEntities = require('../app/namedEntities')
const bookModel = require('../app/book')
const bookLoanModel = require('../app/bookLoan')

const testUtils = require('./testUtils')


beforeAll(() => { 
    testUtils.initFirebase()

    appModel.storeCenter.changeDatabaseDomainTo('test_jest') 
    appModel.storeCenter.setDatabase(firebase.database())
    appModel.storeCenter.init()
    
})
beforeEach(() => {
    return testUtils.removeDatabase('test_jest')
        .then(() => testUtils.copyDatabase('test_jest', 'test_jest_master'))
        .then(() => appModel.storeCenter.readFromDisk(true))
})
afterAll(() => { 
    return testUtils.closeFirebase()
})

test('stupid test', () => {
    console.log('entering first test')
    expect(2+2).toBe(4)
    console.log('ending first test')
}, 20000)

test("just create a book", () => {
    const quijote = new bookModel.Book("Don Quijote")
    quijote.setYear(1605)
    expect(quijote.year()).toBe(1605)
    expect(quijote.authors()).toEqual([])
}, 20000)

test("book and loan - with objects", () => {
    const quijote = new bookModel.Book("Don Quijote")
    const roque = new namedEntities.Person("Roque")

    const theLoan = new bookLoanModel.BookLoan(quijote)
    theLoan.setBorrower(roque).setStartDate(new Date(2018, 0, 5)).setNextAlarmDate(new Date(2018,1,12))
    quijote.addLoan(theLoan)

    expect(theLoan.isActive()).toBeTruthy()
    expect(quijote.currentLoan()).toBeTruthy()

    theLoan.setReturnDate(new Date(2018,1,8))

    expect(theLoan.isActive()).toBeFalsy()
    expect(quijote.currentLoan()).toBeFalsy()
}, 20000)

test("book and loan - with stores", () => {
    let theBook = null
    let firstLoan = null
    //  bookSpec: { title, authorNames, publisherName, genreName, languageNames, year, pages, notes } 
    return appModel.bookStore.addNewBook({
        isbn: null, title: 'El libro de arena', authorNames: ['Jorge Luis Borges'], 
        genreName: 'Cuentos', languageNames: ['Castellano'], publisherName: 'Losada', 
        year: 1975, pages: 181, notes: "este me falta leerlo",
        subtitle: null, collection: null, originalYear: null, bookcase: null, bookshelf: null, 
        isTranslation: false, originalLanguageNames: []
    })
    .then(function(book) {
        theBook = book
        
        /* First loan */
        // loanSpec: { bookId, borrowerName, startDate, nextAlarmDate }
        return appModel.bookStore.lendBook({ 
            bookId: theBook.persistentId(), borrowerName: 'Valentina',
            startDate: new Date(2018, 0, 5), nextAlarmDate: new Date(2018, 1, 20)
        })
    })
    .then(function(bookLoan) {
        firstLoan = bookLoan
        expect(theBook.currentLoan()).toBeTruthy()
        expect(theBook.basicUIJSON().isLent).toBeTruthy()
        expect(theBook.borrower().name()).toEqual("Valentina")
        expect(theBook.currentLoan().nextAlarmDate()).toEqual(new Date(2018, 1, 20))
        expect(firstLoan.isActive()).toBeTruthy()

        /* return first loan */
        return appModel.bookStore.endLoan(firstLoan.persistentId(), new Date(2018,1,12))
    })
    .then(function(bookLoan) {
        expect(theBook.currentLoan()).toBeFalsy()
        expect(theBook.basicUIJSON().isLent).toBeFalsy()
        expect(theBook.borrower()).toBeNull()
        expect(firstLoan.isActive()).toBeFalsy()
        expect(firstLoan.returnDate()).toEqual(new Date(2018, 1, 12))
    
        /* make a second loan on the same book */
        return appModel.bookStore.lendBook({
            bookId: theBook.persistentId(), borrowerName: 'Josefina',
            startDate: new Date(2018, 2, 3), nextAlarmDate: new Date(2018, 3, 15)
        })
    })
    .then(function(bookLoan) {
        expect(theBook.currentLoan()).toBeTruthy()
        expect(theBook.basicUIJSON().isLent).toBeTruthy()
        expect(theBook.borrower().name()).toEqual("Josefina")
        expect(theBook.currentLoan().nextAlarmDate()).toEqual(new Date(2018, 3, 15))
        expect(firstLoan.isActive()).toBeFalsy()
        expect(bookLoan.isActive()).toBeTruthy()
        expect(appModel.bookLoanStore.bookLoans().length).toBe(2)

        return Promise.resolve()
    })
}, 20000)
