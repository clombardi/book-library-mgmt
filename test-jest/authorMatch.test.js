const authorSearch = require('../app/textAnalysis/authors')

const simpleAuthorNameBase = [
    "Jorge Luis Borges", "Julio Cortázar", "J.R.R. Tolkien", "George R.R. Martin"
    , "Naomi Klein", "Adolfo Bioy Casares"
]

const simpleMatcher = new authorSearch.AuthorNameMatcher(simpleAuthorNameBase)

test("exact match", () => {
    expect(simpleMatcher.bestMatchFor("Julio Cortázar")).toBe("Julio Cortázar")
})

test("unknown author", () => {
    expect(simpleMatcher.bestMatchFor("Juan Filloy")).toBe("Juan Filloy")
})

test("middle name missing", () => {
    expect(simpleMatcher.bestMatchFor("Jorge Borges")).toBe("Jorge Luis Borges")
})

test("only middle name initial", () => {
    expect(simpleMatcher.bestMatchFor("Jorge L. Borges")).toBe("Jorge Luis Borges")
})

test("both initials", () => {
    expect(simpleMatcher.bestMatchFor("J. L. Borges")).toBe("Jorge Luis Borges")
})

test("name with hyphen", () => {
    expect(simpleMatcher.bestMatchFor("Adolfo Bioy-Casares")).toBe("Adolfo Bioy Casares")
})

test("missing accent and middle initial added", () => {
    expect(simpleMatcher.bestMatchFor("Julio F. Cortazar")).toBe("Julio Cortázar")
})

test("middle name added", () => {
    expect(simpleMatcher.bestMatchFor("Julio Florencio Cortázar")).toBe("Julio Cortázar")
})

test("only first name initial included", () => {
    expect(simpleMatcher.bestMatchFor("J. Cortázar")).toBe("Julio Cortázar")
})

test("only surname included", () => {
    expect(simpleMatcher.bestMatchFor("Cortázar")).toBe("Julio Cortázar")
})

test("second surname missing", () => {
    expect(simpleMatcher.bestMatchFor("Adolfo Bioy")).toBe("Adolfo Bioy Casares")
})

test("names expanded - 1", () => {
    expect(simpleMatcher.bestMatchFor("John Ronald Reuel Tolkien")).toBe("J.R.R. Tolkien")
})

test("names expanded - 2", () => {
    expect(simpleMatcher.bestMatchFor("George Raymond Richard Martin")).toBe("George R.R. Martin")
})

test("dots separated differently", () => {
    expect(simpleMatcher.bestMatchFor("J. R. R. Tolkien")).toBe("J.R.R. Tolkien")
})

test("middle names missing", () => {
    expect(simpleMatcher.bestMatchFor("John Tolkien")).toBe("J.R.R. Tolkien")
})

test("names coincide, not surname", () => {
    expect(simpleMatcher.bestMatchFor("Jorge Luis Pérez")).toBe("Jorge Luis Pérez")
})

test("differences on name and surname", () => {
    expect(simpleMatcher.bestMatchFor("Jorge Borges Caamaño")).toBe("Jorge Luis Borges")
})

test("same surname, different names - 1", () => {
    expect(simpleMatcher.bestMatchFor("Peter Tolkien")).toBe("Peter Tolkien")
})

test("same surname, different names - 2", () => {
    expect(simpleMatcher.bestMatchFor("Mario Borges")).toBe("Mario Borges")
})

test("only surname", () => {
    expect(simpleMatcher.bestMatchFor("Cortázar")).toBe("Julio Cortázar")
})
