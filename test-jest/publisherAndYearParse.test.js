const bookItems = require('../app/textAnalysis/bookItems')

/* some examples of publisher and year
 *   Alcobendas, Madrid Libsa D.L. 2016
 *   Reino Unido : Usborne, 2015.
 *   México D. F. : Fondo De Cultura Ecónomica, 2015, reimp. 2016.
 *   Barcelona Planeta 2016
 *   Barcelona : Edicomunicación, [1999]
 *   Madrid Edimat [1998]
 *   Barcelona : Alfaguara : Penguin Random House Grupo Editorial, noviembre de 2015.
 *   [S.l.] : EDITORIAL ALMA, 2018.
 *   [Zaragoza] Edelvives D.L. 2016
 *   Barcelona : Mondadori, 2007.
 *   [Barcelona] Debolsillo 2009.
 *   Buenos Aires : Literatura Random House, 2016.
 *   Barcelona : Debolsillo, 2014.
 *   Buenos Aires : Mondadori, 2012, cop. 2012.
 *   Madrid : Destino, cop. 2009.
 *   Buenos Aires : Debolsillo, 2012, ©2011.
 *   New York : Rayo : Planeta, 2008.
 *   [Madrid] : Espasa-Calpe, [1999]
 *   Barcelona : Planeta-Agostini ; Madrid : Distribución : Marco Ibérica, ©1985.
 *   Madrid Alianza Editorial 2011
 */


test('town name with comma', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Alcobendas, Madrid Libsa D.L. 2016")
    expect(analyzer.publisher()).toBe("Libsa D.L.")
    expect(analyzer.year()).toBe(2016)
}, 20000)

test('colon and final dot', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Reino Unido : Usborne, 2015.")
    expect(analyzer.publisher()).toBe("Usborne")
    expect(analyzer.year()).toBe(2015)
}, 20000)

test('colon, final dot, extra info', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("México D. F. : Fondo De Cultura Ecónomica, 2015, reimp. 2016.")
    expect(analyzer.publisher()).toBe("Fondo De Cultura Ecónomica")
    expect(analyzer.year()).toBe(2015)
}, 20000)

test('no extra chars', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Barcelona Planeta 2016")
    expect(analyzer.publisher()).toBe("Planeta")
    expect(analyzer.year()).toBe(2016)
}, 20000)

test('colon, year inside square brackets', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Barcelona : Edicomunicación, [1999]")
    expect(analyzer.publisher()).toBe("Edicomunicación")
    expect(analyzer.year()).toBe(1999)
}, 20000)

test('multiple colons, comma', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Barcelona : Alfaguara : Penguin Random House Grupo Editorial, noviembre de 2015.")
    expect(analyzer.publisher()).toBe("Alfaguara")
    expect(analyzer.year()).toBe(2015)
}, 20000)

test('city inside square brackets, colon', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("[S.l.] : EDITORIAL ALMA, 2018.")
    expect(analyzer.publisher()).toBe("EDITORIAL ALMA")
    expect(analyzer.year()).toBe(2018)
}, 20000)

test('city inside square brackets, no colon', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("[Zaragoza] Edelvives D.L. 2016")
    expect(analyzer.publisher()).toBe("Edelvives D.L.")
    expect(analyzer.year()).toBe(2016)
}, 20000)

test('colon, comma, final dot', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Barcelona : Mondadori, 2007.")
    expect(analyzer.publisher()).toBe("Mondadori")
    expect(analyzer.year()).toBe(2007)
}, 20000)

test('colon, comma, final dot - city and publisher many words', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Buenos Aires : Literatura Random House, 2016.")
    expect(analyzer.publisher()).toBe("Literatura Random House")
    expect(analyzer.year()).toBe(2016)
}, 20000)

test('colon, comma, extra info', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Buenos Aires : Mondadori, 2012, cop. 2012.")
    expect(analyzer.publisher()).toBe("Mondadori")
    expect(analyzer.year()).toBe(2012)
}, 20000)

test('text between publisher and year', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Madrid : Destino, cop. 2009.")
    expect(analyzer.publisher()).toBe("Destino")
    expect(analyzer.year()).toBe(2009)
}, 20000)

test('extra info with strange char', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Buenos Aires : Debolsillo, 2012, ©2011.")
    expect(analyzer.publisher()).toBe("Debolsillo")
    expect(analyzer.year()).toBe(2012)
}, 20000)

//  *   New York : Rayo : Planeta, 2008.
//  *   [Madrid] : Espasa-Calpe, [1999]
//  *   Barcelona : Planeta-Agostini ; Madrid : Distribución : Marco Ibérica, ©1985.
//  *   Madrid Alianza Editorial 2011
//  */
test('two publishers', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("New York : Rayo : Planeta, 2008.")
    expect(analyzer.publisher()).toBe("Rayo")
    expect(analyzer.year()).toBe(2008)
}, 20000)

test('city and year inside square brackets', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("[Madrid] : Espasa-Calpe, [1999]")
    expect(analyzer.publisher()).toBe("Espasa-Calpe")
    expect(analyzer.year()).toBe(1999)
}, 20000)

test('two sets of city / publisher before year', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Barcelona : Planeta-Agostini ; Madrid : Distribución : Marco Ibérica, ©1985.")
    expect(analyzer.publisher()).toBe("Planeta-Agostini")
    expect(analyzer.year()).toBe(1985)
}, 20000)

test('no extra char, publisher two words', () => {
    const analyzer = new bookItems.HtmlPublisherAndYearAnalyzer("Madrid Alianza Editorial 2011")
    expect(analyzer.publisher()).toBe("Alianza Editorial")
    expect(analyzer.year()).toBe(2011)
}, 20000)

