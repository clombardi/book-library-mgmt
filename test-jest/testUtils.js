const firebase = require("firebase");
const Promise = require('bluebird')

const appModel = require('../app/appModel')

const dbUnitNames = [
    'authors', 'languages', 'genres', 'people', 'publishers', 
    'lastlyEditedBookId', 'books', 'bookLoans', 'lastTestFileNumber'
]

function initFirebase() {
    const firebaseConfig = {
        apiKey: "AIzaSyBxUQT0jBauqPzedBs7aFPPyIhFU0Q6LbQ",
        authDomain: "biblapp-ff6b8.firebaseapp.com",
        databaseURL: "https://biblapp-ff6b8.firebaseio.com",
        storageBucket: "biblapp-ff6b8.appspot.com"
    };
    firebase.initializeApp(firebaseConfig);
}

function closeFirebase() {
    return firebase.app().delete()
}

function copyDatabase(target, source) {
    return firebase.database().ref(source).once('value').then(sourceData => {
        if (sourceData.exists()) {
            return Promise.all(dbUnitNames
                .filter(unitName => sourceData.hasChild(unitName))
                .map(unitName => firebase.database().ref(target + '/' + unitName).set(sourceData.child(unitName).val()))
            ).then(listOfVoids => Promise.resolve())
        } else {
            return Promise.resolve()
        }
    })
}

function createSomeData(dbRoot) {
    appModel.storeCenter.changeDatabaseDomainTo(dbRoot)
    appModel.storeCenter.setDatabase(firebase.database())
    appModel.storeCenter.init()

    return appModel.storeCenter.readFromDisk()
        .then(objectCount => { return appModel.languageStore.addNewEntity("Castellano") })
        .then(entity => { return appModel.languageStore.addNewEntity("Italiano") })
        .then(entity => { return appModel.peopleStore.addNewEntity("Caro") })
        //    bookSpec: { title, authorNames, publisherName, genreName, languageNames, year, pages, notes } 
        .then(entity => { 
            return appModel.bookStore.addNewBook({
                title: 'El Quijote', authorNames: ['Miguel de Cervantes'], 
                genreName: 'Novela', languageNames: ['Castellano'],
                year: 1605, pages: null, notes: null
            })
        })
}

function removeDatabase(dbRoot) {
    return firebase.database().ref(dbRoot).remove()
}

function doDbAction(action, messageGenerator) {
    initFirebase()
    action()
        .then((eventualActionResult) => {
            console.log(messageGenerator(eventualActionResult))
            return closeFirebase()
        })
        .catch(error => {
            console.log(error)
            return closeFirebase()
        })
}

function addDataToMaster() {
    const theDbRoot = 'test_jest_master'
    doDbAction(() => {
        console.log('about to add data to ' + theDbRoot)
        return createSomeData(theDbRoot)
    }, (lastCreatedEntity) => lastCreatedEntity.persistentId())
}

function removeMaster() {
    const theDbRoot = 'test_jest_master'
    doDbAction(() => {
        console.log('about to remove ' + theDbRoot)
        return removeDatabase(theDbRoot)
    }, () => theDbRoot + ' should have been removed')
}

function copyFromMaster() {
    const sourceDb = 'test_jest_master'
    const targetDb = 'test_jest'
    doDbAction(() => {
        console.log('about to copy ' + sourceDb + ' to ' + targetDb)
        return copyDatabase(targetDb, sourceDb)
    }, () => 'the copy should be ready')
}



module.exports.initFirebase = initFirebase
module.exports.closeFirebase = closeFirebase
module.exports.removeDatabase = removeDatabase
module.exports.copyDatabase = copyDatabase


