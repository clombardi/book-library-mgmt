const authorAnalysis = require('../app/textAnalysis/authors')

/*
 Casos de autores
    "author":"Juan Filloy.",
	"author":"Jorge Luis Borges ; Adolfo Bioy Casares.",
	"author":"Jorge Luis Borges, Adolfo Bioy-Casares ; translated by Norman Thomas di Giovanni.",
	"author":"Jorge Luis Borges, Adolfo Bioy-Casares, alias H. Bustos Domecq.",
	"author":"Jorge Luis Borges, Adolfo Bioy Casares.",
	"author":"Jorge Luis Borges con Adolfo Bioy Casares.",
	"author":"Jorge Luis Borges. Hrsg. von Gisbert Haefs und Fritz Arnold",
	"author":"Jorge Luis Borges; Adolfo Bioy Casares. Hrsg. von Gisbert Haefs.",
	"author":"Jorge Luis Borges; Adolfo Bioy Casares. Aus d. Argent. übers. von Liselott Reger ...",
	"author":"Dunlop ; Cortázar.",
	"author":"Carol Dunlop y Julio Cortázar ; dibujos de Stephane Hebert.",
	"author":"Carol Dunlop, Julio Cortázar ; dibujos de Stéphane Hébert.",
	"author":"Carol Dunlop, Julio Cortázar.",
	"author":"José Ortega y Gasset.",
	"author":"José Ortega y Gasset ; introducción Julián Marías.",
	"author":"Ortega y Gasset.",
	"author":"José Ortega y Gasset.",
	"author":"José Ortega y Gassett.",
	"author":"José Ortega y Gasset. Introd. Julián Marías",
	"author":"José Ortega y Gasset ; introducción de Julián Marías.",
	"author":"José Ortega y Gasset ; edición de Domingo Hernández Sánchez.",
	"author":"José Ortega y Gasset ; prólogo de Jordi Llovet.",
	"author":"José Ortega y Gasset. Ed., introd. y notas de Thomas Mermall",
	"author":"Philip K. Dick ; [traducción de Manuel Espín]",
	"author":"Philip K. Dick ; przeł. Michał Ronikier ; [rys. Wojciech Siudmak].",
	"author":"Philip K. Dick ; traducción, Manuel Espín, cedida por Martínez Roca.",
	"author":"Philip K. Dick. Mit e. Nachw. von Stanisław Lem. [Aus d. Amerikan. von Renate Laux]",
	"author":"Philip K. Dick.",
	"author":"by Philip K. Dick.",
	"author":"Philip K. Dick.",
	"author":"Philip K. Dick ; trad. de l'américain par Alain Dorémieux.",
	"author":"Filip K. Dik ; [translated by] A. Lazerchuk.",
	"author":"Philip K. Dick ; postfazione e cura di Carlo Pagetti ; traduzione dall'inglese di Paolo Prezzavento.",
	"author":"[Traducciʹon de Manuel Espʹin.].",
	"author":"Philip K. Dick ; [traducción, Manuel Espín].",
	"author":"Philip K. Dick ; [suomennos: Matti Rosvall].",
	"author":"Philip K. Dick ; [překlad Richard Podaný]",
	"author":"Filip Dik ; [per. s angl. A. Lazarchuka].",
	"author":"Philip K. Dick ; with a new introd. by Michael Bishop.",
	"author":"Philip K. Dick ; mit einem Nachwort von Stanisław Lem ; [translated by Renate Laux].",
	"author":"J.R.R.Tolkien.",
	"author":"J. R. R. Tolkien.",
	"author":"J.R.R. Tolkien ; [traducción de Matilde Horne y Luis Domènech].",
	"author":"J.R.R. Tolkien ; traducción de Matilde Horne y Luis Domènech.",
	"author":"J.R.R. Tolkien ; ilustrado por Alan Lee.",
	"author":"[trad. de Rubén Masera].",
	"author":"J R R Tolkien ; tr. Luis Domènech y Matilde Horne.",
	"author":"Juan Filloy ; traduit de l'espagnol (Argentine) par Céleste Desoille.",
	"author":"Juan Filloy ; translated by Lisa Dillman.",
	"author":"Jorge Luis Borges, Adolfo Bioy Casares (H. Bustos Domecq).",
 */

test('just one author', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Juan Filloy").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Juan Filloy")
}, 20000)

test('one author with dot', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Juan Filloy.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Juan Filloy")
}, 20000)

test('two authors', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges ; Adolfo Bioy Casares").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test('two authors with dot', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges ; Adolfo Bioy Casares.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test('two authors with translator', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges, Adolfo Bioy-Casares ; translated by Norman Thomas di Giovanni.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy-Casares")
}, 20000)

test('two authors with comma and dot', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges, Adolfo Bioy Casares.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test("two authors with 'con'", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges con Adolfo Bioy Casares.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test('two authors with subsequent parentheses', () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges, Adolfo Bioy Casares (H. Bustos Domecq).").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test("one author with German translation", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges. Hrsg. von Gisbert Haefs und Fritz Arnold").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
}, 20000)

test("two authors with German translation", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges; Adolfo Bioy Casares. Hrsg. von Gisbert Haefs.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test("two authors with German text", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Jorge Luis Borges; Adolfo Bioy Casares. Aus d. Argent. übers. von Liselott Reger ...").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Jorge Luis Borges")
    expect(theAuthors[1]).toBe("Adolfo Bioy Casares")
}, 20000)

test("two authors just surname", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Dunlop ; Cortázar.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Dunlop")
    expect(theAuthors[1]).toBe("Cortázar")
}, 20000)

test("two authors with 'y' and collaborator", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Carol Dunlop y Julio Cortázar ; dibujos de Stephane Hebert.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Carol Dunlop")
    expect(theAuthors[1]).toBe("Julio Cortázar")
}, 20000)

test("two authors with 'et'", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Carol Dunlop et Julio Cortázar.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Carol Dunlop")
    expect(theAuthors[1]).toBe("Julio Cortázar")
}, 20000)

test("two authors with comma and collaborator", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Carol Dunlop, Julio Cortázar ; dibujos de Stéphane Hébert.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Carol Dunlop")
    expect(theAuthors[1]).toBe("Julio Cortázar")
}, 20000)

test("two authors with comma and dot, other case", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Carol Dunlop, Julio Cortázar.").rawAuthorNames()
    expect(theAuthors.length).toBe(2)
    expect(theAuthors[0]).toBe("Carol Dunlop")
    expect(theAuthors[1]).toBe("Julio Cortázar")
}, 20000)

test("cortazar translated", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Julio Cortázar; translated from the Spanish by Gregory Rabassa.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Julio Cortázar")
}, 20000)

test("one author including 'y'", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author including 'y' - just surnames", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Ortega y Gasset.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Ortega y Gasset")
}, 20000)

test("one author including 'y' - following text with dot and uppercase - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset. Introd. Julián Marías").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author including 'y' - following text with dot and uppercase - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset. Ed., introd. y notas de Thomas Mermall").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author including 'y' - following text with semicolon - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset ; introducción de Julián Marías.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author including 'y' - following text with semicolon - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset ; edición de Domingo Hernández Sánchez.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author including 'y' - following text with semicolon - 3", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("José Ortega y Gasset ; prólogo de Jordi Llovet.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("José Ortega y Gasset")
}, 20000)

test("one author with one dot", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot and bothering prefix", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("by Philip K. Dick.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("by Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and square bracket - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; [traducción de Manuel Espín]").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and square bracket - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Filip K. Dik ; [translated by] A. Lazerchuk.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Filip K. Dik")
}, 20000)

test("one author with one dot, semicolon and square bracket - 3", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; [traducción, Manuel Espín].").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and square bracket - 4", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; [překlad Richard Podaný]").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; przeł. Michał Ronikier ; [rys. Wojciech Siudmak].").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; traducción, Manuel Espín, cedida por Martínez Roca.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 3", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; trad. de l'américain par Alain Dorémieux.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 4", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; postfazione e cura di Carlo Pagetti ; traduzione dall'inglese di Paolo Prezzavento.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 5", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; with a new introd. by Michael Bishop.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 6", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; mit einem Nachwort von Stanisław Lem ; [translated by Renate Laux].").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot, semicolon and following stuff - 6", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick ; mit einem Nachwort von Stanisław Lem ; [translated by Renate Laux].").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("one author with one dot and then another dot", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Philip K. Dick. Mit e. Nachw. von Stanisław Lem. [Aus d. Amerikan. von Renate Laux]").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Philip K. Dick")
}, 20000)

test("no authors, just square bracketed text", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("[Traducciʹon de Manuel Espʹin.].").rawAuthorNames()
    expect(theAuthors.length).toBe(0)
}, 20000)

test("one author with several dots, no spaces", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("J.R.R.Tolkien.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("J.R.R.Tolkien")
}, 20000)

test("one author with several dots, spaces", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("J. R. R. Tolkien.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("J. R. R. Tolkien")
}, 20000)

test("one author with several dots, semicolon and square brackets", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("J.R.R. Tolkien ; [traducción de Matilde Horne y Luis Domènech].").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("J.R.R. Tolkien")
}, 20000)

test("one author with several dots and semicolon - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("J.R.R. Tolkien ; traducción de Matilde Horne y Luis Domènech.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("J.R.R. Tolkien")
}, 20000)

test("one author with several dots and semicolon - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("J.R.R. Tolkien ; ilustrado por Alan Lee.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("J.R.R. Tolkien")
}, 20000)

test("one author with semicolon - 1", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Juan Filloy ; traduit de l'espagnol (Argentine) par Céleste Desoille.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Juan Filloy")
}, 20000)

test("one author with semicolon - 2", () => {
    const theAuthors = new authorAnalysis.RawAuthorAnalyzer("Juan Filloy ; translated by Lisa Dillman.").rawAuthorNames()
    expect(theAuthors.length).toBe(1)
    expect(theAuthors[0]).toBe("Juan Filloy")
}, 20000)

