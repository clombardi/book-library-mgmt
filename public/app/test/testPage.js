const React = require('react')
const ReactDOM = require('react-dom')

import { prop } from 'ramda'

const _ = require("lodash")
const Promise = require('bluebird')

import Dropzone from 'react-dropzone'

const fileSaver = require("file-saver")
const xlsx = require("xlsx-style")
import {default as ExcelFile, ExcelSheet, ExcelColumn} from 'react-data-export'
const axios = require('axios')

import FormGroup from 'react-bootstrap/lib/FormGroup'
import Button from 'react-bootstrap/lib/Button'
import Tabs from 'react-bootstrap/lib/Tabs'
import Tab from 'react-bootstrap/lib/Tab'

const formComponents = require('../lib/formComponents')


/*
 *  Image test
 ***********************/
class ImageTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            message: "Ingrese un nombre de imagen", imagename: null, version: 0,  
            bookInfo: null, bookMessage: "Ingrese un ID de libro para ver info",
            loadedImage: null, dropzoneMessage: "Arrastre la foto aquí"
        }
        this.createComponent()
    }

    createComponent() {
        this.state.filenameComponent = new formComponents.TextComponent('imagename', 'Nombre de imagen', this)
        this.state.filenameComponent.beForInput()
    }

    forceRedraw() {
        this.setState({ version: this.state.version + 1 })
    }

    render() {
        console.log("render()")
        console.log(this.state.bookInfo)
        const verticalAlignmentStyle = { float: "none", display: "inline-block", verticalAlign: "bottom" }
        return (
            <div className="container">
                <div className="row">
                    {/* image name */}
                    <div className="col-sm-6" style={verticalAlignmentStyle}>
                        {this.state.filenameComponent.buildGroup()}
                    </div>
                    <div className="col-sm-2" style={verticalAlignmentStyle}>
                        <FormGroup>
                        <Button onClick={() => { this.checkFileExistence() }}>
                            ¿Existe?
                        </Button>
                        </FormGroup>
                    </div>
                    <div className="col-sm-2" style={verticalAlignmentStyle}>
                        <FormGroup>
                        <Button onClick={() => { this.downloadImage() }}>
                            Cargar
                        </Button>
                        </FormGroup>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        { this.state.message }
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        {this.imageTag()}
                    </div>
                    <div className="col-sm-6">
                        { this.renderBookInfo() }
                    </div>
                </div>
                <div className="row" style={{marginTop: "40px"}}>
                    <div className="col-sm-6">
                        <div className="row">
                            <Dropzone onDrop={(files) => this.onPhotoDrop(files)}>
                                <p>{this.state.dropzoneMessage}</p>
                            </Dropzone>
                        </div>
                        <div className="row">
                            <Button onClick={() => { this.saveLoadedImage() }} disabled={!this.state.loadedImage}>
                                Grabar
                            </Button>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="row">
                            {this.state.loadedImage
                                ? <img
                                    src={URL.createObjectURL(this.state.loadedImage)}
                                    alt="imagen cargada" style={{ maxWidth: "100%", height: "auto" }}
                                />
                                : null}
                        </div>
                        <div className="row">
                                { prop("name", this.state.loadedImage) }
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderBookInfo() {
        return (
            this.state.bookInfo
                ? [
                    <div className="row" key="info.title">
                        <div className="col-sm-4">Titulo</div><div className="col-sm-8">{this.state.bookInfo.title}</div>
                    </div>,
                    <div className="row" key="info.publisher">
                        <div className="col-sm-4">Editorial</div><div className="col-sm-8">{this.state.bookInfo.publisher}</div>
                    </div>,
                    <div className="row" key="info.authors">
                        <div className="col-sm-4">Autores</div><div className="col-sm-8">{this.state.bookInfo.authors}</div>
                    </div>,
                    <div className="row" key="info.imageExists">
                        <div className="col-sm-4">¿hay imagen?</div>
                        <div className="col-sm-8">{this.state.bookInfo.imageExists ? "Sí" : "No"}</div>
                    </div>,
                    <div className="row" key="info.isTranslation">
                        <div className="col-sm-4">¿es traducción?</div>
                        <div className="col-sm-8">{this.state.bookInfo.isTranslation ? "Sí" : "No"}</div>
                    </div>,
                    <div className="row" key="info.isLent">
                        <div className="col-sm-4">¿está prestado?</div>
                        <div className="col-sm-8">{this.state.bookInfo.isLent ? "Sí" : "No"}</div>
                    </div>
                ]
                : this.state.bookMessage
        )
    }

    onPhotoDrop(files) {
        if (files.length == 1) {
            this.setState({loadedImage: files[0]})
        } else {
            this.setState({dropzoneMessage: "Se acepta un único archivo"})
        }
    }

    checkFileExistence() {
        const imagename = this.state.filenameComponent.valueForServer()
        if (imagename && imagename.length) {
        this.setState({ message: "... analizando ..." })
        return axios.get("/imageExists/" + imagename)
            .then(response => {
                this.setState({ message: "La imagen " + (response.data.exists ? "sí " : "no ") + "existe" })
            })
        } else {
            this.setState({ message: "El nombre de imagen no puede estar vacío" })
        }
    }

    downloadImage() {
        const imagename = this.state.filenameComponent.valueForServer()
        if (imagename && imagename.length) {
            const oldImagename = this.state.imagename
            if (!(oldImagename == imagename)) {
                // Promise
                //     .all([axios.get('/books/' + imagename), axios.get("/imageExists/" + imagename)])
                //     .spread((bookInfoResp, imageExistsResp) => {
                //         const bookInfo = bookInfoResp.data
                //         bookInfo.imageExists = imageExistsResp.data.exists
                //         console.log(bookInfo)
                //         console.log(imageExistsResp.data)
                //         this.setState({ bookInfo, bookMessage: null, imagename: imageExistsResp.data.exists ? imagename : null })
                //     })
                //     .catch((error) => {
                //         console.log(error)
                //         this.setState({ bookInfo: null, bookMessage: "No se reconoce el ID de libro", imagename: null })
                //     })
                axios.get("/imageExists/" + imagename)
                    .then(response => {
                        if (response.data.exists) { this.setState({ imagename: imagename }) }
                        axios
                            .get('/books/' + imagename)
                            .then((response) => {
                                console.log(response.status)
                                this.setState({ bookInfo: response.data, bookMessage: null })
                            })
                            .catch((error) => {
                                console.log(error)
                                this.setState({ bookInfo: null, bookMessage: "No se reconoce el ID de libro" })
                            })
                    })
            }
        }
    }

    saveLoadedImage() {
      console.log(this.state.loadedImage)
        let payload = new FormData()
        payload.append("fileContents", this.state.loadedImage)
        payload.append("filename", "savedImage_" + new Date().getTime())
        axios.post("/bookImage", payload)
            .then((response) => this.setState({loadedImage: null}))
    }

    imageTag() {
        const imagename = this.state.imagename
        if (imagename && imagename.length) {
            return (<img src={"/imageContents/" + imagename} alt="imagen" />)
        } else {
            return null
        }
    }

}


/*
 *  Tab test
 ***********************/
class TabTest extends React.Component {
  render() {
    return (
      <div>
        <Tabs defaultActiveKey={2} id="uncontrolled-tab-example">
          <Tab eventKey={1} title="Tab 1">
            Tab 1 content
          </Tab>
          <Tab eventKey={2} title="Tab 2">
            Tab 2 content
          </Tab>
          <Tab eventKey={3} title="Tab 3" disabled>
            Tab 3 content
          </Tab>
        </Tabs>
      </div>
    )
  }
}



/*
 *  Cookie test
 ***********************/
class StringCodedAttributeListManager {
  constructor(attrSeparator, keyValueSeparator) {
    this._attrSeparator = attrSeparator
    this._keyValueSeparator = keyValueSeparator
    this._keyValuePairs = []
    this._encodedString = null
  }

  parseString(encodedString) {
    this._encodedString = encodedString
    if (encodedString.length) {
      let codedAttributeList = encodedString.split(this._attrSeparator)
      this._keyValuePairs = codedAttributeList.map(codedAttr => {
        let keyAndValue = codedAttr.split(this._keyValueSeparator)
        return { key: keyAndValue[0].trim(), value: keyAndValue[1].trim() }
      })
    } else {
      this._keyValuePairs = []
    }
  }

  valueForKey(theKey) {
    let theComp = this._keyValuePairs.find(kv => kv.key == theKey)
    return theComp ? theComp.value : null
  }

  includesKey(theKey) { return this._keyValuePairs.some(kv => kv.key == theKey) }

  encodedString() { return this._encodedString }
}

class CookieManager {
  constructor() {
    this._analyzer = new StringCodedAttributeListManager(";", "=")
    this.analyzeCookie()
  }

  analyzeCookie() { this._analyzer.parseString(document.cookie) }

  addCookie(key, value, durationInSecs) {
    document.cookie = key + "=" + value + ";max-age=" + durationInSecs
    this.analyzeCookie()
  }

  hasCookie(key) { return this._analyzer.includesKey(key) }

  cookieValue(key) { return this._analyzer.valueForKey(key) }

  rawCompleteCookie() { return this._analyzer.encodedString() }
}


class CookieTest extends React.Component {
  constructor(props) {
    super(props);
    this.state = { version: 0 }
    this.cookieManager = new CookieManager()
  }

  render() {
    return (
      <div>
        <p>
          <button onClick={() => this.setShortLifeCookie()}>Set 15-sec Cookie</button>
          <button style={{ marginLeft: "20px" }} onClick={() => this.setLongLifeCookie()}>Set 10-min Cookie</button>
          <button style={{ marginLeft: "20px" }} onClick={() => this.analyzeCookie()}>Refresh cookie status</button>
        </p>
        <p style={{ marginTop: "20px" }}>
          Complete cookie string:
          <span style={{ color: "blue" }}>{this.completeCookieString()}</span>
        </p>
        <p style={{ marginTop: "20px" }}>
          Short life cookie is set:
          <span style={{ color: "red" }}>{this.shortLifeCookieStatus()}</span>
        </p>
        <p style={{ marginTop: "20px" }}>
          Long life cookie is set:
          <span style={{ color: "red" }}>{this.longLifeCookieStatus()}</span>
        </p>
      </div>
    )
  }

  setShortLifeCookie() {
    this.cookieManager.addCookie("shortLifeCookie", "true", 15)
    this.analyzeCookie()
  }

  setLongLifeCookie() {
    this.cookieManager.addCookie("longLifeCookie", "true", 600)
    this.analyzeCookie()
  }

  completeCookieString() { return this.cookieManager.rawCompleteCookie() }
  shortLifeCookieStatus() { return this.cookieManager.hasCookie("shortLifeCookie") ? "true" : "false" }
  longLifeCookieStatus() { return this.cookieManager.hasCookie("longLifeCookie") ? "true" : "false" }

  analyzeCookie() {
    this.cookieManager.analyzeCookie()
    this.setState({ version: this.state.version + 1 })
  }
}



/*
 *  LocalStorageTest
 ***********************/
class LocalStorageTest extends React.Component {
  constructor(props) {
    super(props);
    this.state = { version: 0 }
  }

  render() {
    return (
      <div>
        <p>
          <button onClick={() => this.setActiveFilters()}>Set active filters</button>
          <button style={{marginLeft: "20px"}} onClick={() => this.setSignal()}>Set signal from bookPage to listBooks</button>
          <button style={{marginLeft: "20px"}} onClick={() => this.removeInfo()}>Remove info</button>
        </p>
        <p style={{ marginTop: "20px" }}>
          Active filters:
          <span style={{ color: "red" }}>{ this.activeFilters() }</span>
        </p>
        <p style={{ marginTop: "20px" }}>
          Signal:
          <span style={{ color: "red" }}>{this.signal()}</span>
        </p>
      </div>
    )
  }

  setActiveFilters() { 
    sessionStorage.setItem("activeFilters", "{autor: {texto: 'Borges', modo: 'Exacto'}}") 
    this.refreshInfo()
  }
  setSignal() { 
    sessionStorage.setItem("comesFromBookPage", "true") 
    this.refreshInfo()
  }

  removeInfo() { 
    sessionStorage.removeItem("activeFilters") 
    sessionStorage.removeItem("comesFromBookPage") 
    this.refreshInfo()
  }

  activeFilters() { return _.defaultTo(sessionStorage.getItem("activeFilters"), "--- not set ---") }
  signal() { return _.defaultTo(sessionStorage.getItem("comesFromBookPage"), "--- not set ---") }
  
  refreshInfo() {
    this.setState({version: this.state.version + 1})
  }
}



/*
 *  XlsxGenerationTest
 ***********************/
class XlsxGenerationTest extends React.Component {
  constructor(props) {
    super(props);
  }

  exampleDataSet() {
    return [
        {
            name: "Johson",
            amount: 30000,
            sex: 'M',
            is_married: true
        },
        {
            name: "Monika",
            amount: 355000,
            sex: 'F',
            is_married: false
        },
        {
            name: "John",
            amount: 250000,
            sex: 'M',
            is_married: false
        },
        {
            name: "Josef",
            amount: 450500,
            sex: 'M',
            is_married: true
        }
    ]
  }

  strToArrBuffer(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);

      for (var i = 0; i != s.length; ++i) {
          view[i] = s.charCodeAt(i) & 0xFF;
      }

      return buf;
  }

  rawXlsxGeneration() {
    this.saveWorkbook(this.testXlsxWorkbook(), "employees.xlsx")
  }

  testXlsxWorkbook() {
    let workbook = xlsx.utils.book_new()
    xlsx.utils.book_append_sheet(workbook, this.testXlsxWorksheet(), "Employee data")
    return workbook
  }

  testXlsxWorksheet() {
    return xlsx.utils.json_to_sheet(this.exampleDataSet())
  }

  buildXlsxWorksheetLowLevel() {
    let worksheet = {}

    worksheet["A1"] = {t: "s", v: "Nombre", s: {"font": {"bold": true, "color": {"rgb": "FF002699"}}}}
    worksheet["B1"] = {t: "s", v: "Importe"}
    worksheet["C1"] = {t: "s", v: "Género"}
    worksheet["D1"] = {t: "s", v: "Casado s/n"}

    worksheet["A2"] = {t: "s", v: "Johnson"}
    worksheet["B2"] = {t: "n", v: 30000}
    worksheet["C2"] = {t: "s", v: "M"}
    worksheet["D2"] = {t: "b", v: true}

    worksheet["A3"] = {t: "s", v: "Monika"}
    worksheet["B3"] = {t: "n", v: 355000}
    worksheet["C3"] = {t: "s", v: "F"}
    worksheet["D3"] = {t: "b", v: false}

    worksheet['!ref'] = "A1:D3"

    const workbook = { SheetNames: [], Sheets: {} }
    const sheetName = "Empleados"
    workbook.SheetNames.push(sheetName);
    workbook.Sheets[sheetName] = worksheet;

    this.saveWorkbook(workbook, "Empleados_bajo_nivel.xlsx")
  }

  saveWorkbook(workbook, filename) {
    // xlsx.writeFile(workbook, "employees.xlsx")
    const wbout = xlsx.write(workbook, { bookType: 'xlsx', bookSST: true, type: 'binary' });
    fileSaver.saveAs(new Blob([(0, this.strToArrBuffer)(wbout)], { type: "application/octet-stream" }), filename);
  }

  saveWorksheet(worksheet, sheetName, filename) {
    let workbook = xlsx.utils.book_new()
    xlsx.utils.book_append_sheet(workbook, worksheet, sheetName)
    this.saveWorkbook(workbook, filename)
  }

  testXlsxFunctions() {
    let address = xlsx.utils.encode_cell({c:1,r:0})
    let newAddress = xlsx.utils.encode_cell({c:4,r:0})
    let worksheet = this.testXlsxWorksheet()
    console.log(address)
    console.log(worksheet[address])
    worksheet[address].v = "importe"
    worksheet[newAddress] = {t: "s", v: "soy okupa"}
    console.log(worksheet[address])
    console.log(worksheet[newAddress])
    console.log(worksheet['!ref'])
    worksheet['!ref'] = "A1:E5"
    console.log(worksheet['!ref'])
    this.saveWorksheet(worksheet, "Empleados", "empleados_con_okupa.xlsx")
  }

  trivialTextFileGeneration() {
    fileSaver.saveAs(new Blob(["hola gente linda", "\n", "como andan"], { type: "text/plain" }), "prueba1.txt");
    console.log("deberia haber grabado")
  }

  render() {
    const botoncito = <Button style={{backgroundColor: "#ffbf80", marginLeft: "20px", marginRight: "20px"}}>Generar .xlsx</Button>
    return (
      <div className="container">
        <div className="panel panel-info">
          <div className="panel-heading">
            <h4>Lo que queremos testear</h4>
          </div>
          <div className="panel-body">
            <div className="row">
              <p>Hola gente linda</p>
            </div>
            <div className="row">
              <ExcelFile element={ botoncito }>
                <ExcelSheet data={this.exampleDataSet()} name="Employees">
                    <ExcelColumn label="Name" value="name" />
                    <ExcelColumn label="Wallet Money" value="amount" />
                    <ExcelColumn label="Gender" value="sex" />
                    <ExcelColumn label="Marital Status" 
                                 value={(col) => col.is_married ? "Married" : "Single"} />
                </ExcelSheet>
              </ExcelFile>
              <Button style={{backgroundColor: "#ffccff", marginLeft: "20px", marginRight: "20px"}} onClick={() => this.rawXlsxGeneration()}>
                Generar .xlsx (bajo nivel)
              </Button>
              <Button style={{backgroundColor: "#ffaaff", marginLeft: "20px", marginRight: "20px"}} onClick={() => this.testXlsxFunctions()}>
                Testeo de funciones librería XLSX
              </Button>
              <Button style={{backgroundColor: "#ff66ff", marginLeft: "20px", marginRight: "20px"}} onClick={() => this.buildXlsxWorksheetLowLevel()}>
                Generación XLSX a bajo nivel
              </Button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}



ReactDOM.render(
  <ImageTest />,
  document.getElementById('reactPage')
);
