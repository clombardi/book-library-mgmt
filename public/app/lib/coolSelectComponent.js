const React = require('react')
const ReactDOM = require('react-dom')
const Typeahead = require('react-bootstrap-typeahead').Typeahead

const comps = require('./formComponents')

import InputGroup from 'react-bootstrap/lib/InputGroup'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import Button from 'react-bootstrap/lib/Button'
import Table from 'react-bootstrap/lib/Table'



const commonBehaviorMixin = {
  setOptionList(list) { 
    const sortedList = Object.assign([], list).sort()
    this._optionList = sortedList.map((name) => { return {id: name, label: name} } ) 
  },
  addOptions(names) {
    let sortedNewNames = Object.assign([], names).sort()
    for (let index = 0; index < this._optionList.length; index++) {
      const option = this._optionList[index];
      if (option.id == sortedNewNames[0]) {
        sortedNewNames.splice(0,1)
      } else if (option.id > sortedNewNames[0]) {
        while (sortedNewNames.length && sortedNewNames[0] < option.id) {
          this._optionList.splice(index, 0, { id: sortedNewNames[0], label: sortedNewNames[0]})
          index++
          sortedNewNames.splice(0,1)
        }
      }
    }
    // if there are names left in sortedNewNames, then they must be added at the end
    sortedNewNames.forEach(name => {
      this._optionList.push({ id: name, label: name })
    })
  },
  // if the name is null or undefined, then there is nothing to add
  addOption(name) { if (name != null) { this.addOptions([name]) } }
}

class TypeaheadComponent extends comps.TextComponent {
  constructor (componentId, controlLabel, selectLabel, reactComponent) {
    super(componentId, controlLabel, reactComponent)
    this._selectLabel = selectLabel
    this._optionList = []
    Object.assign(this, commonBehaviorMixin)
  }

  buildInputComponent() {
    const selectedValues = this.valueForServer() ? [ {id: this.valueForServer(), label: this.valueForServer()} ] : []
    return (
      <Typeahead
        allowNew
        align="justify"
        newSelectionPrefix={ "Crear " + this._selectLabel + " : " }
        selected={ selectedValues }
        options={ this._optionList }
        onInputChange={ (text) => this.setValueFromInside(text) }
        disabled={ this.isDisabled() }
        autoFocus={this.isAutofocus()}
      />
    )
  }

  setValueFromInside(value) { this.setValueFromServer(value) }
}


class MultiTypeaheadComponent extends comps.FormComponent {
  constructor (componentId, controlLabelPlural, controlLabelSingular, selectLabel, reactComponent) {
    super(componentId, controlLabelPlural, reactComponent)
    this._controlLabelSingular = controlLabelSingular
    this._selectLabel = selectLabel
    this._optionList = []
    this._fixedValues = []
    this._typedValue = null
    Object.assign(this, commonBehaviorMixin)
  }

  clearValueForInput() { 
    this._fixedValues = []
    this._typedValue = null
  }
  clearValueForDisplay() { 
    this._fixedValues = []
    this._typedValue = ''
  }

  label() { return this._fixedValues.length ? super.label() : this._controlLabelSingular }
  valueForDisplayComponent() { return this._typedValue }
  bareValue() { return this.value() }
  valueForServer() { return this.value() }
  value() { 
    const fullValue = Object.assign([], this._fixedValues)
    if (this._typedValue && this._typedValue.length) {
      fullValue.push(this._typedValue)
    }
    return fullValue
  }
  valueForTypeahead() { 
    return (this._typedValue && this._typedValue.length) 
        ? [ {id: this._typedValue, label: this._typedValue} ] : []
  }

  setValueFromInside(value) { this.setValueFromServer(value) }
  silentSetValueFromServer(value) { 
    if (!value || value.length == 0) {
      this.clearValueForInput()
    } else if (value.length == 1) {
      this._typedValue = value[0]
      this._fixedValues = []
    } else {
      this._typedValue = null
      this._fixedValues = value
    }
  } 
  setTypedValue(value) { 
    this._typedValue = value 
    this.forceRedraw()
  }
  addTypedToFixed() {
    this._fixedValues.push(this._typedValue)
    this._typedValue = null
    // this.reactComponent().refs[this.componentId()].getInstance().clear()
    this.forceRedraw()
  }
  removeFromFixed(value) { 
    this._fixedValues.splice(this._fixedValues.indexOf(value), 1) 
    this.forceRedraw()
  }

  buildActualInputGroup() {
    return (
      <FormGroup controlId={this.componentId()}>
        <ControlLabel style={this.labelStyle()}>{this.label()}</ControlLabel>
        { this.buildFixedList() }
        { this.buildSelectControl() }
      </FormGroup>
    )
  }

  buildDisplayGroup() {
    if (this._fixedValues.length) {
      return (
        <FormGroup controlId={this.componentId()}>
          <ControlLabel>{this.label()}</ControlLabel>
          { this.buildDisplayList() }
        </FormGroup>
      )
    } else {
      return super.buildDisplayGroup()
    }
  }

  buildSelectControl() {
    const theSelect = (
      <Typeahead
        ref={ this.componentId() }
        allowNew
        align="justify"
        newSelectionPrefix={ "Crear " + this._selectLabel + " : " }
        selected={ this.valueForTypeahead() }
        options={ this._optionList }
        onInputChange={ (text) => this.setTypedValue(text) }
        disabled={ this.isDisabled() }
        autoFocus={this.isAutofocus()}
      />
    );
    const theButton = (
      <InputGroup.Button>
        <Button onClick={(value) => this.addTypedToFixed()}>+ {this._selectLabel}</Button>
      </InputGroup.Button>
    );
    return (
      <InputGroup>
        { theSelect }
        { theButton }
      </InputGroup>
    )
  }

  buildFixedList() {
    return this._fixedValues.length ? this.buildSelectedListFor(this._fixedValues, true) : []
  }

  buildDisplayList() {
    return this.buildSelectedListFor(this._fixedValues, false)
  }

  buildSelectedListFor(givenList, includeRemoveButtons) {
    const self = this
    let displayMode = 'none'
    if (givenList.length) { displayMode = 'block' }

    return (
      <Table condensed style={{display: displayMode, marginBottom: "2px"}}>
        <tbody>
          {
            givenList.map(function(opt) {
              let columns = [<td style={{fontSize: "90%"}} key="value">{opt}</td>]
              if (includeRemoveButtons) {
                columns.push(<td>
                  <Button key="deleteButton" bsStyle="info" bsSize="xsmall" onClick={() => self.removeFromFixed(opt)}>Eliminar</Button>
                </td>)
              }
              return (
                <tr key={opt}>{ columns }</tr>
              )
            })
          }
        </tbody>
      </Table>
    )
  }

}


module.exports.TypeaheadComponent = TypeaheadComponent
module.exports.MultiTypeaheadComponent = MultiTypeaheadComponent
