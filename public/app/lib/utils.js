const React = require('react')
require('date-utils')

import Tooltip from 'react-bootstrap/lib/Tooltip'
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger'

const fieldComparator = function(fieldName) {
	return function(obj1, obj2) {
		if (obj1[fieldName] < obj2[fieldName]) {
			return -1
		} else if (obj1[fieldName] > obj2[fieldName]) {
			return 1
		} else {
			return 0
		}
	}
}

const twoFieldComparator = function(field1, field2) {
	return function(obj1, obj2) {
		const comp1 = fieldComparator(field1)(obj1, obj2)
		if (comp1) {
			return comp1
		} else {
			return fieldComparator(field2)(obj1, obj2)
		}
	}
}

const formattedDate = function(serverDate) {
	const tempDate = new Date()
	tempDate.setTime(serverDate)
	return formattedDateAsDate(tempDate)
}

/* the date picker library used seems to give UTC dates, so that UTC destructors must be used */
const dateToServerTime = function(browserDate) {
    return (browserDate == null) 
        ? null 
        : Date.UTC(browserDate.getUTCFullYear(), browserDate.getUTCMonth(), browserDate.getUTCDate())
}

const serverTimeToDate = function(serverTime) {
    if (serverTime == null) {
        return null
    } else {
        const utcDate = new Date(serverTime)
        return new Date(utcDate.getUTCFullYear(), utcDate.getUTCMonth(), utcDate.getUTCDate())
    }
}

const todayAsServerTime = function() { 
    const rawDate = new Date()
    return Date.UTC(rawDate.getFullYear(), rawDate.getMonth(), rawDate.getDate())
}

const formattedDateAsDate = function(_date) {
    // actual use of date-utils
	return _date.toUTCFormat('DD/MM/YYYY')
}

const nullSafeTimeToDate = function(time) {
    if (!time) { return null }
    const theDate = new Date()
    theDate.setTime(time)
    return theDate
}

const nullSafeDateIsBetween = function(dateToCompare, dateFrom, dateTo) {
    // actual use of date-utils
    const breaksFrom = dateToCompare && dateFrom && dateToCompare.isBefore(dateFrom)
    const breaksTo = dateToCompare && dateTo && dateToCompare.isAfter(dateTo)
    return dateToCompare && !(breaksFrom || breaksTo)
}

const nullSafeStringToNumber = function(stringOrNumber) {
    if (!stringOrNumber && (stringOrNumber != 0)) { return null }
    return Number(stringOrNumber)
}

const nullSafeNumberIsBetween = function(numberToCompare, numberFrom, numberTo) {
    const breaksFrom = numberToCompare && numberFrom && (numberToCompare < numberFrom)
    const breaksTo = numberToCompare && numberTo && (numberToCompare > numberTo)
    return numberToCompare && !(breaksFrom || breaksTo)
}


const nullSafeBooleanCompare = function(bool1, bool2) {
    return ((bool1 && bool2) || (!bool1 && !bool2))
}

/*
 * the idem having the given id in session storage
 * as such items are always strings, transform "null" or "undefined" into null
 */
const sessionStorageItemNullified = function(id) {
    let item = sessionStorage.getItem(id)
    if (item === "null" || item === "undefined") { item = null }
    return item
}

const stringToSpanishCompareValue = function(str) {
    return str.split('').map(letter => charToSpanishCompareValue(letter)).join('')
}

const charToSpanishCompareValue = function(char) {
    const accents =    'áàäéèëíìïóòöúùü'
    const normalized = 'aaaeeeiiiooouuu'
    let compareChar = char.toLowerCase()
    const accentIx = accents.indexOf(compareChar)
    if (accentIx > -1) {
        compareChar = normalized.charAt(accentIx)
    }
    return compareChar
}


/*************************************************** 
 *      Custom subpanel (redefining some colors)
 ***************************************************/
class PaleSubpanel extends React.Component {
    render() {
        return (
            <div className="panel panel-default" style={{marginTop: "40px", borderColor: "#cce6ff"}}>
                <div className="panel-heading" style={{borderColor: "#cce6ff", backgroundColor: "#e6f2ff", color: "#0959aa"}}>
                    <h5 className="panel-title">{ this.props.title }</h5>
                </div>
                <div className="panel-body">
                    { this.props.children }
                </div>
            </div>
        )
    }
}


/*************************************************** 
 *      Component to possibly give a tooltip to some React fragment
 *      Props: tooltipText (can be null)
 ***************************************************/
class PossibleTooltip extends React.Component {
    tooltipText() { return this.props.tooltipText }

    render() {
        if (this.tooltipText()) {
            const tooltip = (<Tooltip id="tooltip">{this.tooltipText()}</Tooltip>);
            return (
                <OverlayTrigger placement="top" overlay={tooltip}>
                    { this.props.children }
                </OverlayTrigger>
            )
        } else {
            return this.props.children 
        }

    }
}

module.exports.fieldComparator = fieldComparator
module.exports.twoFieldComparator = twoFieldComparator
module.exports.formattedDate = formattedDate
module.exports.dateToServerTime = dateToServerTime
module.exports.serverTimeToDate = serverTimeToDate
module.exports.todayAsServerTime = todayAsServerTime
module.exports.formattedDateAsDate = formattedDateAsDate
module.exports.nullSafeDateIsBetween = nullSafeDateIsBetween
module.exports.nullSafeTimeToDate = nullSafeTimeToDate
module.exports.nullSafeStringToNumber = nullSafeStringToNumber
module.exports.nullSafeNumberIsBetween = nullSafeNumberIsBetween
module.exports.sessionStorageItemNullified = sessionStorageItemNullified
module.exports.nullSafeBooleanCompare = nullSafeBooleanCompare
module.exports.stringToSpanishCompareValue = stringToSpanishCompareValue
module.exports.PaleSubpanel = PaleSubpanel
module.exports.PossibleTooltip = PossibleTooltip

