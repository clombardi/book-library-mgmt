const React = require('react')
import Button from 'react-bootstrap/lib/Button'

import { notificationColors, NotificationPanel } from "./notificationPanel"

class FormInquirer {
    constructor(confirmMessage, confirmAction) {
        this._inquiries = []
        this._messages = []
        this._confirmMessage = confirmMessage
        this._confirmAction = confirmAction
    }

    addInquiry(inquiry) { this._inquiries.push(inquiry) }

    computeMessages() { 
        this._messages = this._inquiries.reduce((nessagesSoFar, inquiry) => nessagesSoFar.concat(inquiry.messages()) , [])
    }

    hasIssues() { return this._messages.some((message) => message.isIssue()) }
    hasErrors() { return this._messages.some((message) => message.isError()) }
    hasWarnings() { return this._messages.some((message) => message.isWarning()) }

    errors() { return this._messages.filter(message => message.isError())}
    warnings() { return this._messages.filter(message => message.isWarning())}

    issuePanel() { 
        const self = this

        // colors 
        let theBorderColor = notificationColors.okBorderColor
        let theBackgroundColor = notificationColors.okBackgroundColor
        if (this.hasErrors()) {
            theBorderColor = notificationColors.errorBorderColor
            theBackgroundColor = notificationColors.errorBackgroundColor
        } else if (this.hasIssues()) {
            theBorderColor = notificationColors.warningBorderColor
            theBackgroundColor = notificationColors.warningBackgroundColor
        }

        // title
        const errorTitle = (
            <span style={{ fontSize: "inherit", color: notificationColors.errorLabelColor }}>
                Errores
            </span>
        )
        const warningTitle = (
            <span style={{ fontSize: "inherit", color: notificationColors.warningLabelColor }}>
                Sugerencias
            </span>
        )

        let title = "Los datos están OK"
        if (this.hasErrors() && this.hasWarnings()) {
            title = [errorTitle, <span style={{ fontSize: "inherit" }}> y </span>, warningTitle]
        } else if (this.hasErrors() && !this.hasWarnings()) {
            title = errorTitle
        } else if (!this.hasErrors() && this.hasWarnings()) {
            title = warningTitle
        }

        // items
        const items = self.errors().map(
            (msg) => self.singleIssueItem(msg, notificationColors.errorLabelColor)
        ).concat(self.warnings().map(
            (msg) => self.singleIssueItem(msg, notificationColors.warningLabelColor)
        ))

        // button
        const button = (this.hasIssues() && !this.hasErrors()) 
            ? (<Button onClick={this._confirmAction}>Está bien así, {this._confirmMessage}</Button>)
            : null

        
        return (
            <NotificationPanel
                title={ title }  items={ items } button={ button }
                borderColor={ theBorderColor } backgroundColor={ theBackgroundColor }
            />
        )
    }

    singleIssueItem(issue, labelColor) {
        return ([
            <span key="component" style={{ fontWeight: "bold", color: labelColor, backgroundColor: "inherit" }}>
                {issue.component().label()}
            </span>,
            <span key="text">: {issue.text()}</span>
        ])
    }
}

class InquirerMessage {
    constructor(component, text) {
        this._component = component
        this._text = text
    }

    component() { return this._component }
    text() { return this._text }

    // abstract isIssue()
    isWarning() { return false }
    isError() { return false }
}

class IssueMessage extends InquirerMessage {
    constructor(component, text) {
        super(component, text)
        this._isWarning = false
        this._isError = false
    }
    isIssue() { return true }
    isWarning() { return this._isWarning }
    isError() { return this._isError }
    beWarning() { this._isWarning = true ; return this }
    beError() { this._isError = true ; return this }
}

class SingleComponentInquiry {
    constructor(form, componentId) {
        this._form = form
        this._componentId = componentId
    }

    component() { return this._form.component(this._componentId) }

    valueToCheck() { return this.component().valueForServer() }
}


class FilledComponentInquiry extends SingleComponentInquiry {
    constructor(form, componentId) {
        super(form, componentId)
        this.beText()
    }
    
    messages() {
        const theValue = this.valueToCheck()
        const theMessages = []
        if (this.isEmptyValue()) { theMessages.push(this.createMessage()) }
        return theMessages
    }
    
    beText() { Object.assign(this, textEmptyFieldCheck); return this }
    beNumeric() { Object.assign(this, numericEmptyFieldCheck); return this }
}

const textEmptyFieldCheck = {
    isEmptyValue: function () { return !this.valueToCheck() || !this.valueToCheck().length }
}
const numericEmptyFieldCheck = {
    valueToCheck: function() { return this.component().bareValue() },
    isEmptyValue: function () { 
        return this.valueToCheck() == null || this.valueToCheck() === '' 
    }
}


class MandatoryTextComponent extends FilledComponentInquiry {
    createMessage() {
        const vacioWord = this.component().isFemale() ? "vacía" : "vacío"
        return new IssueMessage(this.component(), "no puede ser " + vacioWord).beError()
    }
}

class AtLeastOneValue extends FilledComponentInquiry {
    createMessage() {
        return new IssueMessage(this.component(), "debe haber al menos un valor").beError()
    }
}

class EmptyComponentWarning extends FilledComponentInquiry {
    createMessage() {
        return new IssueMessage(this.component(), "no se ingresó ningún valor").beWarning()
    }
}



module.exports.FormInquirer = FormInquirer
module.exports.InquirerMessage = InquirerMessage
module.exports.IssueMessage = IssueMessage
module.exports.MandatoryTextComponent = MandatoryTextComponent
module.exports.AtLeastOneValue = AtLeastOneValue
module.exports.EmptyComponentWarning = EmptyComponentWarning
