const React = require('react')
import { defaultTo } from 'lodash'
import 'moment/locale/es';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css'
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment'

const utils = require('./utils')
const comps = require("./formComponents")




/********************************* 
 *      Date component
 *********************************/

class DateComponent extends comps.FormComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = null
  }

  clearValueForInput() { this._value = null }
  clearValueForDisplay() { this._value = null }

  handleDayChange(day) {
    this._value = defaultTo(day, null)
    this.forceRedraw()
  }
  valueForDisplayComponent() { return this._value ? utils.formattedDateAsDate(this._value) : '' }

  bareValue() { return this._value }

  setTodayAsValue() { 
    this.silentSetValueFromServer(utils.todayAsServerTime()); return this 
  }
  silentSetValueFromServer(serverValue) { 
    this._value = utils.serverTimeToDate(serverValue)
  }
  valueForServer() { return utils.dateToServerTime(this._value) }

  buildInputComponent() {
    return (
      <DayPickerInput 
        id={this.componentId()}
        formatDate={formatDate}
        parseDate={parseDate}
        value={this.bareValue()}
        inputProps={{ className: 'form-control' }}
        dayPickerProps={{
          locale: 'es',
          localeUtils: MomentLocaleUtils,
        }}
        placeholder="DD/MM/YYYY"
        onDayChange={(day) => this.handleDayChange(day)}
      />
    )
  }
}


// class OldDateComponent extends comps.FormComponent {
//   constructor(componentId, label, reactComponent) {
//     super(componentId, label, reactComponent)
//     this._value = null
//   }

//   clearValueForInput() { this._value = null }
//   clearValueForDisplay() { this._value = null }

//   handleInputChange(event) {
//     this._value = this.valueToSet(event)
//     this.forceRedraw()
//   }
//   valueToSet(event) {
//     return event ? new Date(event.substr(0, 10)) : null
//   }
//   valueForDisplayComponent() { return this._value ? utils.formattedDateAsDate(this._value) : '' }

//   bareValue() { return this._value }

//   setTodayAsValue() {
//     this.silentSetValueFromServer(utils.todayAsServerTime()); return this
//   }
//   silentSetValueFromServer(serverValue) {
//     this._value = utils.serverTimeToDate(serverValue)
//   }
//   valueForServer() { return utils.dateToServerTime(this._value) }

//   buildInputComponent() {
//     return (
//       <DatePicker
//         id={this.componentId()}
//         value={this._value ? this._value.toISOString() : ''}
//         onChange={(event) => this.handleInputChange(event)}
//       />
//     )
//   }
// }


module.exports.DateComponent = DateComponent
