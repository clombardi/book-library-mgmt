const React = require('react')
const ReactDOM = require('react-dom')

const comps = require('./formComponents')

import InputGroup from 'react-bootstrap/lib/InputGroup'
import FormControl from 'react-bootstrap/lib/FormControl'
import Button from 'react-bootstrap/lib/Button'

class CountComponent extends comps.TextComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = "0"
  }

  buildInputComponent() {
    return (
      <InputGroup>
        <FormControl type="text" 
          name={this.componentId()} 
          disabled={true}
          value={this._value}
          onChange={(event) => this.handleInputChange(event)}
        />
        <InputGroup.Button>
          <Button onClick={(value) => this.incValue()}>+</Button>
          <Button onClick={(value) => this.decValue()}>-</Button>
        </InputGroup.Button>
      </InputGroup>
    )
  }

  clearValueForInput() { this._value = '0' }
  clearValueForDisplay() { this._value = '0' }
  incValue() { this.setValueFromInside((Number(this._value) + 1).toString()) }
  decValue() { this.setValueFromInside((Number(this._value) - 1).toString()) }

  silentSetValueFromServer(value) {
    let newValueAsNumber = value ? Math.max(1,Number(value)) : 1
    super.silentSetValueFromServer(newValueAsNumber.toString())
  }
  valueForServer() {
    return (this._value == null || this._value === '') ? null : Number(this._value)
  }
}


module.exports.CountComponent = CountComponent
