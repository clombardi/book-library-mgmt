const _ = require('lodash')
const React = require('react')
import Button from 'react-bootstrap/lib/Button'
import { withAlert, positions } from 'react-alert'
import { NumberComponent } from './formComponents'

/**
 * Props
 *   listComponent: an object that can be sent: fetchPage(pageNumber) / currentPage()
 *   pageCount: a number
 *   currentPage: a number
 *   domain: a String just for the message `No ${domain} found`.
 *   appliesFilter: a boolean, just to know whether ' that applies the selected filters' must be appended 
 *                  to the `No ${domain} found` message
 */
class PaginationRow extends React.Component {
    constructor(props) { 
        super(props); 
        this.buildFields()
    }

    buildFields() {
        this._pageNumberField = new NumberComponent("paginationRow_pageNumber", "", this)
        this._pageNumberField.setInputSize(5).beForInput()
            .silentSetValueFromServer(this.currentPage())
            .setKeyPressHandler((event, component) => this.handleKeyPressed(event, component))
    }

    // lifecycle
    componentWillReceiveProps(nextProps) {
        this._pageNumberField.silentSetValueFromServer(nextProps.currentPage)
    }

    // data and actions
    listComponent() { return this.props.listComponent; }
    pageCount() { return this.props.pageCount }
    currentPage() { return this.props.currentPage }

    fetchPage(pageNumber) { 
        return this.listComponent().fetchPage(pageNumber) 
    }

    forceRedraw() { this.forceUpdate() }

    // events
    handleKeyPressed(event, textComponent) {
        if (event.charCode == 13) {
            // cancel standard behavior of enter key
            event.preventDefault()
            event.stopPropagation()
            // specific behavior: make this filter active and tell the controller
            // that filters must be reapplied
            const proposedPageNumber = this._pageNumberField.valueForServer()
            if (proposedPageNumber >= 1 && proposedPageNumber <= this.pageCount()) {
                this.fetchPage(proposedPageNumber)
            } else {
                this.showAlert("Número de página inválido")
            }
        } else {
        }

    }

    // rendering
    paginationRowStandardColor() { return "Aquamarine" }
    paginationRowStrongColor() { return "MediumAquamarine" }

    alert() { return this.props.alert }
    showAlert(message) {
        this.alert().error(<span style={{paddingLeft: 10, paddingRight: 10}}>{message}</span>, {
        timeout: 2500,
        position: positions.BOTTOM_CENTER
    }) }

    paginationButtonStyle(backColor) {
        return {
            marginLeft: "20px", backgroundColor: _.defaultTo(backColor, this.paginationRowStandardColor()),
            paddingLeft: "18px", paddingRight: "18px"
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12" style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
                    {this.pageCount() === 0 ? this.contentsIfEmpty() : this.contentsIfNonEmpty() }
                </div>
            </div>
        )
    }

    contentsIfEmpty() {
        let message = "No se encontraron " + this.props.domain
        if (this.props.filterApplies) {
            message = message + " que coincidan con la búsqueda solicitada"
        }
        return (
            <span style={{
                alignSelf: "center", marginLeft: "30px", marginRight: "10px",
                paddingLeft: "25px", paddingRight: "25px", paddingTop: "8px", paddingBottom: "8px",
                borderStyle: "solid", borderWidth: 2, borderColor: this.paginationRowStrongColor(), borderRadius: "10px",
                backgroundColor: "LightCyan"
            }}> 
                {message}
            </span>
        )
    }

    contentsIfNonEmpty() {
        const rowContents = [
            <Button style={this.paginationButtonStyle(this.paginationRowStrongColor())}
                onClick={() => this.fetchPage(1)}
                disabled={this.currentPage() == 1}
                key="first"
            >
                Primera
            </Button>,
            <Button style={this.paginationButtonStyle()}
                onClick={() => this.fetchPage(this.currentPage() - 1)}
                disabled={this.currentPage() == 1}
                key="previous"
            >
                Anterior
            </Button>,
            <span style={{
                alignSelf: "center", marginLeft: "30px", marginRight: "10px",
                paddingLeft: "25px", paddingRight: "25px", paddingTop: "8px", paddingBottom: "8px",
                borderStyle: "solid", borderWidth: 2, borderColor: this.paginationRowStrongColor(), borderRadius: "10px",
                display: "flex", flexDirection: "row", justifyContent: "flex-start", alignItems: "center"
            }}
                key="current"
            >
                <span>Página</span> 
                <span key="pageNumber" style={{ width: 80, marginLeft: 10, marginRight: 10 }}>
                    {this._pageNumberField.buildInputComponent()}
                </span>
                <span>de {this.pageCount()}</span>
            </span>,
            <Button style={this.paginationButtonStyle()}
                onClick={() => this.fetchPage(this.currentPage() + 1)}
                disabled={this.currentPage() == this.pageCount()}
                key="next"
            >
                Siguiente
            </Button>,
            <Button style={this.paginationButtonStyle(this.paginationRowStrongColor())}
                onClick={() => this.fetchPage(this.pageCount())}
                disabled={this.currentPage() == this.pageCount()}
                key="last"
            >
                Última
            </Button>
        ]
        return rowContents
    }

}


module.exports = { PaginationRow: withAlert()(PaginationRow) }