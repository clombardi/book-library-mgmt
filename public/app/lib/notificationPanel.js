
const React = require('react')
import { prop, defaultTo } from "ramda"
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar'


export const notificationColors = {
    warningBorderColor: "#e6e600",
    warningBackgroundColor: "#ffff99",
    warningLabelColor: "#b3b300",
    errorBorderColor: "#e65c00",
    errorBackgroundColor: "#ffe0cc",
    errorLabelColor: "#e63900",
    okBorderColor: "#006699",
    okBackgroundColor: "#99ddff",
}
notificationColors.okLabelColor = notificationColors.okBorderColor

/*
 * props: title / items / button / borderColor / backgroundColor / labelColor
 */
export class NotificationPanel extends React.Component {

    title() { return prop("length", this.props.items) ? this.props.title : null }
    singleLineBody() { return prop("length", this.props.items) ? null : this.props.title }
    items() { return this.props.items }
    button() { return this.props.button }
    borderColor() { return this.props.borderColor }
    backgroundColor() { return this.props.backgroundColor }
    labelColor() { return defaultTo(this.props.borderColor, this.props.labelColor) }

    titleRow() {
        return this.title() 
            ? ( <div className="row" style={{ fontSize: "150%", textAlign: "center" }}>
                    {this.title()}
                </div> )
            : null
    }

    body() { 
        if (this.singleLineBody()) {
            const rowStyle = { fontSize: "150%", color: this.labelColor(), textAlign: "center" }
            return ( <div className="row" style={ rowStyle }> { this.singleLineBody() } </div> )
        } else {
            return (
                <ul>
                    { this.items().map( (item, ix) => (<li key={ix} style={{ fontSize: "larger" }}>{ item }</li>)) }
                </ul>
            )
        }
    }

    buttonRow() {
        return this.button() 
            ? ( <ButtonToolbar style={{ float: "right" }}>{ this.button() }</ButtonToolbar> ) 
            : null
    }

    render() {
        return (
            <div className="panel"
                style={{
                    borderRadius: "25px", borderColor: this.borderColor(),
                    backgroundColor: this.backgroundColor(), borderWidth: "4px"
                }}
            >
                <div className="panel-body" style={{ borderRadius: "25px" }}>
                    {this.titleRow()}
                    <div className="row">
                        {this.body()}
                    </div>
                    {this.buttonRow()}
                </div>
            </div>
        )
    }

}