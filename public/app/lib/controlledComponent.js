let ControllerComponentMixin = function(myBaseClass) { 
    return (
        class extends myBaseClass {
            // controlledComponents()  abstract
            handleInputChange(event) { 
                super.handleInputChange(event)
                if (!this.bareValue()) {      
                    this.controlledComponents().forEach(theComp => theComp.clearValueForInput())
                }
            }
        }
    )
}


let ExplicitControllerComponentMixin = function(myBaseClass) { 
    return (
        class extends ControllerComponentMixin(myBaseClass) {
            setControlledComponents(comps) { 
                this._controlled = []
                comps.forEach(theComp => {
                    this._controlled.push(theComp)
                    if (typeof(theComp.setControllerComponent) == 'function') {
                        theComp.setControllerComponent(this)
                    }
                })
                return this
            }

            controlledComponents() { return this._controlled }
        }
    )
}


let ControlledComponentMixin = function(myBaseClass) {
    return (
        class extends myBaseClass {
            // controllerComponent()  abstract
            label() { return (this.isDisabled() && this.isForDisplay()) ? "" : super.label() }
            isDisabled() { return super.isDisabled() || !this.controllerComponent().bareValue() }
        }
    )
}


let ExplicitControlledComponentMixin = function(myBaseClass) {
    return (
        class extends ControlledComponentMixin(myBaseClass) {
            setControllerComponent(comp) { this._controller = comp }
            controllerComponent() { return this._controller }
        }
    )
}


module.exports.ControlledComponentMixin = ControlledComponentMixin
module.exports.ControllerComponentMixin = ControllerComponentMixin
module.exports.ExplicitControlledComponentMixin = ExplicitControlledComponentMixin
module.exports.ExplicitControllerComponentMixin = ExplicitControllerComponentMixin
 