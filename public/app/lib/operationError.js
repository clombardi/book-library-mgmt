export class OperationError {
    constructor(messages, component = null) {
        this._messages = messages
        this._component = component
    } 

    messages() { return this._messages }
    component() { return this._component }
}