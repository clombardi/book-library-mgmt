const React = require('react')

const utils = require('./utils')
const formComponents = require('./formComponents')

import Form from 'react-bootstrap/lib/Form'



/********************************************************************
 *            Filter abstract superclass
 ********************************************************************/
class Filter {
  constructor(filterId, nameForSelect, nameToDisplay) {
    this._filterId = filterId
    this._nameForSelect = nameForSelect
    this._nameToDisplay = nameToDisplay
    this._isActive = false
  }

  filterId() { return this._filterId }
  nameForSelect() { return this._nameForSelect }
  nameToDisplay() { return this._nameToDisplay }
  
  // rangeFields() abstract

  setReactComponent(component) { this.rangeFields().forEach(field => field.setReactComponent(component)) }

  // editIsInline() abstract
  // stringToDisplay() abstract

  objectToCompare(serverData) { return serverData[this.filterId()] }

  // isSatisfiedBy(serverData) abstract

  editForm() { 
    let fieldRenderFunction = this.editIsInline() 
        ? (field => field.buildInlineGroup()) 
        : (field => field.buildGroup())
    return (
      <Form inline={this.editIsInline()}>
        { this.rangeFields().map(fieldRenderFunction) }
      </Form>
    )
  }

  clearFields() { this.rangeFields().forEach(field => field.clearValueForInput()) }
  componentMemento() { 
    return this.rangeFields().map(function(field) { 
      return {id: field.componentId(), value: field.valueForMemento()} 
    }) 
  }
  setComponentsFromMemento(memento) {
    let self = this
    memento.forEach(function(componentData) {
      const theComponent = self.rangeFields().find(comp => comp.componentId() == componentData.id)
      theComponent.setValueFromMemento(componentData.value)
    })
  }

  activate() { this._isActive = true }
  deactivate() { 
    this._isActive = false 
    this.clearFields()
  }
  isActive() { return this._isActive }

  nameForServer() { return this.filterId() }
  valueForServer() { return this.componentMemento() }
}


/********************************************************************
 *            Standard filters
 ********************************************************************/
class DateRangeFilter extends Filter {
  constructor(filterId, nameForSelect, nameToDisplay, reactComponent) {
    super(filterId, nameForSelect, nameToDisplay)
    this.buildFields(reactComponent)
  }

  buildFields(reactComponent) {
    this._fromField = new formComponents.DateComponent(this.filterId() + "_from", "Desde", reactComponent)
    this._toField = new formComponents.DateComponent(this.filterId() + "_to", "Hasta", reactComponent)
    this._fromField.beForInput()
    this._fromField.beAutofocus()
    this._fromField.setInlineRightMargin("20px")
    this._toField.beForInput()
    this._toField.setInlineRightMargin("0px")
  }

  rangeFields() { return [this._fromField, this._toField] }
  editIsInline() { return true }

  stringToDisplay() {
    let rangeString = null
    if (this._fromField.bareValue() && this._toField.bareValue()) {  // from and to set
      rangeString = "del " + this._fromField.valueForDisplayComponent() 
          + " al " + this._toField.valueForDisplayComponent()
    } else if (this._fromField.bareValue()) {                        // only from set
      rangeString = "desde el " + this._fromField.valueForDisplayComponent() 
    } else if (this._toField.bareValue()) {                          // only to set
      rangeString = "hasta el " + this._toField.valueForDisplayComponent() 
    }
    return this.nameToDisplay() + ": " + rangeString
  }

  isSatisfiedBy(serverData) {
    let dateFromServer = this.objectToCompare(serverData)
    const dateToCompare = utils.nullSafeTimeToDate(dateFromServer)
    return utils.nullSafeDateIsBetween(dateToCompare, this._fromField.bareValue(), this._toField.bareValue())
  }
}


class NumberRangeFilter extends Filter {
  constructor(filterId, nameForSelect, nameToDisplay, reactComponent) {
    super(filterId, nameForSelect, nameToDisplay)
    this.buildFields(reactComponent)
  }

  buildFields(reactComponent) {
    this._fromField = new formComponents.TextComponent(this.filterId() + "_from", "Desde", reactComponent)
    this._toField = new formComponents.TextComponent(this.filterId() + "_to", "Hasta", reactComponent)
    this._fromField.beForInput()
    this._fromField.beAutofocus()
    this._toField.beForInput()
  }

  rangeFields() { return [this._fromField, this._toField] }
  editIsInline() { return true }

  stringToDisplay() {
    let rangeString = null
    if (this._fromField.bareValue() && this._toField.bareValue()) {  // from and to set
      rangeString = "de " + this._fromField.valueForDisplayComponent() 
          + " a " + this._toField.valueForDisplayComponent()
    } else if (this._fromField.bareValue()) {                        // only from set
      rangeString = this._fromField.valueForDisplayComponent() + ' o más'
    } else if (this._toField.bareValue()) {                          // only to set
      rangeString = "hasta " + this._toField.valueForDisplayComponent() 
    }
    return this.nameToDisplay() + ": " + rangeString
  }

  isSatisfiedBy(serverData) {
    let numberFromServer = this.objectToCompare(serverData)
    const numberToCompare = utils.nullSafeStringToNumber(numberFromServer)
    const numberFrom = utils.nullSafeStringToNumber(this._fromField.bareValue())
    const numberTo = utils.nullSafeStringToNumber(this._toField.bareValue())
    return utils.nullSafeNumberIsBetween(numberToCompare, numberFrom, numberTo)
  }
}


class BooleanFilter extends Filter {
  constructor(filterId, nameForSelect, nameToDisplay, reactComponent) {
    super(filterId, nameForSelect, nameToDisplay)
    this.buildField(reactComponent)
  }

  buildField(reactComponent) {
    this._filterField = new formComponents.CheckboxComponent(this.filterId() + "_check", "", reactComponent)
    this._filterField.beForInput()
    this._filterField.beAutofocus()
  }

  rangeFields() { return [this._filterField] }
  editIsInline() { return true }

  stringToDisplay() {
    let rangeString = this._filterField.bareValue() ? "sí" : "no"
    return this.nameToDisplay() + ": " + rangeString
  }

  filterValueToCompare() { return this._filterField.bareValue() }

  isSatisfiedBy(serverData) {
    return utils.nullSafeBooleanCompare(this.objectToCompare(serverData), this.filterValueToCompare() )
  }
}


class TextFilter extends Filter {
  constructor(filterId, nameForSelect, nameToDisplay, reactComponent) {
    super(filterId, nameForSelect, nameToDisplay)
    this.buildFields(reactComponent)
  }

  buildFields(reactComponent) {
    let theSearchStringClass = this.searchStringFieldClass()
    let theSearchModeClass = this.searchModeFieldClass()
    this._searchStringField = new theSearchStringClass (
      this.filterId() + "_searchString", "Texto a buscar", reactComponent
    )
    this._searchModeField = new theSearchModeClass (
      this.filterId() + "_searchMode", "", reactComponent,
      [ {value: "contains", label: "Tiene"}, {value: "start", label: "Empieza"}, 
        {value: "end", label: "Termina"}, {value: "exact", label: "Exacto"} ]
    )
    this._searchStringField.beForInput()
    this._searchStringField.beAutofocus()
    this._searchModeField.beForInput()
    this._searchModeField.beNotInline()
  }

  searchStringFieldClass() { return formComponents.TextComponent }
  searchModeFieldClass() { return formComponents.RadioButtonGroupComponent }

  rangeFields() { return [this._searchStringField, this._searchModeField] }
  editIsInline() { return false }

  searchModeForDisplayComponent() { 
    let label = this._searchModeField.valueForDisplayComponent() 
    if (label == "Empieza" || label == "Termina") {
      label = label + " con"
    }
    return label
  }

  valueToDisplay() {
    return this.searchModeForDisplayComponent()   
        + ' "' + this._searchStringField.valueForDisplayComponent() + '"' 
  }

  stringToDisplay() {
    return this.nameToDisplay() 
        + ': ' + this.valueToDisplay()
  }

  serverStringSatisfies(serverString) {
    // null server strings do not match any condition
    if (serverString == null) { return false }

    const searchString = utils.stringToSpanishCompareValue(this._searchStringField.bareValue())
    const comparableServerString = utils.stringToSpanishCompareValue(serverString)
    if (this._searchModeField.bareValue() == 'contains') {
      return comparableServerString.indexOf(searchString) > -1
    } else if (this._searchModeField.bareValue() == 'start') {
      return comparableServerString.startsWith(searchString)
    } else if (this._searchModeField.bareValue() == 'end') {
      return comparableServerString.endsWith(searchString)
    } else if (this._searchModeField.bareValue() == 'exact') {
      return comparableServerString == searchString
    } else {
      return true
    }
  }
  
  isSatisfiedBy(serverData) {
    return this.serverStringSatisfies(this.objectToCompare(serverData))
  }
}


module.exports.Filter = Filter
module.exports.DateRangeFilter = DateRangeFilter
module.exports.NumberRangeFilter = NumberRangeFilter
module.exports.BooleanFilter = BooleanFilter
module.exports.TextFilter = TextFilter
