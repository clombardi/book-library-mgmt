const React = require('react')

const utils = require('../lib/utils')

import FormGroup from 'react-bootstrap/lib/FormGroup'
import Row from 'react-bootstrap/lib/Row'
import ControlLabel from 'react-bootstrap/lib/ControlLabel'
import FormControl from 'react-bootstrap/lib/FormControl'
import Checkbox from 'react-bootstrap/lib/Checkbox'
import Radio from 'react-bootstrap/lib/Radio'



/********************************* 
 *      Generic components
 *********************************/

const inputComponentMixin = { 
  isForDisplay() { return false },
  buildGroup: function() { return this.buildInputGroup() },
  buildReactOwnComponent: function() { return this.buildReactOwnInputComponent() },
  buildInlineGroup: function() { return this.buildInlineInputGroup() },
  clearValue: function() { return this.clearValueForInput() }
}

const displayComponentMixin = { 
  isForDisplay() { return true },
  buildGroup: function() { return this.buildDisplayGroup() },
  buildReactOwnComponent: function() { return this.buildReactOwnDisplayComponent() },
  buildInlineGroup: function() { return this.buildInlineDisplayGroup() },
  clearValue: function() { return this.clearValueForDisplay() }
}

const nullValueForSelect = "0"


class FormComponent {
  constructor(componentId, label, reactComponent) {
    this._componentId = componentId
    this._label = label
    this._reactComponent = reactComponent
    this._disabled = false
    this._inlineRightMargin = null
    this._isFemale = false
    this._isAutofocus = false
    this._tooltipText = null
    this._isDiscrete = false
  }
  componentId() { return this._componentId }
  label() { return this._label }
  labelForInput() { return this.label() }
  labelForDisplay() { return this.label() }
  reactComponent() { return this._reactComponent }
  isDisabled() { return this._disabled }
  isAutofocus() { return this._isAutofocus }
  isFemale() { return this._isFemale }
  inlineRightMargin() { return this._inlineRightMargin ? this._inlineRightMargin : "40px" }

  setLabel(newLabel) { this._label = newLabel; return this }
  setReactComponent(component) { this._reactComponent = component; return this }
  beForInput() { Object.assign(this, inputComponentMixin); return this }
  beForDisplay() { Object.assign(this, displayComponentMixin); return this }
  beDisabled() { this._disabled = true; return this }
  beEnabled() { this._disabled = false; return this }
  beAutofocus() { this._isAutofocus = true; return this }
  beFemale() { this._isFemale = true; return this; return this }
  beDiscrete() { this._isDiscrete = true; return this }
  setInlineRightMargin(value) { this._inlineRightMargin = value ; return this }
  setTooltipText(text) { this._tooltipText = text; return this }


  // clearValue() abstract, depending on mixin

  // bareValue() abstract
  // clearValueForInput() abstract
  // clearValueForDisplay() abstract
  // valueForDisplayComponent() abstract

  // silentSetValueFromServer(value) abstract
  // valueForServer() abstract
  setValueFromServer(_value) {
    this.silentSetValueFromServer(_value)
    this.forceRedraw()
  }

  valueForMemento() { return this.valueForServer() }
  setValueFromMemento(_value) { this.silentSetValueFromServer(_value) }

  labelStyle() { return this._isDiscrete ? { fontWeight: "normal" } : {} }

  buildInputGroup() {
    return (
      <utils.PossibleTooltip tooltipText={this._tooltipText} key={this.componentId()}>
        {this.buildActualInputGroup()}
      </utils.PossibleTooltip>
    )
  }

  buildActualInputGroup() {
    return (
      <FormGroup controlId={this.componentId()}>
        <ControlLabel style={this.labelStyle()}>{this.labelForInput()}</ControlLabel>
        { this.buildInputComponent() }
      </FormGroup>
    )
  }

  buildInlineInputGroup() {
    return (
      <utils.PossibleTooltip tooltipText={this._tooltipText} key={this.componentId()}>
        {this.buildActualInlineInputGroup()}
      </utils.PossibleTooltip>
    )
  }

  buildActualInlineInputGroup() {
    return (
      <FormGroup controlId={this.componentId()} style={{ marginRight: this.inlineRightMargin() }}>
        <ControlLabel style={Object.assign(this.labelStyle(), {marginRight: "10px"})} >{this.label()}</ControlLabel>
        { this.buildInputComponent() }
      </FormGroup>
    )
  }

  buildReactOwnInputComponent() {
    return (
      <utils.PossibleTooltip tooltipText={this._tooltipText} key={this.componentId()}>
        {this.buildInputComponent()}
      </utils.PossibleTooltip>
    )
  }

  // buildInputComponent() abstract

  buildDisplayGroup() {
    return (
      <FormGroup controlId={this.componentId()} style={{ marginBottom: "20px" }}>
        <ControlLabel>{this.labelForDisplay()}</ControlLabel>
        <Row style={{ marginTop: "0px" }}>
          { this.buildDisplayComponent() }
        </Row>
      </FormGroup>
    )
  }

  buildInlineDisplayGroup() {
    return (
      <FormGroup controlId={this.componentId()} style={{ marginBottom: "20px", marginRight: "40px" }}>
        <ControlLabel style={{marginRight: "10px"}}>{this.label()}</ControlLabel>
        <Row style={{ marginTop: "0px" }}>
          { this.buildDisplayComponent() }
        </Row>
      </FormGroup>
    )
  }

  buildReactOwnDisplayComponent() {
    <utils.PossibleTooltip tooltipText={this._tooltipText} key={this.componentId()}>
      {this.buildDisplayComponent()}
    </utils.PossibleTooltip>
  }


  buildDisplayComponent() {
    return <span style={{ paddingLeft: "15px" }}>{this.valueForDisplayComponent()}</span>
  }

  buildComponentInRow(widthSpec) {
    return (
      <div className={widthSpec}>
        { this.buildGroup() }
      </div>
    )
  }

  // handleInputChange(event) abstract

  // buildGroup() abstract, depending on mixin
  // buildInlineGroup() abstract, depending on mixin

  forceRedraw() {
    if ((this._reactComponent) && (typeof(this._reactComponent.forceRedraw) == 'function')) {
      this._reactComponent.forceRedraw()
    }
  }
}


class TextComponent extends FormComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = null
    this._inputSize = null
    this._keyPressHandler = null
    this._placeholder = null
    this._customControlStyle = null
  }

  placeholder() { return this._placeholder ? this._placeholder : "" }
  customControlStyle() { return this._customControlStyle ? this._customControlStyle : {}}

  setInputSize(size) { this._inputSize = size; return this }
  setPlaceholder(string) { this._placeholder = string; return this }
  setCustomControlStyle(style) { this._customControlStyle = style; return this }

  /* 
   * the handler is given two arguments: the keyPress event, and the component
   */
  setKeyPressHandler(handler) { this._keyPressHandler = handler; return this }

  inputSize() { return this._inputSize }

  clearValueForInput() { this._value = '' }
  clearValueForDisplay() { this._value = null }
  valueForDisplayComponent() { return this._value }

  bareValue() { return this._value }
  valueForInput() { return (this._value == null) ? '' : this._value }

  handleInputChange(event) { 
    this._value = this.valueToSet(event)
    this.forceRedraw()
  }
  handleKeyPress(event) {
    if (this._keyPressHandler) {
      this._keyPressHandler(event, this)
    }
  }
  valueToSet(event) { return event.target.value }

  setValueFromInside(value) { this.setValueFromServer(value) }
  silentSetValueFromServer(value) { 
    this._value = (value ? value : null) 
    return this
  }
  valueForServer() { 
    return (this._value == null || this._value == "") ? null : this._value
  }

  inputComponentType() { return "text" }
  buildInputComponent() {
    return (
      <FormControl type={this.inputComponentType()} style={this.customControlStyle()}
        name={this.componentId()} 
        size={this.inputSize()}
        disabled={this.isDisabled()}
        value={this.valueForInput()}
        onChange={(event) => this.handleInputChange(event)}
        onKeyPress={(event) => this.handleKeyPress(event)}
        autoFocus={ this.isAutofocus() }
        placeholder={ this.placeholder() }
      />
    )
  }

}


class NumberComponent extends TextComponent {
  silentSetValueFromServer(value) { 
    this._value = (value == null ? null : value) 
    return this
  }
  valueForServer() { 
    return (this._value == null || this._value === '') ? null : Number(this._value) 
  }
  inputComponentType() { return "number" }
}


class TextAreaComponent extends TextComponent {
  constructor(componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = ''
  }

  // text area input does not like null value
  // clearValueForDisplay() { this._value = '' }
  // silentSetValueFromServer(value) { this._value = value ? value.toString() : '' }

  buildInputComponent() {
    return (
      <FormControl componentClass="textarea" rows="5"
        name={this.componentId()} 
        disabled={this.isDisabled()}
        value={this.valueForInput()}
        onChange={(event) => this.handleInputChange(event)}
        autoFocus={ this.isAutofocus() }
      />
    )
  }

  buildDisplayComponent() {
    return (
      <FormControl componentClass="textarea" rows="5"
        name={this.componentId()} 
        disabled={true}
        value={this.valueForInput()}
      />
    )
  }

  // must rewrite since "display" component is in fact a disabled input display
  buildDisplayGroup() {
    return (
      <FormGroup controlId={this.componentId()}>
        <ControlLabel>{this.label()}</ControlLabel>
        { this.buildDisplayComponent() }
      </FormGroup>
    )
  }
}



class CheckboxComponent extends FormComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = false
  }

  isInGroup() { return false }

  clearValueForInput() { this._value = false }
  clearValueForDisplay() { this._value = false }
  valueForDisplayComponent() { 
    if (this.isDisabled()) {
      return ""
    } else {      
      return this._value 
        ? <span className="glyphicon glyphicon-ok" style={{paddingLeft: "10px", color: "green"}}></span>
        : <span className="glyphicon glyphicon-remove" style={{paddingLeft: "10px", color: "red"}}></span>
    }
  }

  handleInputChange(event) { 
    this._value = event.target.checked
    this.forceRedraw()
  }

  setValueFromInside(value) { this.setValueFromServer(value) }
  silentSetValueFromServer(value) { 
    this._value = ((value == null) ? false : value) 
    return this
  }
  valueForServer() { return this._value }
  bareValue() { return this._value }

  setValueFromMemento(_value) { this.silentSetValueFromServer(_value) }

  buildInputComponent() {
    let checkboxText = this.isInGroup() ? this.label() : ''
    let leftMarginValue = this.isInGroup() ? "20px" : "2px"
    return (
      <Checkbox 
        inline={this.isInGroup()}
        style={{marginTop: "0px", marginLeft: leftMarginValue}}
        name={this.componentId()} 
        disabled={this.isDisabled()}
        checked={this._value}
        onChange={(event) => this.handleInputChange(event)}
        autoFocus={ this.isAutofocus() }
       >
       { checkboxText }
       </Checkbox>
    )
  }

  buildActualInputGroup() {
    return (
      <FormGroup controlId={this.componentId()}>
        <ControlLabel style={Object.assign(this.labelStyle(), {marginBottom: "0px"})}>{this.label()}</ControlLabel>
        { this.buildInputComponent() }
      </FormGroup>
    )
  }

  buildDisplayComponent() {
    let theLabel = this.isInGroup() ? ( <span style={{ marginLeft: "5px" }}>{this.label()}</span> ) : []
    return (
      [ <span style={{ marginLeft: "20px" }}>{this.valueForDisplayComponent()}</span>, theLabel ]
    )
  }

}

class InGroupCheckboxComponent extends CheckboxComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
  }

  isInGroup() { return true }
}


class RadioButtonGroupComponent extends FormComponent {
  constructor (componentId, label, reactComponent, options) {
    super(componentId, label, reactComponent)
    this._options = options
    this.clearValueForInput()
  }

  firstValue() { return this._options[0].value }
  labelForValue(theValue) { 
    return (this._options.find(anOption => anOption.value == theValue)).label
  }
  labelForCheckedOption() { return this.labelForValue(this.bareValue()) }

  beInline() { this._inline = true }
  beNotInline() { this._inline = false }
  isInline() { return this._inline }

  clearValueForInput() { this._value = this.firstValue() }
  clearValueForDisplay() { this._value = this.firstValue() }
  valueForDisplayComponent() { return this.labelForCheckedOption() }

  handleInputChange(event) { 
    this._value = event.target.value
    this.forceRedraw()
  }

  setValueFromInside(value) { this.setValueFromServer(value) }
  silentSetValueFromServer(value) { 
    this._value = ((value == null) ? null : value) 
    return this
  }
  valueForServer() { return this._value }
  bareValue() { return this._value }

  isFirstOption(value) { return value === this._options[0].value }

  buildInputComponent() {
    const self = this
    return this._options.map(option => 
      <Radio 
          key={option.value}
          inline={this.isInline()}
          name={this.componentId()} 
          value={option.value}
          checked={this.bareValue() == option.value}
          disabled={this.isDisabled()}
          onChange={(event) => this.handleInputChange(event)}
          autoFocus={ this.isAutofocus() && this.isFirstOption(option.value) }
        >
        { option.label }
      </Radio>
    )
  }

  buildDisplayComponent() {
    return (
      <span style={{ marginLeft: "20px" }}>{this.valueForDisplayComponent()}</span>
    )
  }

}



module.exports.FormComponent = FormComponent
module.exports.TextComponent = TextComponent
module.exports.NumberComponent = NumberComponent
module.exports.TextAreaComponent = TextAreaComponent
module.exports.CheckboxComponent = CheckboxComponent
module.exports.InGroupCheckboxComponent = InGroupCheckboxComponent
module.exports.RadioButtonGroupComponent = RadioButtonGroupComponent
module.exports.nullValueForSelect = nullValueForSelect


