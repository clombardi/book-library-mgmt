const React = require('react')

import ReactTimeout from 'react-timeout'

class ExternalSearchStatus {
    constructor() { this.reset() }

    // operations
    beginSearch() { this.reset(); this._isSearching = true; return this }
    endSearchOk() { this._isSearching = false; this._hasErrors = false; return this }
    endSearchWithError(msg) {
        this._isSearching = false
        this._hasErrors = true
        this._errorMessage = msg
        return this
    }
    reset() {
        this._isSearching = false
        this._hasErrors = false
        this._errorMessage = null
        return this
    }

    // info
    isSearching() { return this._isSearching }
    hasErrors() { return this._hasErrors }
    searchErrorMessage() { return this._errorMessage }
}


/**
 * Props: isWaitingForResponse / isError / errorMessage
 */
class ExternalOperationMessage extends React.Component {
    constructor(props) {
        super(props)
        this._isTurnedOff = false
    }
    mustShowMessage() { return this.isWaitingForResponse() || this.isError() }
    isError() { return this.props.status.hasErrors() }
    isWaitingForResponse() { return this.props.status.isSearching() }
    errorMessage() { return this.props.status.searchErrorMessage() }

    turnOff() { this.props.status.reset() }

    componentDidUpdate() { 
        if (this.mustShowMessage()) {
            const timeOutFn = () => {
                this.turnOff()
                this.forceUpdate()
            }
            this.props.setTimeout(timeOutFn, 10000)
        }
    }

    /* from FormInquirer.issuePanel
        const errorBorderColor = "#e65c00"
        const errorBackgroundColor = "#ffe0cc"
        const errorLabelColor = "#e63900"
        const okBorderColor = "#006699"
        const okBackgroundColor = "#99ddff"
        const okLabelColor = okBorderColor

        this._panelBorderColor = "#bce8f1"
        this._panelHeadingBackgroundColor = "#d9edf7"
        this._panelHeadingTextColor = "#31708f"

    */

    borderColor() { return this.isError() ? "#e65c00" : "#bce8f1" }
    backgroundColor() { return this.isError() ? "#ffe0cc" : "#d5f6f6" }
    textColor() { return this.isError() ? "#e63900" : "#24a8a8" }

    colorAndBorderStyle() { 
        return {
            borderWidth: "0px", borderColor: this.borderColor(), borderStyle: "solid",
            borderRadius: "10px",
            backgroundColor: this.backgroundColor(), color: this.textColor()
        }
    }

    marginAndPaddingStyle() {
        const horizPadding = this.isError() ? "20px" : "60px"
        return { 
            paddingTop: "10px", paddingBottom: "10px", paddingLeft: horizPadding, paddingRight: horizPadding
        }
    }

    textStyle() {
        const theFontSize = this.isError() ? "14px" : "16px"
        return { fontSize: theFontSize,  textAlign: "center" }
    }

    message() {
        if (this.isWaitingForResponse()) {
            return "Buscando información"
        } else if (this.isError()) {
            return this.errorMessage() ? this.errorMessage() : "Error al buscar información"
        }
    }

    render() {
        if (this.mustShowMessage()) {
            const theStyle = Object.assign(this.colorAndBorderStyle(), this.marginAndPaddingStyle(), this.textStyle())
            return ( <div style={theStyle}>{ this.message() }</div> )
        } else {
            this._isTurnedOff = false
            return null
        }
    }
}



module.exports.ExternalSearchStatus = ExternalSearchStatus
module.exports.ExternalOperationMessage = ReactTimeout(ExternalOperationMessage)