const React = require('react')

class FilterView extends React.Component {
    page() { return this.props.page }
    filters() { return this.page().filters() }
    filter(id) { return this.page().filter(id) }
}


/*
 * What expects from page: filters() / activeFilters() / recomputeList()
 */
class FilterController {
    constructor(page) { 
        this._page = page 
        this._view = null
    }
    page() { return this._page }
    filters() { return this.page().filters() }
    filter(id) { return this.page().filter(id) }
    activeFilters() { return this.page().activeFilters() }
    view() { return this._view }
    setView(reactComponent) { this._view = reactComponent }

    reapplyFilters() { return this.page().recomputeList() }

    saveFilterState() {
        const activeFiltersMemento = this.activeFilters().map(function (filter) {
            return { filterId: filter.filterId(), components: filter.componentMemento() }
        })
        sessionStorage.setItem("activeFilters", JSON.stringify(activeFiltersMemento))
    }

    clearFilterState() { sessionStorage.removeItem("activeFilters") }

    restoreFilterState() {
        const self = this
        const activeFiltersString = sessionStorage.getItem("activeFilters")
        if (activeFiltersString) {
            const activeFiltersData = JSON.parse(activeFiltersString)
            activeFiltersData.forEach(function (filterData) {
                const theFilter = self.filters().find(aFilter => aFilter.filterId() == filterData.filterId)
                theFilter.activate()
                theFilter.setComponentsFromMemento(filterData.components)
            })
        }
    }

    clearFilters() { this.filters().forEach(filter => filter.deactivate()) }

    forceRedraw() { if (this.view()) { this.view().forceUpdate() } }
}

module.exports.FilterView = FilterView
module.exports.FilterController = FilterController
