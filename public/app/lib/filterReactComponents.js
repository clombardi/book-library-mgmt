const React = require('react')
const formComponents = require('./formComponents')

import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar'
import Button from 'react-bootstrap/lib/Button'
import Table from 'react-bootstrap/lib/Table'
import Form from 'react-bootstrap/lib/Form'
import FormControl from 'react-bootstrap/lib/FormControl'


/********************************************************************
 *            FilterSelector
 * 
 * Props
 * - pageComponent, expected to understand
 *      - filters()
 *      - activeFilters()
 *      - recomputeList()
 ********************************************************************/
class FilterSelector extends React.Component {
  constructor(props) {
    super(props);
    let theSelectField = new SelectFilterComponent("filterSelector", "", this)
    theSelectField.beForInput()
    this.state = { version: 1, selectField: theSelectField }
	}	


  /***    Data    ***/
  filters() { return this.props.pageComponent.filters() }

  activeFilters() { return this.props.pageComponent.activeFilters() }


  /***    Control / actions    ***/
  chosenFilter() {
    return this.state.selectField.chosenFilter()
  }

  clearChosenFilter() {
    this.state.selectField.clearValueForInput()
  }

  activateChosenFilter() {
    this.chosenFilter().activate()
    this.clearChosenFilter() 
    this.forceFullRedraw()
  }

  deactivateFilter(_filterId) {
    let theFilter = this.filters().find(aFilter => (aFilter.filterId() == _filterId))
    theFilter.deactivate()
    this.forceFullRedraw()
  }

  silentClearFilters() {
    this.filters().forEach(filter => filter.deactivate())
  }


  /***    save/restore filter state    ***/
  restoreFilterState() {
    this.silentRestoreFilterState()
    this.forceFullRedraw()
  }

  silentRestoreFilterState() {
    const self = this
    const activeFiltersString = sessionStorage.getItem("activeFilters")
    if (activeFiltersString) {
      const activeFiltersData = JSON.parse(activeFiltersString)
      activeFiltersData.forEach(function(filterData) {
        const theFilter = self.filters().find(aFilter => aFilter.filterId() == filterData.filterId)
        theFilter.activate()
        theFilter.setComponentsFromMemento(filterData.components)
      })
    }
  }

  saveFilterState() {
    const activeFiltersMemento = this.activeFilters().map(function (filter) {
      return { filterId: filter.filterId(), components: filter.componentMemento() }
    })
    sessionStorage.setItem("activeFilters", JSON.stringify(activeFiltersMemento)) 
  }

  removeFilterState() { sessionStorage.removeItem("activeFilters") }


  /***    Rendering    ***/
  forceRedraw() { this.setState({version: this.state.version + 1}) }

  forceFullRedraw() {
    this.forceRedraw()
    this.props.pageComponent.recomputeList()    
  }

  filterSelectionRow() {
    return (
      [
        <div className="col-sm-2" key="label">
          Nuevo filtro
        </div>
        ,
        <div className="col-sm-2" key="input">
          <Form inline>
            { this.state.selectField.buildInlineGroup() }
          </Form>
        </div>
      ]
    )
  }

  chosenFilterRow() {
    return (
      [
        <div className="col-sm-2" key="name">
          { this.chosenFilter().nameToDisplay() }
        </div>
        ,
        <div className="col-sm-7" key="edit">
          { this.chosenFilter().editForm() }
        </div>
        ,
        <div className="col-sm-3" key="buttons">
          <ButtonToolbar>
            <Button onClick={() => { this.chosenFilter().clearFields() ; this.clearChosenFilter() } } style={{backgroundColor: "#66e0ff"}}>
              Cancelar
            </Button>
            <Button onClick={() => this.activateChosenFilter() } style={{backgroundColor: "#5bd778"}}>
              Agregar
            </Button>
          </ButtonToolbar>
        </div>
      ]
    )
  }

  activeFilterTable() {
    let theActiveFilters = this.activeFilters()
    if (theActiveFilters.length) {
      return (
        <Table condensed><tbody>
          { theActiveFilters.map(aFilter => 
              <tr key={ aFilter.filterId() }>
                <td key="displayString">{ aFilter.stringToDisplay() }</td>
                <td key="removeButton">
                  <Button onClick={() => this.deactivateFilter(aFilter.filterId()) } style={{backgroundColor: "#4d94ff"}}>
                    Quitar
                  </Button>
                </td>
              </tr>
            )
          }
        </tbody></Table>
      )
    } else {
      return null
    }
  }

  render() {
    let newFilterRow = null
    if (this.chosenFilter()) {
      newFilterRow = this.chosenFilterRow()
    } else {
      newFilterRow = this.filterSelectionRow()
    }
    let activeFilterRow = this.activeFilterTable()
    if (activeFilterRow) {
      activeFilterRow = <div className="row">{ activeFilterRow }</div>
    } else {
      activeFilterRow = []
    }
  	return (
			<div className="well" style={{marginBottom: "0px", backgroundColor: "#e6fefe"}}> {/* #e6f7ff" */}
        { activeFilterRow }
				<div className="row">
          { newFilterRow }
	      </div>
			</div>
  	)
  }
}


class SelectFilterComponent extends formComponents.FormComponent {
  constructor (componentId, label, reactComponent) {
    super(componentId, label, reactComponent)
    this._value = formComponents.nullValueForSelect
    this._fullDisplayValue = null
  }

  clearValueForInput() { 
    this._value = formComponents.nullValueForSelect 
    this.forceRedraw()
  }
  clearValueForDisplay() { this._value = formComponents.nullValueForSelect }
  valueForDisplayComponent() { return this._fullDisplayValue ? this._fullDisplayValue.nameToDisplay : '' }

  // selects a filter
  handleInputChange(event) { 
    this._value = event.target.value 
    this.forceRedraw()
  }

  setValueFromServer(value) { 
    if (value) { this._value = value.filterId() } else { this.clearValueForInput() }    
    this._fullDisplayValue = value
    this.forceRedraw()
  }
  valueForServer() { return this._value }

  filters() { return this.reactComponent().filters() }

  chosenFilter() { 
    return (this._value == formComponents.nullValueForSelect)
    		? null
    		: this.filters().find(aFilter => aFilter.filterId() == this._value) 
  }

  isDisabled() { return this.chosenFilter() }

  buildInputComponent() {
    return (
      <FormControl componentClass="select"
        name={this.componentId()} 
        value={this._value}
        disabled={this.isDisabled()}
        onChange={(event) => this.handleInputChange(event)}
      >
        <option key={formComponents.nullValueForSelect} value={formComponents.nullValueForSelect}>...elegir...</option>)
        { this.filters()
            .filter(filter => !filter.isActive())
            .map((filter) => <option key={filter.filterId()} value={filter.filterId()}>{filter.nameForSelect()}</option>) 
        }
      </FormControl>
    )
  }
}


module.exports.FilterSelector = FilterSelector
