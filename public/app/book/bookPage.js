const React = require('react')
const ReactDOM = require('react-dom')
const Promise = require('bluebird')
const axios = require('axios')

const bookForm = require('./bookForm')


/*
 *  BookPage
 ***********************/
class BookPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {bookCount: ''}
    this._addedBookCount = 0
    this._lastLocallyAddedBookId = null
  }

  doesEdit() { return (pageAction == pageActions.add || pageAction == pageActions.edit) }
  doesAdd() { return (pageAction == pageActions.add) }
  handlesExistingBook() { return (pageAction == pageActions.show || pageAction == pageActions.edit) }

	componentDidMount() {
		this.updateInfo()
	}

	updateInfo() {
		return this.updateBookInfo()
	}

 	updateBookInfo() {
    if (this.doesAdd()) {
  		return Promise.all([this.updateBookCount(), this.updateLastlyEditedBook()])
    } else {
      return Promise.resolve()
    }
	}

	updateBookCount() {
		const self = this
		return axios
			.get('/books')
			.then(function(response) {
				self.setState({bookCount: response.data.length})
        return "ok"
			} )
  }

	updateLastlyEditedBook() {
		const self = this
		return axios
			.get('/lastlyEditedBook')
			.then(function(response) {
				self.setState({lastlyEditedBook: response.data})
        return "ok"
			} )
  }
  
  /*
    Information about added books
   */
  registerLocallyAddedBookId(id) {
    this._addedBookCount++
    this._lastLocallyAddedBookId = id
  }
  addedBookCount() { return this._addedBookCount }
	lastLocallyAddedBookId() { return this._lastLocallyAddedBookId }
	
	saveRecordedInfoAboutAddedBooks() {
		sessionStorage.setItem("editedBookCount", this.addedBookCount())
		sessionStorage.setItem("lastEditedBookId", this.lastLocallyAddedBookId())
	}

	saveInfoAboutOneEditedBook(bookId) {
		sessionStorage.setItem("editedBookCount", 1)
		sessionStorage.setItem("lastEditedBookId", bookId)
	}


  render() {
    let form = null
    let bottomFrames = null
    if (pageAction == pageActions.add) {
      form = (
    		<bookForm.AddBookForm
    			additionalSubmitAction={(postBookResponse) => {
            this.updateInfo().then(() => this.registerLocallyAddedBookId(postBookResponse.newBookId))
          }}
    			authorList={this.state.authorList}
    			languageList={this.state.languageList}
    			publisherList={this.state.publisherList}
          genreList={this.state.genreList}
          rootComponent={this}
    		/>
      )
      bottomFrames = [
    		<BookCount bookCount={this.state.bookCount} /> ,
    		this.state.lastlyEditedBook &&
    				<LastlyEditedBook book={this.state.lastlyEditedBook} />
    	]
    } else if (pageAction == pageActions.show) {
			form = (<bookForm.ShowBookForm bookId={bookId} rootComponent={this} /> )
      bottomFrames = []
    } else if (pageAction == pageActions.edit) {
			form = (<bookForm.ModifyBookForm bookId={bookId} rootComponent={this}/> )
      bottomFrames = []
    }
    return (
      <div>
        { form }
        { bottomFrames }
  		</div>
	  )
  }
}
// BookPage - end



/*
 *  BookCount
 ***********************/
class BookCount extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
			<div className="container">
				<div className="panel panel-default">
					<div className="panel-body">
			      <p>
			      Cantidad de libros: {this.props.bookCount}
			      </p>
			  	</div>
			  </div>
			</div>
    )
  }
}



/*
 *  LastlyEditedBook
 ***********************/
class LastlyEditedBook extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const publisherData = this.props.book.publisher 
            ? (<p> Editorial: {this.props.book.publisher} </p>)
            : null
        return (
			<div className="container">
				<div className="panel panel-default">
					<div className="panel-heading">
						<h4>Ultimo libro ingresado</h4>
					</div>
					<div className="panel-body">
                        <p> Título: {this.props.book.title} </p>
                        <p>
                            { (this.props.book.authors.length == 1) ? 'Autor: ' : 'Autores: ' }
                            {this.props.book.authors.join(', ')}
                        </p>
                        { publisherData }
                    </div>
                </div>
            </div>
        )
  }
}


ReactDOM.render(
  <BookPage />,
  document.getElementById('reactPage')
);
