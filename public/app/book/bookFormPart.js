const React = require('react')

/*
 * For any BookFormPart: props must include bookForm
 */
class BookFormPart extends React.Component {
    form() { return this.props.bookForm }
    page() { return this.form().page() }

    bookId() { return this.form().props.bookId }

    components() { return this.form().components() }
    component(name) { return this.form().component(name) }
    loanComponents() { return this.form().loanComponents() }
    loanComponent(name) { return this.form().loanComponent(name) }

    clearForm() { this.form().clearForm() }
    forceRedraw() { this.form().forceRedraw() }
    notify(what) { this.form().notify(what) }
    whatToNotify() { return this.form().whatToNotify() }

    inquirer() { return this.form().inquirer() }

    confirmButtonText() { return this.form().confirmButtonText() }
}


module.exports.BookFormPart = BookFormPart
