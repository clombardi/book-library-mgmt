const React = require('react')
import Button from 'react-bootstrap/lib/Button'
import FormGroup from 'react-bootstrap/lib/FormGroup'

import Dropzone from 'react-dropzone'

const formComponents = require('../lib/formComponents')
const bookFormPart = require('./bookFormPart')
const externalOperation = require('../lib/externalOperation')

/**
 * BookMainComponents
 */
class BookMainComponents extends bookFormPart.BookFormPart {
    render() {
        return (
            <div style={{marginBottom: "30px"}}>
                <div className="row">
                    <div className="col-sm-2">
                        { this.component("image").buildGroup() }
                    </div>
                    <div className="col-sm-10">
                        {this.isbnRow()}
                        <div key="title" className="row">
                            {/* Title */}
                            {this.component("title").buildComponentInRow("col-sm-6")}
                            {/* Authors */}
                            {this.component("authors").buildComponentInRow("col-sm-6")}
                        </div>
                        <div key="publisher" className="row">
                            {/* Publisher */}
                            {this.component("publisher").buildComponentInRow("col-sm-4")}
                            {/* Genre */}
                            {this.component("genre").buildComponentInRow("col-sm-4")}
                            {/* Language */}
                            {this.component("languages").buildComponentInRow("col-sm-4")}
                        </div>
                        <div key="year" className="row">
                            {/* Copy count */}
                            {this.component("copyCount").buildComponentInRow("col-sm-2")}
                            {/* Notes */}
                            {this.component("notes").buildComponentInRow("col-sm-10")}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    isbnRow() {
        const isbnSearchButton = (this.form().editBookInfo())
            ? (
                <Button key="isbnSearch"
                    disabled={!this.form().isbnSearchEnabled()}
                    onClick={() => this.form().searchByIsbn()}
                    style={{ backgroundColor: "#e6f9ff" }}
                >
                    Buscar por ISBN
                </Button>
            )
            : null;
        const isbnSearchMessage = (this.form().editBookInfo())
            ? (
                <externalOperation.ExternalOperationMessage status={this.form().searchStatus()} />
            )
            : null;
        const verticalAlignmentStyle = { float: "none", display: "inline-block", verticalAlign: "bottom" }

        return (
            <div key="isbnRow" className="row">
                {/* ISBN */}
                <div className="col-sm-6" style={verticalAlignmentStyle}>
                    {this.component("isbn").buildGroup()}
                </div>
                <div className="col-sm-6" style={verticalAlignmentStyle}>
                    <FormGroup controlId="isbnSearchButton">
                        <div style={Object.assign({ marginRight: "40px" }, verticalAlignmentStyle)}>
                            {isbnSearchButton}
                        </div>
                        <div style={verticalAlignmentStyle}>
                            {isbnSearchMessage}
                        </div>
                    </FormGroup>
                </div>
                {/* <div className="col-sm-2" style={verticalAlignmentStyle}>
                        <FormGroup controlId="isbnSearchButton">
                            {isbnSearchButton}
                        </FormGroup>
                    </div>
                    <div className="col-sm-4" style={verticalAlignmentStyle}>
                        <FormGroup controlId="isbnSearchMessage">
                            { isbnSearchMessage }
                        </FormGroup>
                    </div> */}
            </div>
        )
    }
}


/**
 * BookImageComponent
 */
export class BookImageComponent extends formComponents.FormComponent {
    constructor(componentId, label, reactComponent) {
        super(componentId, label, reactComponent)
        this._filename = null
        this._imageExists = false
        this._loadedImage = null
        this._mustDelete = false
        this.restoreBottomMessage()
    }
    
    restoreBottomMessage() { this._bottomMessage = "Arrastre la imagen en el cuadro" }
    clearValueForInput() { 
        this._loadedImage = null; this._imageExists = false 
        this.restoreBottomMessage()
    }
    clearValueForDisplay() { this._filename = null ; this._imageExists = false }

    bareValue() { return this._filename }

    // the value is set as an object { filename: String, imageExists: boolean }
    silentSetValueFromServer(value) {
        if (value) {
            this._filename = value.filename
            this._imageExists = value.imageExists
        }
        return this
    }

    // the value to be stored, as an object: { filename: String, image: File, mustDelete: boolean }
    valueForServer() { return { filename: this._filename, image: this._loadedImage, mustDelete: this._mustDelete } }

    buildActualInputGroup() {
        const deleteButtonStyle = Object.assign(
            { width: "100%"},
            this.reactComponent().panelHeadingStyle()
        )
        return (
            <div>
                <div className="row">
                    <div className="col-sm-12">
                        <Dropzone
                            style={{
                                borderWidth: "2px", borderRadius: "5px", borderStyle: "dashed", borderColor: "black",
                                width: "100%", height: "300px"
                            }}
                            onDrop={(files) => this.onPhotoDrop(files)}
                        >
                            { this.imageTag() }
                        </Dropzone>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <span style={{ width: "100%"}}>{ this._bottomMessage }</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <Button key="deleteImage" style={deleteButtonStyle}
                            onClick={() => { this.clearValueForInput() ; this._mustDelete = true ; this.forceRedraw() }}
                        >
                            Borrar
                        </Button>
                    </div>
                </div>
            </div>
        )
    }

    buildDisplayGroup() {
        return this.hasImage() ? this.imageTag() : "Imagen no disponible"
    }

    onPhotoDrop(files) {
        if (files.length == 1) {
            this._loadedImage = files[0]
            this._mustDelete = false
            this._bottomMessage = "cargó imagen " + this._loadedImage.name
        } else {
            this._bottomMessage = "No se puede poner más de un archivo"
        }
        this.forceRedraw()
    }

    imageSource() {
        return this._loadedImage ? URL.createObjectURL(this._loadedImage) : "/imageContents/" + this._filename
    }

    imageTag() { 
        return this.hasImage() 
            ? (<img style={{ maxWidth: "100%", height: "auto" }} src={this.imageSource()} alt={this.label()} />)
            : null
    }

    hasImage() { return (this._imageExists || this._loadedImage) && !this._mustDelete }
}



/**
 * BookLoanComponents
 */
class BookLoanComponents extends bookFormPart.BookFormPart {
    render() { return this.renderLoanComponents() }

    // renderLoanComponents() abstract

    loanDataFrameStyle() {
        return {
            marginTop: "20px", marginBottom: "20px", marginLeft: "10px", marginRight: "10px",
            borderStyle: "solid", borderWidth: "2px", borderColor: "LimeGreen", borderRadius: "10px",
            paddingTop: "5px", paddingBottom: "5px"
        }
    }

    loanInfoFields(secondDateField) {
        return [
            /* Borrower */
            this.loanComponent("borrower").buildComponentInRow("col-sm-6"),
            /* Start date */
            this.loanComponent("loanStartDate").buildComponentInRow("col-sm-3"),
            /* Next alarm or return date */
            this.loanComponent(secondDateField).buildComponentInRow("col-sm-3")
        ]
    }

    currentLoanInfoFields() { return this.loanInfoFields("loanNextAlarmDate") }
    
}

class ShowLoanInfoInFrame extends BookLoanComponents {
    renderLoanComponents() {
        let frameStyle = this.loanDataFrameStyle()
        if (this.props.isLent) {
            frameStyle.backgroundColor = "#fffce6"
        }
        return (
            <div key="loan" className="row" style={frameStyle}>
                <div className="container">
                    <div className="row" style={{ marginBottom: "10px" }}>
                        <div className="col-sm-12"><h4>{this.loanMessage()}</h4></div>
                    </div>
                    <div className="row">
                        { this.loanInfo() }
                    </div>
                </div>
            </div>
        )
    }

    loanMessage() {
        if (this.props.isLent) {
            return "Este libro está prestado"
        } else if (this.props.wasLent) {
            return "Datos del último préstamo"
        } else {
            return "Este libro nunca fue prestado"
        }
    }

    loanInfo() {
        if (this.props.isLent || this.props.wasLent) {
            return this.loanInfoFields(this.props.isLent ? "loanNextAlarmDate" : "loanReturnDate")
        } else {
            return null
        }
    }
}

class LendBookComponents extends BookLoanComponents {
    renderLoanComponents() {
        return (
            <div key="loan" className="row" style={this.loanDataFrameStyle()}>
                {this.currentLoanInfoFields()}
            </div> 
        )
    }    
}

class ReturnBookComponents extends BookLoanComponents {
    renderLoanComponents() {
        return (
            <div>
                <div key="loanData" className="row">
                    {this.currentLoanInfoFields()}
                </div>
                <div key="loanReturnData" className="row" style={this.loanDataFrameStyle()}>
                    {/* Start date */}
                    {this.loanComponent("loanReturnDate").buildComponentInRow("col-sm-3")}
                </div>
            </div>
        )
    }
}



/**
 * BookExtraComponents - clase utilitaria
 */
class ComponentSetPanel extends bookFormPart.BookFormPart {
    render() {
        let headingStyle = Object.assign(
            this.form().panelHeadingStyle(), { paddingTop: "2px", paddingBottom: "2px" }
        )
        return (
            <div className="panel panel-info" style={this.form().panelStyle()}>
                <div className="panel-heading" style={headingStyle}>
                    <h5>{ this.props.title }</h5>
                </div>
                <div className="panel-body">
                    { this.props.children }
                </div>
            </div>
        )        
    }
}

/**
 * BookExtraComponents
 */
class BookExtraComponents extends bookFormPart.BookFormPart {
    render() {
        return (this.form().mustExpandExtraComponents()) 
            ? <ExpandedBookExtraComponents bookForm={this.form()} />
            : <ShrunkBookExtraComponents bookForm={this.form()} />
    }
}

class ExpandedBookExtraComponents extends bookFormPart.BookFormPart {
    render() {
        return (
            <div>
                <ComponentSetPanel bookForm={ this.form() } title="Ubicación">
                    <div key="bookLocation" className="row">
                        {/* Bookcase */}
                        {this.component("bookcase").buildComponentInRow("col-sm-3")}
                        {/* Bookshelf */}
                        {this.component("bookshelf").buildComponentInRow("col-sm-3")}
                    </div>
                </ComponentSetPanel>
                <ComponentSetPanel bookForm={this.form()} title="Otros datos bibliográficos">
                    <div key="subtitle" className="row">
                        {/* subtitle */}
                        {this.component("subtitle").buildComponentInRow("col-sm-6")}
                        {/* Bookshelf */}
                        {this.component("collection").buildComponentInRow("col-sm-6")}
                    </div>
                    <div key="yearpagestranslation" className="row">
                        {/* pages */}
                        {this.component("pages").buildComponentInRow("col-sm-2")}
                        {/* year */}
                        {this.component("year").buildComponentInRow("col-sm-2")}
                        {/* original year */}
                        {this.component("originalYear").buildComponentInRow("col-sm-2")}
                        {/* is translation */}
                        {this.component("isTranslation").buildComponentInRow("col-sm-2")}
                        {/* original languages */}
                        {this.component("originalLanguages").buildComponentInRow("col-sm-4")}
                    </div>
                </ComponentSetPanel>
            </div>
        )
    }
}

class ShrunkBookExtraComponents extends bookFormPart.BookFormPart {
    render() {
        let buttonStyle = Object.assign(
            this.form().panelHeadingStyle(), { paddingLeft: "30px", paddingRight: "30px" }
        )
        return (
            <div className="panel panel-info" style={this.form().panelStyle()}>
                <div className="panel-body">
                    <div className="row"><div className="col-sm-12">
                        <Button key="expandExtraComponents" style={ buttonStyle }
                            onClick={() => this.expandExtraComponents()}>
                            Más info
                        </Button>
                    </div></div>
                </div>
            </div>
        )
    }

    expandExtraComponents() { this.form().expandExtraComponents() }
}


module.exports.BookMainComponents = BookMainComponents
module.exports.ShowLoanInfoInFrame = ShowLoanInfoInFrame
module.exports.LendBookComponents = LendBookComponents
module.exports.ReturnBookComponents = ReturnBookComponents
module.exports.BookExtraComponents = BookExtraComponents
