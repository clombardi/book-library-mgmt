const React = require('react')
const Promise = require('bluebird')
const axios = require('axios')

import Button from 'react-bootstrap/lib/Button'

const formComponents = require('../lib/formComponents')
const controlledComp = require('../lib/controlledComponent')
const dateComp = require('../lib/dateComponent')
const coolSelectComp = require('../lib/coolSelectComponent')
const countComponent = require('../lib/countComponent')
const inquirer = require('../lib/formInquirer')
const externalOperation = require('../lib/externalOperation')
import { NotificationPanel, notificationColors } from "../lib/notificationPanel"

const bookOperation = require('./bookOperation')
const bookComponent = require('./bookComponent')



/*
 *  BookForm
 ***********************/

const notificationStates = { nothing: 0, saving: 1, messages: 2, operationError: 3, aboutToDelete: 4 }

class BookForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = { whatToNotify: notificationStates.nothing }
        this.state.version = 1
        this.state.components = this.createComponents()
        this.prepareComponents()
        this.state.loanComponents = this.createLoanComponents()
        this.prepareLoanComponents()
        this.state.inquirer = this.createInquirer()
        this.state.mustExpandExtraComponents = false

        // valores sacados del panel-info de Bootstrap
        this._panelBorderColor = "#bce8f1"
        this._panelHeadingBackgroundColor = "#d9edf7"
        this._panelHeadingTextColor = "#31708f"

        // other related objects
        this._operation = this.createOperation()
        this._operationError = null

    }

    forceRedraw() {
        this.inquirer().computeMessages()
        this.setState({version: this.state.version + 1})
    }

    page() { return this.props.rootComponent }

    operation() { return this._operation }
    createOperation() { return null }
    setOperationError(theError) { 
        this._operationError = theError 
        this.notify(notificationStates.operationError)
    }

    editBookInfo() { return false }

    componentDidMount() { 
        return this.prepareReactComponent().then(() => this.forceRedraw())
    }

    /**
     * fetch info
     */
    fetchBookToHandle() {
        return Promise
            .all([axios.get('/books/' + this.props.bookId), axios.get("/imageExists/" + this.props.bookId)])
            .spread((bookInfoResp, imageExistsResp) => {
                const bookInfo = bookInfoResp.data
                bookInfo.imageExists = imageExistsResp.data.exists
                this.useFetchedBook(bookInfo)
                return Promise.resolve()
            })
            .catch((error) => {
                console.log(error)
                return Promise.reject(error)
            })
    }

    useFetchedBook(bookJson) {
        this.components().forEach(
            (comp) => comp.silentSetValueFromServer(bookJson[comp.componentId()])
        )
        // in fact the value for the image component is set twice, 
        // firstly with undefined
        // afterwards with filename and imageExists
        this.component("image").silentSetValueFromServer({ 
            filename: this.props.bookId, imageExists: bookJson.imageExists 
        })
    }
    
    fetchSingleListOfNames(serviceName) {
        const self = this
        return axios
            .get('/' + serviceName)
            .then(function(response) {
                return response.data.map((enti) => enti.name)
            })
    }

    fetchNameLists() {
        const self = this
        return (
            Promise.all(["authorList", "publisherList", "languageList", "genreList"]
                .map((service) => this.fetchSingleListOfNames(service))
            ).spread(function(authors, publishers, languages, genres) {

                self.component("authors").setOptionList(authors)
                self.component("languages").setOptionList(languages)
                self.component("originalLanguages").setOptionList(languages)
                self.component("publisher").setOptionList(publishers)
                self.component("genre").setOptionList(genres)
                return "ok"
            })
        )
    }


    // redirects
    showListOfBooks() { 
        sessionStorage.setItem("lastPage", "bookPage")
        window.location.href = "/listBooks" 
    }



    /**
     * Component manangement
     */
    createComponents() {
        let originalLanguagesComponent = 
            new (controlledComp.ExplicitControlledComponentMixin(coolSelectComp.MultiTypeaheadComponent)) (
                'originalLanguages', 'Idiomas originales', 'Idioma original', 'idioma', this
            )
        let isTranslationComponent = 
            new (controlledComp.ExplicitControllerComponentMixin(formComponents.CheckboxComponent)) (
                'isTranslation', 'Es traducción', this
            ).setControlledComponents([originalLanguagesComponent])
        return [
            new formComponents.TextComponent('isbn', 'ISBN', this),
            new formComponents.TextComponent('title', 'Título', this),
            new formComponents.NumberComponent('year', 'Año', this),
            new formComponents.NumberComponent('pages', 'Páginas', this),
            new formComponents.TextAreaComponent('notes', 'Anotaciones', this),
            new coolSelectComp.TypeaheadComponent('publisher', 'Editorial', 'editorial', this),
            new coolSelectComp.TypeaheadComponent('genre', 'Género', 'género', this),
            new coolSelectComp.MultiTypeaheadComponent('authors', 'Autores', 'Autor', 'autor', this),
            new coolSelectComp.MultiTypeaheadComponent('languages', 'Idiomas', 'Idioma', 'idioma', this),
            new countComponent.CountComponent('copyCount', 'Ejemplares', this),
            new formComponents.TextComponent('bookcase', 'Biblioteca', this),
            new formComponents.TextComponent('bookshelf', 'Estante / Sección', this),
            new formComponents.TextComponent('subtitle', 'Subtítulo', this),
            new formComponents.TextComponent('collection', 'Colección / Saga', this),
            new formComponents.NumberComponent('originalYear', 'Año original', this),
            isTranslationComponent,
            originalLanguagesComponent,
            new bookComponent.BookImageComponent('image', 'Imagen del libro', this)
        ]
    }

    // prepareComponents() abstract

    component(name) { return this.state.components.find((comp) => comp.componentId() == name) }

    components() { return this.state.components }


    /**
     * Loan components
     */
    createLoanComponents() {
        return [
            new coolSelectComp.TypeaheadComponent('borrower', 'Prestado a', 'a quién', this),
            new dateComp.DateComponent('loanStartDate', 'En la fecha', this),
            new dateComp.DateComponent('loanNextAlarmDate', 'Se espera devolución', this),
            new dateComp.DateComponent('loanReturnDate', 'Se devolvió el', this)
        ]
    }

    loanComponent(name) { return this.loanComponents().find((comp) => comp.componentId() == name) }
    loanComponents() { return this.state.loanComponents }

    prepareLoanComponents() { this.loanComponents().forEach((comp) => comp.beForDisplay()) }


    /**
     * Inquirer
     */
    createInquirer() { 
        let confirmMessage = this.confirmButtonText()
        confirmMessage = confirmMessage[0].toLowerCase() + confirmMessage.substring(1)

        const theInquirer = new inquirer.FormInquirer(confirmMessage, () => this.operation().doAction())
        theInquirer.addInquiry(new inquirer.MandatoryTextComponent(this, "title"))
        theInquirer.addInquiry(new inquirer.AtLeastOneValue(this, "authors"))
        theInquirer.addInquiry(new inquirer.AtLeastOneValue(this, "languages"))
        theInquirer.addInquiry(new inquirer.EmptyComponentWarning(this, "genre"))
        theInquirer.addInquiry(new inquirer.EmptyComponentWarning(this, "publisher"))
        // theInquirer.addInquiry(new inquirer.EmptyComponentWarning(this, "year").beNumeric())
        // theInquirer.addInquiry(new inquirer.EmptyComponentWarning(this, "pages").beNumeric())
        return theInquirer
    }

    inquirer() { return this.state.inquirer }


    /**
     * Form management
     */
    clearForm() { 
        this.state.components.forEach((comp) => comp.clearValue()) 
        this.component("copyCount").silentSetValueFromServer("1")
        this.notify(notificationStates.nothing)
    }

    updateLists() {
        this.component("authors").addOptions(this.component("authors").valueForServer())
        this.component("languages").addOptions(this.component("languages").valueForServer())
        this.component("languages").addOptions(this.component("originalLanguages").valueForServer())
        this.component("originalLanguages").addOptions(this.component("languages").valueForServer())
        this.component("originalLanguages").addOptions(this.component("originalLanguages").valueForServer())
        this.component("publisher").addOption(this.component("publisher").valueForServer())
        this.component("genre").addOption(this.component("genre").valueForServer())
        return Promise.resolve()
    }

    isbnSearchEnabled() { return false }

    
    /**
     * events
     */
    whatToNotify() { return this.state.whatToNotify }
    notify(what) { this.setState({ whatToNotify: what }) }

    expandExtraComponents() { this.setState({ mustExpandExtraComponents: true }) }
    shrinkExtraComponents() { this.setState({ mustExpandExtraComponents: false }) }
    mustExpandExtraComponents() { return this.state.mustExpandExtraComponents }


    /**
     * style
     */
    setPanelColors(border, headingBackground, headingText) {
        this._panelBorderColor = border
        this._panelHeadingBackgroundColor = headingBackground
        this._panelHeadingTextColor = headingText
    }

    panelStyle() {
        return this._panelBorderColor ? { borderColor: this._panelBorderColor } : {}
    }

    panelHeadingStyle() {
        let theStyle = {}
        if (this._panelBorderColor) { theStyle.borderColor = this._panelBorderColor }
        if (this._panelHeadingBackgroundColor) { theStyle.backgroundColor = this._panelHeadingBackgroundColor }
        if (this._panelHeadingTextColor) { theStyle.color = this._panelHeadingTextColor }
        return theStyle
    }


    /**
     * Renderization
     */

    notificationPanel() {
        const self = this
        if (this.whatToNotify() == notificationStates.nothing ) {
            return []
        } else if (this.whatToNotify() == notificationStates.messages ) {
            return (
                <div className="row" key="notificationPanel">
                    <div className="col-sm-10 col-sm-offset-1">
                        { this.state.inquirer.issuePanel() }
                    </div>
                </div>
            )
        } else if (this.whatToNotify() == notificationStates.saving ) {
            return (
                <div className="row" key="notificationPanel">
                    <div className="col-sm-10 col-sm-offset-1">
                        <div className="panel panel-warning" style={{borderRadius: "25px"}}>
                            <div className="panel-heading" style={{borderRadius: "25px"}}>
                                <h4><p className="text-center">Registrando ...</p></h4>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else if (this.whatToNotify() == notificationStates.operationError) {
            return (
                <NotificationPanel 
                    key="notificationPanel"
                    title={ <span style={{ color: notificationColors.errorLabelColor }}>Error en la registración</span> }
                    items={ this._operationError.messages() }
                    borderColor={ notificationColors.errorBorderColor }
                    backgroundColor={ notificationColors.errorBackgroundColor }
                />
            )
        } 
    }

    fields() { 
        return this._operationError ? null : (<bookComponent.BookMainComponents bookForm={this} key="bookFields" />) 
    }

    renderExtraComponents() { 
        return this._operationError ? null : <bookComponent.BookExtraComponents bookForm={this} key="bookExtraFields" /> 
    }

    renderActualLoanComponents() { return this._operationError ? null : this.renderLoanComponents() }

    // does nothing by default
    renderLoanComponents() { return null }

    actualButtons() {
        return this._operationError 
            ? (<bookOperation.OperationErrorToolbar bookForm={this} key="errorToolbar" />) 
            : this.buttons() 
    }

    formContents() {
        return [
            this.notificationPanel(),
            this.fields(),
            this.renderExtraComponents(),
            this.renderActualLoanComponents(),
            this.actualButtons()
        ]
    }

    render() {
        let self = this
        return (
            <div className="container">
                <div className="panel panel-info" style={this.panelStyle()}>
                    <div className="panel-heading" style={this.panelHeadingStyle()}>
                        <h4>{ this.title() }</h4>
                    </div>
                    <div className="panel-body">
                        <form>
                            { this.formContents() }
                        </form>
                    </div>
                </div>
            </div>
        )
    }


    // title() abstract
    // buttons() abstract
}
// BookForm - end


class EditBookForm extends BookForm {
    constructor(props) {
        super(props)
        this._searchStatus = new externalOperation.ExternalSearchStatus()
    }
    editBookInfo() { return true }
    searchStatus() { return this._searchStatus }

    forceRedraw() {
        this.searchStatus().reset()
        super.forceRedraw()
    }

    isbnSearchEnabled() { 
        return !this.searchStatus().isSearching() && [10, 13].includes(this.searchableIsbnValue().length) 
    }

    searchableIsbnValue() {
        const isbnValue = this.component("isbn").bareValue()
        if (!isbnValue) { return "" }
        return isbnValue.replace(/-/g, "")
    }

    disableComponents() {
        this.components().forEach( (comp) => comp.beDisabled() )
        this.forceUpdate()
    }

    enableComponents() {
        this.components().forEach((comp) => comp.beEnabled())
        this.forceUpdate()
    }

    showSearchError(errorMessage) {
        this.searchStatus().endSearchWithError(errorMessage)
        this.forceUpdate()
    }

    searchByIsbn() {
        const self = this
        this.disableComponents()
        this.searchStatus().beginSearch()
        return axios.get("/bookInfoByIsbn/" + this.searchableIsbnValue())
            .then(response => {
                const bookData = response.data
                const fields = ["title", "publisher", "year", "authors", "languages"]
                fields.forEach(fieldName => {
                    self.component(fieldName).silentSetValueFromServer(bookData[fieldName])    
                })
                // pages
                self.component("pages").silentSetValueFromServer(bookData.pageCount)    
                // // language and originalLanguage
                // if (bookData.language) {
                //     self.component("languages").silentSetValueFromServer([bookData.language])
                // } else {
                //     self.component("languages").clearValueForInput()
                // }
                // if (bookData.originalLanguage) {
                //     self.component("originalLanguages").silentSetValueFromServer([bookData.originalLanguage])
                // } else {
                //     self.component("originalLanguages").clearValueForInput()
                // }

                // found!!
                this.searchStatus().reset()
                this.enableComponents()
                return Promise.resolve()
            })    
            .catch(error => {
                const message = error.response.data.message
                this.showSearchError(message)
                this.enableComponents()
                return Promise.resolve()
            })
    }

}


/*
 *  AddBookForm
 ***********************/
class AddBookForm extends EditBookForm {
    createOperation() { return new bookOperation.AddBookOperation(this) }

    editBookInfo() { return true }

    prepareComponents() {
        this.state.components.forEach( (comp) => comp.beForInput() )
        this.clearForm()
    }   

    prepareReactComponent() { return this.fetchNameLists() }

    title() { return "Cargar un nuevo libro" }

    confirmButtonText() { return "Agregar libro" }

    buttons() { return <bookOperation.AddBookToolbar bookForm={this} key="buttons" /> }
 
    /*
     * redirects
     */
    showListOfBooks() {
        this.page().saveRecordedInfoAboutAddedBooks()
        super.showListOfBooks()
    }
}


/*
 *  ShowBookForm
 ***********************/
class ShowBookForm extends BookForm {
    constructor(props) { 
        super(props); 
        this.isLent = false
        this.wasLent = false
    }
    
    prepareComponents() { this.state.components.forEach( (comp) => comp.beForDisplay() ) }

    prepareReactComponent() { 
        return this.fetchBookToHandle().then(() => this.fetchLastLoan())
    }    
    
    useFetchedBook(bookJson) {
        super.useFetchedBook(bookJson)
        this.isLent = bookJson.isLent
        if (this.isLent) {
            this.loanComponent("borrower").setValueFromServer(bookJson.borrower)
            this.loanComponent("loanStartDate").setValueFromServer(bookJson.currentLoanStartDate)
            this.loanComponent("loanNextAlarmDate").setValueFromServer(bookJson.currentLoanNextAlarmDate)
        }
    }

    fetchLastLoan() {
        if (!this.isLent) {
            return axios.get('/books/' + this.props.bookId + '/lastLoan')
                .then(response => {
                    if (response.data.hasLoan) {
                        this.wasLent = true
                        this.loanComponent("borrower").setValueFromServer(response.data.borrower)
                        this.loanComponent("loanStartDate").setValueFromServer(response.data.startDate)
                        this.loanComponent("loanReturnDate").setValueFromServer(response.data.returnDate)
                    } else {
                        this.wasLent = false
                    }    
                })    
        } else {
            return Promise.resolve()
        }
    }        
    
    title() { return "Detalle de libro" }


    /**
     * Renderization
     */
    notificationPanel() {
        if (this.whatToNotify() === notificationStates.aboutToDelete) {
            return (
                <NotificationPanel
                    key="deletionConfirmationPanel"
                    title="Confirmar la eliminación de este libro"
                    items={[]}
                    borderColor="RebeccaPurple"
                    backgroundColor="Thistle"
                    button={[
                        <Button onClick={() => { 
                            new bookOperation.DeleteBookOperation(this).doAction()
                        }}>OK, eliminar</Button>,
                        <Button onClick={() => {
                            console.log("Cancel book deletion");
                            this.notify(notificationStates.nothing)
                        }}>Me confundí, cancelar</Button>
                    ]}
                />
            )
        } else {
            return super.notificationPanel()
        }
    }

    renderLoanComponents() { 
        return <bookComponent.ShowLoanInfoInFrame bookForm={this} isLent={this.isLent} wasLent={this.wasLent} key="bookLoanInfo" /> 
    }

    confirmButtonText() { return "Modificar libro" }

    buttons() { return <bookOperation.ShowBookToolbar bookForm={this} isLent={this.isLent} key="buttons" /> }
}


/*
 *  ModifyBookForm
 ***********************/
class ModifyBookForm extends EditBookForm {
    createOperation() { return new bookOperation.ModifyBookOperation(this) }

    prepareComponents() { this.state.components.forEach( (comp) => comp.beForInput() ) }

    prepareReactComponent() { return this.fetchNameLists().then(() => this.fetchBookToHandle()) }

    useFetchedBook(bookJson) {
        super.useFetchedBook(bookJson)
        this.isbnBeforeModification = this.component("isbn").bareValue()
    }

    isbnSearchEnabled() { 
        return super.isbnSearchEnabled() 
            && (this.component("isbn").bareValue() != this.isbnBeforeModification) 
    }

    title() { return "Modificar libro existente" }

    confirmButtonText() { return "Confirmar modificación" }

    buttons() { return <bookOperation.ModifyBookToolbar bookForm={this} key="buttons" /> }
}


module.exports.notificationStates = notificationStates
module.exports.BookForm = BookForm
module.exports.AddBookForm = AddBookForm
module.exports.ModifyBookForm = ModifyBookForm
module.exports.ShowBookForm = ShowBookForm
