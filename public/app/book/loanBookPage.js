const React = require('react')
const ReactDOM = require('react-dom')

import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar'
import Button from 'react-bootstrap/lib/Button'

const axios = require('axios')

const bookForm = require('./bookForm')
const bookComponent = require('./bookComponent')
const bookOperation = require('./bookOperation')


class BookLoanPage extends React.Component {
    render() {
        if (actionToPerform == "lendBook") {
            return ( <LendBookForm bookId={bookId} /> )
        } else if (actionToPerform == "returnLentBook") {
            return ( <ReturnBookForm bookId={bookId} /> )
        } else {
            return (<div className="container"><h1>actionToPerform no asignada o valor erróneo</h1></div>)
        }
    }
}


class BookLoanRelatedForm extends bookForm.BookForm {
    /**
     * Component management
     */
    prepareComponents() {
        this.state.components.forEach((comp) => comp.beForDisplay())
    }

    /**
     * Fetch info
     */
    prepareReactComponent() { return this.fetchBookToHandle() }

    /**
     * Renderization
     */
    buttons() { return <bookOperation.BookLoanOperationToolbar bookForm={this} /> }
}


class LendBookForm extends BookLoanRelatedForm {
    constructor(props) {
        super(props)
        this.setPanelColors("MediumSpringGreen", "#AFF0C0", "Green")
    }
    createOperation() { return new bookOperation.LendBookOperation(this) }
    title() { return "Préstamo de libro" }
    
    /**
     * Loan components
     */
    prepareLoanComponents() { 
        this.loanComponents().forEach((comp) => comp.beForInput()) 
        this.loanComponent("borrower").setLabel("Se presta a")
        this.loanComponent("borrower").beAutofocus()
        this.loanComponent("loanStartDate").setTodayAsValue()
    }

    /**
     * Component lifecycle
     */
    prepareReactComponent() {
        return super.prepareReactComponent().then(() => this.fetchPeople())
    }

    fetchPeople() {
        // return Promise.resolve()
        const self = this
        axios.get('/peopleList')
            .then(function(response) { 
                self.loanComponent("borrower").setOptionList(response.data.map((enti) => enti.name)) 
                return Promise.resolve()
            })
    }

    /**
     * Renderization
     */
    renderLoanComponents() { return <bookComponent.LendBookComponents bookForm={this} /> }
    confirmButtonText() { return "Confirmar préstamo" }
}


class ReturnBookForm extends BookLoanRelatedForm {
    constructor(props) {
        super(props)
        this.setPanelColors("SpringGreen", "#ccffcc", "ForestGreen")
    }
    createOperation() { return new bookOperation.ReturnBookOperation(this) }

    title() { return "Devolución de libro" }

    /**
     * Components
     */
    prepareLoanComponents() {
        this.loanComponents().forEach((comp) => { 
            if (comp.componentId() == "loanReturnDate") {
                comp.beForInput()
            } else {
                comp.beForDisplay() 
            }
        })
        this.loanComponent("loanReturnDate").beAutofocus()
        this.loanComponent("loanReturnDate").setTodayAsValue()
    }

    /**
     * Fetch info
     */
    useFetchedBook(bookJson) {
        super.useFetchedBook(bookJson)
        this.loanComponent("borrower").setValueFromServer(bookJson.borrower)
        this.loanComponent("loanStartDate").setValueFromServer(bookJson.currentLoanStartDate)
        this.loanComponent("loanNextAlarmDate").setValueFromServer(bookJson.currentLoanNextAlarmDate)
    }

    /**
     * Renderization
     */
    renderLoanComponents() { return <bookComponent.ReturnBookComponents bookForm={this} /> }
    confirmButtonText() { return "Confirmar devolución" }
}


ReactDOM.render(
    <BookLoanPage />,
    document.getElementById('reactPage')
);
