
/*
 *  BookExporter
 *
 *  Expected prop: bookList
 ***************************************/
// class BookExporter extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {}
//   }

//   render() {
//     return this.renderUsingXlsxStyle()
//   }

//   renderUsingXlsxStyle() {
//     return this.buildButton()
//   }

//   renderUsingReactDataExport() {
//     return (
//       <ExcelFile element={ this.buildButton() } filename="libros.xlsx">
//         <ExcelSheet data={this.bookList()} name="Libros">
//             <ExcelColumn label="Nombre" value="title" />
//             <ExcelColumn label="Autores" value={(book) => book.authors.join(", ")} />
//             <ExcelColumn label="Idioma" value={(book) => book.languages.join(", ")} />
//             <ExcelColumn label="Editor" value={(book) => book.publisher ? book.publisher : ""} />
//             <ExcelColumn label="Género" value={(book) => book.genre ? book.genre : ""} />
//             <ExcelColumn label="Páginas" value="pages" />
//             <ExcelColumn label="Año" value="year" />
//             <ExcelColumn label="Notas" value="notes" />
//         </ExcelSheet>
//       </ExcelFile>
//     )
//   }

//   buildButton() {
//     const text = this.props.separateAuthorsLanguages ? "Generar Excel (autores separados)" : "Generar Excel"
//     const color = this.props.separateAuthorsLanguages ? "#ffe6ff" : "#ffccff"
//     return (
//       <Button style={{backgroundColor: color}} block>
//         { text }
//       </Button>
//     )
//   }

//   bookList() {
//     console.log('asked for bookList')
//     let theList = null
//     if (this.props.separateAuthorsLanguages) {
//       console.log('asked to separate authors / languages')
//       const newList = []
//       this.props.bookList.forEach((book) => {
//         const rowsForBook = Math.max(book.authors.length, book.languages.length, 1)
//         for (let row = 0; row < rowsForBook; row++) {
//           let thisRow = { 
//             title: (row == 0) ? book.title : null,
//             publisher: (row == 0) ? book.publisher : null,
//             genre: (row == 0) ? book.genre : null,
//             pages: (row == 0) ? book.pages : null,
//             year: (row == 0) ? book.year : null,
//             notes: (row == 0) ? book.notes : null,
//             authors: (row < book.authors.length) ? [book.authors[row]] : [""],
//             languages: (row < book.languages.length) ? [book.languages[row]] : [""]
//           }
//           newList.push(thisRow)
//         }
//       })
//       theList = newList
//     } else {
//       theList = this.props.bookList
//     }
//     console.log(theList)
//     return theList
//   }

//   generateXlsxFileLowLevel() {
//     const worksheet = new Worksheeet()  
//       worksheet.addTitles(["Título", "Autores", "Idioma/s", "Editor", "Género", "Páginas", "Año", "Notas"])
//       this.bookList().forEach((dataRow) => 
//         const row = new WorksheetRow()
//         row.addString(row.title)
//         row.addString(row.authors.join(", "))
//         row.addString(row.languages.join(", "))
//         row.addString(row.publisher)
//         row.addString(row.genre)
//         row.addNumber(row.pages)
//         row.addNumber(row.year)
//         row.addString(row.notes)
//         worksheet.addRow(row)
//       ))
//   }

//   createNewWorksheet() { return {} }
//   createNewWorkbook() { return { SheetNames: [], Sheets: {} } }
//   addSheetToBook(sheet, sheetName, book) {
//     book.SheetNames.push(sheetName);
//     book.Sheets[sheetName] = sheet;
//   }
// }


// class Worksheet {
//   constructor() {
//     this.
//   }
// }

