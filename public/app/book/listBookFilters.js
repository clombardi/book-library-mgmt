var _ = require('lodash')
const React = require('react')

import Form from 'react-bootstrap/lib/Form'
import Button from 'react-bootstrap/lib/Button'
import InputGroup from 'react-bootstrap/lib/InputGroup'
import FormGroup from 'react-bootstrap/lib/FormGroup'
import Glyphicon from 'react-bootstrap/lib/Glyphicon'


const utils = require('../lib/utils')
const filters = require('../lib/filters')
const formComponents = require('../lib/formComponents')


class ListBooksFilter extends filters.Filter {
    constructor(filterId, textualName, controller) {
        super(filterId, textualName, textualName)
        this._controller = controller
        this.buildFields(controller)
    }
    controller() { return this._controller }
    pageComponent() { return this.controller().page() }

    valueChanged() { return this.controller().reapplyFilters() }
}


class BasicTextFilter extends ListBooksFilter {
    buildFields(controller) {
        this._searchStringField = new CustomTextComponent("searchString", "Buscar", controller)
        this._searchStringField.setInputSize(60).beForInput()
            .setPlaceholder("ISBN, Título o Autores")
            .setKeyPressHandler((event, component) => this.handleKeyPressed(event, component))
            .setLensPressedHandler(() => this.handleLensPressed())
            .setCustomControlStyle({ borderColor: "#6495ED", borderWidth: 2 })
            
        this._isbnField = this.buildOptionField("isbn", "ISBN")
        this._titleField = this.buildOptionField("title", "Titulo")
        this._authorsField = this.buildOptionField("authors", "Autores")
        this._othersField = this.buildOptionField("other", "Otros")
                .silentSetValueFromServer(false).setTooltipText("Subtitulo y Colección/saga")

        this._doorButton = new DoorButtonComponent(
            "searchString_door", "Ver opciones de búsqueda", 
            controller, this, () => this.checkboxesBehindDoor()
        ).beForInput()
    }

    buildOptionField(ownId, label) {
        return new CustomCheckboxComponent("searchString_" + ownId, label, this.controller(), this)
            .beForInput().setInlineRightMargin("10px").beDiscrete()
            .silentSetValueFromServer(true)
    }

    beAutofocus() { this._searchStringField.beAutofocus(); return this }

    handleKeyPressed(event, textComponent) {
        if (event.charCode == 13) {
            // cancel standard behavior of enter key
            event.preventDefault()
            event.stopPropagation()
            // specific behavior: make this filter active and tell the controller
            // that filters must be reapplied
            this.activate()
            return this.controller().reapplyFilters()
        }
    }

    handleLensPressed() {
        // behavior: make this filter active and tell the controller
        // that filters must be reapplied
        this.activate()
        return this.controller().reapplyFilters()
    }

    rangeFields() { return [this._searchStringField, this._doorButton,
        this._isbnField, this._titleField, this._authorsField, this._othersField
    ] }

    deactivate() {
        super.deactivate()
        this._isbnField.silentSetValueFromServer(true)
        this._titleField.silentSetValueFromServer(true)
        this._authorsField.silentSetValueFromServer(true)
    }

    checkboxesBehindDoor() { 
        return (
            <Form inline={true} style={{overflow: "hidden"}}>
                <div style={{float: "left"}}>
                    <span style={{ marginLeft: "10px", marginRight: "5px", fontWeight: "bold" }}>Buscar en:</span>
                </div>
                <div style={{ float: "left" }}>
                    { this.renderOptionField(this._isbnField) }
                    { this.renderOptionField(this._titleField) }
                    { this.renderOptionField(this._authorsField) }
                    { this.renderOptionField(this._othersField) }
                </div>
            </Form>
        )
    }

    renderOptionField(field) { 
        return ( <span style={{ marginLeft: "20px" }}>{ field.buildInlineGroup() } </span> )
    }
    
    editIsInline() { return false }

    editForm() {
        return (
            <div>
                <div className="row" style={{marginRight: "20px"}}>
                    <Form inline={false}>
                        {this._searchStringField.buildComponentInRow("col-md-12")}
                    </Form>
                </div>
                <div className="row" style={{ marginRight: "20px" }}>
                    {this._doorButton.buildComponentInRow("col-md-12")}
                </div>
            </div>
        )
    }

    mustCheckIsbn() { return this._isbnField.bareValue() }
    mustCheckTitle() { return this._titleField.bareValue() }
    mustCheckAuthors() { return this._authorsField.bareValue() }
    mustCheckOthers() { return this._othersField.bareValue() }

    isSatisfiedBy(serverData) {
        let valueToCompare = this._searchStringField.valueForServer()
        if (!valueToCompare) { return true }
        valueToCompare = utils.stringToSpanishCompareValue(valueToCompare.toUpperCase())

        let simpleFieldsToCheck = []
        if (this.mustCheckIsbn()) { simpleFieldsToCheck.push("isbn") }
        if (this.mustCheckTitle()) { simpleFieldsToCheck.push("title") }
        if (this.mustCheckOthers()) { simpleFieldsToCheck.push("subtitle", "collection") }

        const stringSatisfies = (str) => str && utils.stringToSpanishCompareValue(str).includes(valueToCompare)
        const simpleFieldMatch = () =>
            simpleFieldsToCheck.some(fieldName => stringSatisfies(serverData[fieldName]))
        const authorMatch = () => this.mustCheckAuthors() && serverData.authors.some(stringSatisfies)
        return simpleFieldMatch() || authorMatch()
    }

    isActive() { return super.isActive() && this._searchStringField.valueForServer() }
    valueForServer() { 
        const idForServer = id => id.includes("_") ? id.slice(id.indexOf("_") + 1) : id
        return _.fromPairs(
            [this._searchStringField, this._isbnField, this._titleField, this._authorsField, this._othersField]
            .map(field => [idForServer(field.componentId()), field.valueForServer()])
        )
    }
}


class OnlyTrueBooleanFilter extends ListBooksFilter {
    buildFields(controller) {
        this._filterField = new CustomCheckboxComponent(this.filterId() + "_check", this.nameToDisplay(), controller, this)
        this._filterField.beForInput()
    }

    setTooltipText(text) {
        this._filterField.setTooltipText(text)
        return this
    }

    rangeFields() { return [this._filterField] }
    editIsInline() { return false }

    filterValueToCompare() { return this._filterField.valueForServer() }

    isSatisfiedBy(serverData) { 
        return this.filterValueToCompare() 
            ? utils.nullSafeBooleanCompare(this.objectToCompare(serverData), this.filterValueToCompare())
            : false
    }

    isActive() { return this.filterValueToCompare() }
    activate() { this._filterField.silentSetValueFromServer(true) }
    deactivate() { this._filterField.silentSetValueFromServer(false) }
    valueForServer() { return this.filterValueToCompare() }
}


class IsLentFilter extends OnlyTrueBooleanFilter {
    valueChanged() {
        if (!this.filterValueToCompare()) {
            this.controller().filter("shouldBeReturned").deactivate()
        }
        super.valueChanged()
    }
}

class ShouldBeReturnedFilter extends OnlyTrueBooleanFilter {
    valueChanged() {
        if (this.filterValueToCompare()) {
            this.controller().filter("isLent").activate()
        }
        super.valueChanged()
    }
}


class CustomTextComponent extends formComponents.TextComponent {
    constructor(componentId, label, reactComponent) {
        super(componentId, label, reactComponent)
        this._lensPressedHandler = null
    }

    setLensPressedHandler(handler) { this._lensPressedHandler = handler ; return this }

    handleLensPressed() {
        if (this._lensPressedHandler) { this._lensPressedHandler() }
    }

    buildInputComponent() {
        return (
            <InputGroup>
                { super.buildInputComponent() }
                <InputGroup.Addon 
                    style={{ backgroundColor: "#6495ED", borderColor: "#6495ED"}} 
                    onClick={() => this.handleLensPressed()}
                >
                    <Glyphicon glyph="search" />
                </InputGroup.Addon>
            </InputGroup>
        )
    }

    buildActualInputGroup() {
        return (
            <FormGroup controlId={this.componentId()}>
                {this.buildInputComponent()}
            </FormGroup>
        )
    }
}


class CustomCheckboxComponent extends formComponents.CheckboxComponent {
    constructor(componentId, label, reactComponent, filter) {
        super(componentId, label, reactComponent)
        this._filter = filter
    }

    handleInputChange(event) {
        super.handleInputChange(event)
        this._filter.valueChanged()
    }
}


class DoorButtonComponent extends CustomCheckboxComponent {
    constructor(componentId, label, reactComponent, filter, behindDoorBuilder) {
        super(componentId, label, reactComponent, filter)
        this._behindDoorBuilder = behindDoorBuilder
    }

    buildActualInputGroup() {
        if (this.bareValue()) {
            return this._behindDoorBuilder()
        } else {
            return (
                <Button onClick={() => this.openDoor()} block style={{ backgroundColor: "LightSkyBlue" }}>
                    {this.label()}
                </Button>
            )
        }
    }

    openDoor() {
        this._value = true
        this._filter.valueChanged()
    }
}


module.exports.BasicTextFilter = BasicTextFilter
module.exports.OnlyTrueBooleanFilter = OnlyTrueBooleanFilter
module.exports.IsLentFilter = IsLentFilter
module.exports.ShouldBeReturnedFilter = ShouldBeReturnedFilter