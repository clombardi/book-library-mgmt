const React = require('react')
const Promise = require('bluebird')
const axios = require('axios')
import { prop, defaultTo } from "ramda"

import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar'
import Button from 'react-bootstrap/lib/Button'

const bookForm = require('./bookForm')
const bookFormPart = require('./bookFormPart')
import { OperationError } from "../lib/operationError"


/** 
 * Auxiliary class 
 */
class SpecForServerBuilder {
    constructor(components) {
        this._components = components
        this._fieldNames = []
    }

    component(name) { return this._components.find((comp) => comp.componentId() == name) }

    addSimpleField(name) {
        this._fieldNames.push({ componentName: name, serverName: name })
        return this
    }
    addSimpleFields(...names) {
        names.forEach(name => this.addSimpleField(name))
        return this
    }
    addField(theComponentName, theServerName) {
        this._fieldNames.push({ componentName: theComponentName, serverName: theServerName })
        return this
    }

    specForServer() {
        const theSpec = {}
        this._fieldNames.forEach(fieldSpec => {
            theSpec[fieldSpec.serverName] = this.component(fieldSpec.componentName).valueForServer()
        })
        return theSpec
    }
}


/****************************************************************
 *      Book operation
 ****************************************************************/
class BookOperation {
    constructor(form) { this._form = form }

    form() { return this._form }
    bookId() { return this.form().props.bookId }

    startSaving() {
        this.form().components().forEach((comp) => comp.beDisabled())
        this.form().notify(bookForm.notificationStates.saving)
    }

    endSaving() {
        this.form().clearForm()
        this.form().components().forEach((comp) => comp.beEnabled())
        this.form().notify(bookForm.notificationStates.nothing)
    }

    informErrorFromResponse(response) {
        const errors = prop("errors", prop("metadata", response.data))
        const actualError = errors && errors.find(theError => theError.status == 500)
        const errorData = defaultTo(response.data, actualError)
        const errorResource = defaultTo("unknown", errorData.resource)

        const messages = []
        if (errorResource != "book") {
            messages.push("El libro fue registrado correctamente")
        }
        messages.push(defaultTo("Se detectó un error desconocido", errorData.errorMessage))

        const operError = new OperationError(messages, errorResource)
        this.form().setOperationError(operError)
    }

    bookDataForServer() {
        const specBuilder = new SpecForServerBuilder(this.form().components())
            .addSimpleFields("isbn", "title", "year", "notes", "pages", "copyCount",
                "bookcase", "bookshelf", "subtitle", "collection", "originalYear", "isTranslation")
            .addField("authors", "authorNames").addField("publisher", "publisherName")
            .addField("genre", "genreName").addField("languages", "languageNames")
            .addField("originalLanguages", "originalLanguageNames")
        return specBuilder.specForServer()
    }

    bookSpecForServer() {
        // must return a FormData in order to (possibly) include an image
        const result = new FormData()

        // pour the book data into the FormData
        // must serialize it since FormData only accept String values
        const orig = this.bookDataForServer()
        result.append("structuredData", JSON.stringify(orig))

        // include the image
        const imageInfo = this.form().component("image").valueForServer()
        if (imageInfo.image && !imageInfo.mustDelete) {
            result.append("image", imageInfo.image)
        }
        return result
    }

    // doAction() abstract

    /* for eventual debugging purposes */
    logBookSpecForServer() {
        for (let pair of this.bookSpecForServer().entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
    }
}


export class AddBookOperation extends BookOperation {
    additionalSubmitAction(data) { this.form().props.additionalSubmitAction(data) }
    updateLists() { this.form().updateLists() }

    doAction() {
        const self = this
        this.startSaving()
        return Promise.resolve(axios.post('/booksWithImage', this.bookSpecForServer()))
            .then(function (response) {
                if (response.status >= 200 && response.status < 300) {
                    return Promise.all([response, self.updateLists(), self.additionalSubmitAction(response.data)])
                } else {
                    return Promise.all([Promise.resolve(response)])
                }
            })
            .spread((response, updateListsResponse, additionalSubmitActionResponse) => { 
                if (response.status == 200 || response.status == 201) {
                    self.endSaving() 
                } else {
                    self.informErrorFromResponse(response)
                }
            })
            .catch(errorInfo => { self.informErrorFromResponse(errorInfo.response) })
    }

    endSaving() {
        super.endSaving()
        this.form().shrinkExtraComponents()
    }
}


export class ModifyBookOperation extends BookOperation {

    bookSpecForServer() {
        const result = super.bookSpecForServer();
        const imageInfo = this.form().component("image").valueForServer();
        if (imageInfo.mustDelete) {
            result.append("deleteImage", "true");
        }
        return result;
    }

    doAction() {
        const self = this
        this.startSaving()
        axios.put('/books/' + this.bookId(), this.bookSpecForServer())
            .then(function () {
                self.form().page().saveInfoAboutOneEditedBook(bookId)
                self.endSaving()
                self.form().showListOfBooks()
            })
    }
}

export class DeleteBookOperation extends BookOperation {
    doAction() {
        this.startSaving();
        axios.delete('/books/' + this.bookId())
            .then(() => this.form().showListOfBooks());
    }
}

class BookLoanOperation extends BookOperation {
    doAction() {
        const self = this
        this.startSaving()
        return this.doSpecificAction().then(function (response) {
            self.endSaving()
            self.form().showListOfBooks()
        })
    }

    // doSpecificAction() abstract
}


export class LendBookOperation extends BookLoanOperation {
    doSpecificAction() {
        /* loanSpec: { bookId, borrowerName, startDate, nextAlarmDate } */
        const specForServer = new SpecForServerBuilder(this.form().loanComponents())
            .addField("borrower", "borrowerName").addField("loanStartDate", "startDate")
            .addField("loanNextAlarmDate", "nextAlarmDate")
            .specForServer()
        specForServer.bookId = this.bookId()
        return axios.post('/bookLoans', specForServer)
    }
}

export class ReturnBookOperation extends BookLoanOperation {
    doSpecificAction() {
        /* returnSpec: { returnDate } */
        return axios.put('/books/' + this.bookId() + '/return', {
            returnDate: this.form().loanComponent("loanReturnDate").valueForServer()
        })
    }
}





/****************************************************************
 *      Book operation toolbar
 ****************************************************************/
class BookOperationToolbar extends bookFormPart.BookFormPart {
    operation() { return this.form().operation() }
    showListOfBooks() { this.form().showListOfBooks() }
    isSaving() { return this.whatToNotify() === bookForm.notificationStates.saving }
    
    /*
    * Renderization
    */
    render() {
        return (
            <ButtonToolbar key="buttons" style={{ float: "right" }}>
                {this.buttons()}
            </ButtonToolbar>
        )
    }

    // buttons() abstract

    mustDisableBackToList() { return this.isSaving() }

    backToListOfBooksButton(buttonText) {
        return (<Button key="showList" disabled={ this.mustDisableBackToList() } onClick={() => this.showListOfBooks()}>
            { buttonText }
        </Button>)
    }
}


/*
 * props: bookForm, isLent
 */
export class ShowBookToolbar extends BookOperationToolbar {
    isAskingAboutDeletion() { return this.whatToNotify() === bookForm.notificationStates.aboutToDelete }
    mustDisableBackToList() { return super.mustDisableBackToList() || this.isAskingAboutDeletion() }
    mustDisableButtons() { return this.isAskingAboutDeletion() }

    buttons() {
        const prestarODevolver = this.props.isLent
            ? <Button key="return" disabled={this.mustDisableButtons()} onClick={() => this.returnBook()}>Se devolvió</Button>
            : <Button key="lend" disabled={this.mustDisableButtons()} onClick={() => this.lendBook()}>Prestar</Button>

        return ([
            this.backToListOfBooksButton("Volver"),
            prestarODevolver,
            <Button key="delete" disabled={this.mustDisableButtons()} onClick={() => this.deleteBook()} style={{ backgroundColor: "Plum" }}>
                Eliminar libro
            </Button>,
            <Button key="edit" disabled={this.mustDisableButtons()} onClick={() => this.editBook()} style={{ backgroundColor: "#ffff80" }}>
                { this.confirmButtonText() }
            </Button>
        ])
    }

    deleteBook() { 
        this.form().notify(bookForm.notificationStates.aboutToDelete);
    }
    editBook() { window.location.href = "/editBook/" + this.bookId() }
    lendBook() { window.location.href = "/lendBook/" + this.bookId() }
    returnBook() { window.location.href = "/returnLentBook/" + this.bookId() }
}


/*
 * superclass for AddBookToolbar and ModifyBookToolbar
 */
class EditBookToolbar extends BookOperationToolbar {
    showMessages() {
        this.inquirer().computeMessages()
        this.notify(bookForm.notificationStates.messages)
    }

    save() {
        this.inquirer().computeMessages()
        if (this.inquirer().hasIssues()) {
            this.notify(bookForm.notificationStates.messages)
        } else {
            this.operation().doAction()
        }
    }

    showMessageButton() {
        return (
            <Button key="messages" disabled={this.isSaving()} onClick={() => { this.showMessages() }}>
                Mostrar mensajes
            </Button>
        )
    }

    saveOperationButton() {
        return (
            <Button key="save" disabled={
                this.isSaving() ||
                (this.inquirer().hasIssues() && this.whatToNotify() == bookForm.notificationStates.messages)
            }
                onClick={() => this.save()} style={{ backgroundColor: "#5bd778" }}>
                {this.confirmButtonText()}
            </Button>
        )
    }
}


/*
 * props: bookForm
 */
export class AddBookToolbar extends EditBookToolbar {
    buttons() {
        return ([
            this.backToListOfBooksButton("Volver al listado"),
            <Button key="clean" disabled={this.isSaving()} onClick={() => { this.clearForm(); this.forceRedraw() }}>
                Limpiar datos
            </Button>,
            this.showMessageButton(),
            this.saveOperationButton()
        ])
    }
}


/*
 * props: bookForm
 */
export class ModifyBookToolbar extends EditBookToolbar {
    buttons() {
        return ([
            this.showMessageButton(),
            this.backToListOfBooksButton("Cancelar"),
            this.saveOperationButton()
        ])
    }
}


/*
 * toolbar for book loan related operations
 */
export class BookLoanOperationToolbar extends BookOperationToolbar {
    buttons() {
        return [
            this.backToListOfBooksButton("Cancelar"),
            <Button key="confirm" onClick={() => this.operation().doAction()} style={{ backgroundColor: "#5bd778" }}>
                {this.confirmButtonText()}
            </Button>
        ]
    }
}

/*
 * props: bookForm
 */
export class OperationErrorToolbar extends BookOperationToolbar {
    buttons() {
        return [ this.backToListOfBooksButton("Volver al listado") ]
    }
}


