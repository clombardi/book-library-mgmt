const React = require('react')
const ReactDOM = require('react-dom')
const axios = require('axios')
import { Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
const _ = require('lodash')

// import {default as ExcelFile, ExcelSheet, ExcelColumn} from 'react-data-export'

// const xlsx = require("xlsx-style")

import Button from 'react-bootstrap/lib/Button'
import Table from 'react-bootstrap/lib/Table'

const utils = require('../lib/utils')
const filterViewController = require('../lib/filterViewController')
const customFilters = require('./listBookFilters')
const paginationRow = require('../lib/paginationRow')


/*
 *  RemoteBookExporter
 *
 *  Expected prop: separateAuthorsLanguages
 ***************************************/
class RemoteBookExporter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return this.buildButton()
  }

  buildButton() {
    const text = this.props.separateAuthorsLanguages ? "Generar Excel (autores separados)" : "Generar Excel"
    const color = this.props.separateAuthorsLanguages ? "LightSkyBlue" : "PaleGreen"
    return (
      <Button onClick={() => this.generateAndDonwload()} style={{backgroundColor: color}} block>
        { text }
      </Button>
    )
  }

  generateAndDonwload() {
    const self = this
    axios
      .post('/fullBookXlsx', { separateMultipleValues: this.props.separateAuthorsLanguages })
      .then(function (response) { 
        const fileUrl = response.data.fileUrl
        const filename = response.data.filename
        self.downloadFile(fileUrl, filename)
      })
  }

  downloadFile(uri, name) {
    const link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
  }
}


/*
 *  Custom filter
 ***********************/
// class TextFilterForMultivaluedAttribute extends filters.TextFilter {
//   // seguir por aca, se supone que this.objectToCompare(serverData) es una lista (verificar)
//   // y que puedo usar this.serverStringSatisfies(serverString)
//   isSatisfiedBy(serverData) {
//     return this.objectToCompare(serverData).some(datum => this.serverStringSatisfies(datum))
//   }
// }


/*
 *  BookListFilterView
 ***********************/
class BookListFilterView extends filterViewController.FilterView {
    render() {
        return (
            <div className="row" style={{
                marginTop: "30px", marginBottom: "20px", backgroundColor: "LightCyan",
                paddingTop: "20px", paddingBottom: "20px"
            }}>
                <div className="col-sm-6" key="queryText">
                    {this.filter("queryText").editForm()}
                </div>
                <div className="col-sm-2" key="isLent">
                    {this.filter("isLent").editForm()}
                </div>
                <div className="col-sm-2" key="shouldBeReturned">
                    {this.filter("shouldBeReturned").editForm()}
                </div>
            </div>
        )
    }
}


/*
 *  BookItem
 ***********************/
class BookItem extends React.Component {
    constructor(props) {
        super(props);
    }

    showBookDetails() {
        this.props.rootComponent.saveNavigationState()
        window.location.href = "/showBook/" + this.props.book.persistentId
    }

    lendBook() {
        this.props.rootComponent.saveNavigationState()
        window.location.href = "/lendBook/" + this.props.book.persistentId
    }

    returnLentBook() {
        this.props.rootComponent.saveNavigationState()
        window.location.href = "/returnLentBook/" + this.props.book.persistentId
    }

    render() {
        const self = this
        let rowStyle = {}
        let textStyle = {}
        if (this.props.book.isLent) {
        // rowStyle.backgroundColor = "#e7fee7"
        rowStyle.backgroundColor = "#ffffe6"
        textStyle.color = "Teal"
        }
        return (
        <tr key={self.props.book.persistentId} style={rowStyle}>
            <td style={textStyle}>{self.props.book.title}</td>
            <td style={textStyle}>{_.join(self.props.book.authors, ", ")}</td>
            <td style={textStyle}>{self.props.book.borrower}</td>
            <td>
            <Button bsSize="small" onClick={() => self.showBookDetails()} 
                style={{backgroundColor: "LightSkyBlue", marginRight: "20px"}}>
                Ver ficha
            </Button>
            { this.prestarODevolver() }
            </td>
        </tr>
        )
    }

    prestarODevolver() {
        return this.props.book.isLent 
        ? (
            <Button bsSize="small" onClick={() => this.returnLentBook()} 
                style={{ backgroundColor: "MediumSpringGreen", color: "GhostWhite" }}>
            Se devolvió
            </Button>
        )
        : (
            <Button bsSize="small" onClick={() => this.lendBook()} 
                style={{ backgroundColor: "MediumSeaGreen", color: "GhostWhite" }}>
            Prestar
            </Button>
        )
    }  
}


/*
 *  BookListPage
 ***********************/
class BookListPage extends React.Component {
    constructor(props) {
        super(props);
        this._filterController = new filterViewController.FilterController(this)
        this._filters = this.buildFilters()
        this._currentPage = 1
        this._pageSize = null
        this._totalBookCount = null
        this._pageCount = null
        // bookList is in **both** object state and React state
        // rationale: bookList could be loaded twice, since if we are getting back from book CRU,
        // we must first fetch the current page, and eventually switch to the first page
        this._bookList = []         
        this.state = { bookList: [] }
    }

    // actions
    componentDidMount() {
        // synchronous operations
        this.restoreCurrentPage()
        this.restoreFilterState()
        // asynchronous operations
        return this.fetchPageSize()
            .then(() => this.fetchBookList())
            .then(() => this.adjustBookListBecauseOfBookEdition())
            .then(() => this.synchronizeBookList())
    }

    fetchPageSize() {
        return axios
            .get('/conf/defaultPageSize')
            .then(response => this._pageSize = response.data.pageSize)
    }

    fetchPage(pageNumber) {
        // synchronous operations
        this.silentSetCurrentPage(pageNumber)
        // asynchronous operations
        return this.fetchBookList().then(() => this.synchronizeBookList())
    }

    /*
     * fetches the current book list in the object state, does not affect React state
     */
    fetchBookList() {
        let querySpec = _.fromPairs(this.activeFilters().map(filter => [filter.nameForServer(), filter.valueForServer()]))
        let searchBody = { 
            referenceDate: utils.todayAsServerTime(), 
            indexRange: {
                from: (this.currentPage() - 1) * this.pageSize(), 
                to: this.currentPage() * this.pageSize()
            },
            query: querySpec
        }
        return axios
            .post('/books/search', searchBody)
            .then(response => {
                // 2019.11.10 - books are given already sorted by the backend
                const sortedBooks = response.data.data
                this._bookList = sortedBooks
                this._totalBookCount = response.data.totalCount
                this._pageCount = Math.floor((this._totalBookCount - 1) / this.pageSize()) + 1 
                if (response.data.firstIndex === 0) { this.silentSetCurrentPage(1) }
                return Promise.resolve(sortedBooks)
            })
        // const theSelectedBooks = this.state.completeBookList.filter(book => this.satisfiesFilters(book))
        // this.setState({ bookList: theSelectedBooks })
    }

    /*
     * updates the current book list in React state, given that it is OK in object state
     */
    synchronizeBookList() {
        this.setState({ bookList: this._bookList })
    }

    /*
     * fetches the current book list, updates both object state and React state
     */
    recomputeList() {
        return this.fetchBookList().then(() => this.synchronizeBookList())
    }

    /*
     * Recomputes book list to point at page 1 if the lastly edited book is not in the current page,
     * or if more than one book was edited in the last book edition session.
     * In this case, also clears filters.
     * Expect this._bookList set, and updates it if needed.
     * Does not update React state.
     */
    adjustBookListBecauseOfBookEdition() {
        let mustRemoveFilters = false
        const editedBookCountString = utils.sessionStorageItemNullified("editedBookCount")
        const lastEditedBookId = utils.sessionStorageItemNullified("lastEditedBookId")
        sessionStorage.removeItem("editedBookCount")
        sessionStorage.removeItem("lastEditedBookId")

        if (editedBookCountString) {
            const editedBookCount = Number(editedBookCountString)
            if (editedBookCount > 1) { mustRemoveFilters = true }
        }
        if (!mustRemoveFilters && lastEditedBookId) {
            const theBook = this._bookList.find(book => book.persistentId == lastEditedBookId)
            if (!theBook) { mustRemoveFilters = true }
        }

        if (mustRemoveFilters) {
            this.filterController().clearFilterState()
            this.filterController().clearFilters()
            this.silentSetCurrentPage(1)
            return this.fetchBookList()
        } else {
            return Promise.resolve()
        }
    }


    // redirects
    // ---------------------------------------------------
    showAddBookForm() {
        this.saveNavigationState()
        window.location.href = "/addBooks" 
    }


    // redirect-related actions
    // ---------------------------------------------------
    restoreFilterState() {
        if (sessionStorage.getItem("lastPage") == "bookPage") {
            this.filterController().restoreFilterState()
        }
        sessionStorage.removeItem("lastPage")
        return Promise.resolve()
    }

    saveNavigationState() {
        this.filterController().saveFilterState()
        this.saveCurrentPage()
    }

    saveCurrentPage() {
        sessionStorage.setItem("currentPage", String(this.currentPage()))
    }

    restoreCurrentPage() {
        const thePage = sessionStorage.getItem("currentPage")
        if (thePage) { this.silentSetCurrentPage(Number(thePage)) }
    }


    // data
    // ---------------------------------------------------
    currentPage() { return this._currentPage }
    silentSetCurrentPage(pageNumber) { this._currentPage = pageNumber }
    pageSize() { return _.defaultTo(this._pageSize, 50) }
    pageCount() { return this._pageCount }


    // filters
    // ---------------------------------------------------
    filterController() { return this._filterController }
    filters() { return this._filters }
    filter(id) { return this.filters().find(filter => filter.filterId() == id) }
    activeFilters() { return this.filters().filter(aFilter => aFilter.isActive()) }

    buildFilters() {
        return [
            new customFilters.BasicTextFilter(
                "queryText", "Búsqueda de texto", this._filterController
            ).beAutofocus(),
            new customFilters.IsLentFilter(
                "isLent", "Está prestado", this._filterController
            ),
            new customFilters.ShouldBeReturnedFilter(
                "shouldBeReturned", "Se puede reclamar", this._filterController
            ).setTooltipText('Está prestado, y la fecha de devolución prevista en el préstamo ya pasó.')
        ]
    }

    satisfiesFilters(book) { return this.activeFilters().every(aFilter => aFilter.isSatisfiedBy(book)) }


    // rendering
    // ---------------------------------------------------
    render() {
        const self = this
        return (
            <div className="container">
                <div className="panel panel-info">
                    <div className="panel-heading">
                        <h3>Libros</h3>
                    </div>
                    <div className="panel-body">
                        <div className="well" style={{margin: "0px", backgroundColor: "#ffffff"}}>
                            <div className="row">
                                <div className="col-sm-12">
                                    <Button style={{backgroundColor: "#e6f7ff"}} block 
                                        onClick={() => self.showAddBookForm()}
                                    >
                                        Agregar libro
                                    </Button>
                                </div>
                            </div>
                            <div className="row" style={{ marginTop: "30px", marginBottom: "20px"}}>
                                <div className="col-sm-6">
                                    <div style={{marginLeft: "30px"}}>
                                        <RemoteBookExporter separateAuthorsLanguages={ false }/>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div style={{ marginRight: "30px" }}>
                                        <RemoteBookExporter separateAuthorsLanguages={true} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <BookListFilterView 
                            page={this} 
                            ref={ (componentRef) => self.filterController().setView(componentRef) }    
                        />
                        <Table striped bordered>
                            <thead>
                                <tr>
                                    <th>Título</th>
                                    <th>Autores</th>
                                    <th style={{ minWidth:"120px" }}>Prestado a</th>
                                    <th style={{ minWidth:"220px" }}></th>
                                </tr>
                            </thead>
                            <tbody>
                                { self.state.bookList.map(function(book) {
                                    return <BookItem book={book} key={book.persistentId} rootComponent={self}/>
                                  }) 
                                }
                            </tbody>
                        </Table>
                        <paginationRow.PaginationRow 
                            listComponent={this} pageCount={this.pageCount()} currentPage={this.currentPage()}
                            domain="libros" 
                            filterApplies={this.activeFilters().length > 0}
                        />
                    </div>
                </div>
            </div>
        )
    }

/*
    <filterComponents.FilterSelector
        pageComponent={this}
        ref={(componentRef) => self.setFilterSelector(componentRef)}
    />
 */
    
}

const TouchedAlertTemplate = ({ message, options, style, close }) => {
    return AlertTemplate({message, options, style: Object.assign(style, {width: 'auto'}), close})
}

ReactDOM.render(
    <Provider template={TouchedAlertTemplate} offset='80px'>
        <BookListPage />
    </Provider>
    ,
    document.getElementById('reactPage')
);
