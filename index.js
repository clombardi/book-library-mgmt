/*
Useful data

Deploys
- casa: https://damp-temple-13815.herokuapp.com/listBooks
- Gimerrague: https://bibliography-card-app-dwbrxxnwxb.now.sh
- show: https://bibliography-card-app-kuafpocrmo.now.sh
- laraPorCaro: https://bibliography-card-app-jmfwhheqlo.now.sh
- javi: https://bibliography-card-app-vxdvhcnfio.now.sh
- ceefe: https://bibliography-card-app-vzhsnfmamu.now.sh 
- charlyBlueAlba: https://bibliography-card-app-lxxfdzrwrp.now.sh 

Firebase config data
  var config = {
    apiKey: "AIzaSyBxUQT0jBauqPzedBs7aFPPyIhFU0Q6LbQ",
    authDomain: "biblapp-ff6b8.firebaseapp.com",
    databaseURL: "https://biblapp-ff6b8.firebaseio.com",
    storageBucket: "biblapp-ff6b8.appspot.com",
    messagingSenderId: "911795853268"
  };
*/


// index.js
const path = require('path')  
const bodyParser = require("body-parser");
const express = require('express')  
const exphbs = require('express-handlebars')
const _ = require('lodash');
const config = require('config');

const app = express()
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '/public')));

app.engine('.hbs', exphbs({  
  defaultLayout: 'simplePage',
  extname: '.hbs',
  layoutsDir: path.join(__dirname, 'views/layout')
}))
app.set('view engine', '.hbs')  
app.set('views', path.join(__dirname, 'views/body'))  

// multer configuration
const multer = require("multer")
const upload = multer({ storage: multer.memoryStorage() })

// Firebase configuration 
const firebase = require("firebase");
const firebaseConfig = {
    apiKey: "AIzaSyBxUQT0jBauqPzedBs7aFPPyIhFU0Q6LbQ",
    authDomain: "biblapp-ff6b8.firebaseapp.com",
    databaseURL: "https://biblapp-ff6b8.firebaseio.com",
    storageBucket: "biblapp-ff6b8.appspot.com"
};
firebase.initializeApp(firebaseConfig);
const database = firebase.database();
// const storage = firebase.storage(); does not work, since Firebase does not include Storage for Node.js server


// book model initiialization
const appModel = require('./app/appModel')
appModel.storeCenter.setDatabase(database)
appModel.storeCenter.changeDatabaseDomainTo('casa')
appModel.storeCenter.init()
appModel.storeCenter.readFromDisk(false)
console.log('after appModel.storeCenter.readFromDisk()')

/*
 * additional imports
 */
const databaseCopy = require('./app/databaseCopy')
const misc = require('./lib/misc')
const bookSearch = require('./app/bookSearch/bookSearch')
const bookApi = require("./app/api/backend/bookApi") 
const smallEntityApi = require("./app/api/backend/smallEntitiesApi")
const bookLoanApi = require("./app/api/backend/bookLoanApi")

// constants for small code to inject into the html pages
const fixTypeaheadCssClass = ".rbt .dropdown-menu-justify { right: 0; }"


/* 
    bibliography app interacting with React and Bootstrap -- public URLs 
 */
app.get('/addBooks', (request, response) => { 
  const pageActionsCode = "const pageActions = {add: 1, edit: 2, show: 3}"
  const thisPageActionCode = "const pageAction = pageActions.add "
  
  response.render("simpleReactPageBody", {
    titlePage: "Agregar libros",
    sourceFile: "book/bookPage_build",
    injectedJsCode: _.join([pageActionsCode, thisPageActionCode], "\n"),
    injectedCssCode: fixTypeaheadCssClass,
    additionalBodyLibs: ""
  })  
})

app.get('/showBook/:bookId', (request, response) => { 
  const pageActionsCode = "const pageActions = {add: 1, edit: 2, show: 3}"
  const thisPageActionCode = "const pageAction = pageActions.show "
  const bookIdCode = "const bookId = \"" + request.params.bookId + "\""

  response.render("simpleReactPageBody", {
    titlePage: "Libro",
    sourceFile: "book/bookPage_build",
    injectedJsCode: _.join([pageActionsCode, thisPageActionCode, bookIdCode], "\n"),
    injectedCssCode: "",
    additionalBodyLibs: ""
  })  
})

app.get('/editBook/:bookId', (request, response) => { 
  const pageActionsCode = "const pageActions = {add: 1, edit: 2, show: 3}"
  const thisPageActionCode = "const pageAction = pageActions.edit "
  const bookIdCode = "const bookId = \"" + request.params.bookId + "\""

  response.render("simpleReactPageBody", {
    titlePage: "Modificar libro",
    sourceFile: "book/bookPage_build",
    injectedJsCode: _.join([pageActionsCode, thisPageActionCode, bookIdCode], "\n"),
    injectedCssCode: fixTypeaheadCssClass,
    additionalBodyLibs: ""
  })  
})

app.get('/lendBook/:bookId', (request, response) => {
  const bookIdCode = "const bookId = \"" + request.params.bookId + "\""
  const action = "const actionToPerform = \"lendBook\""

  response.render("simpleReactPageBody", {
    titlePage: "Préstamo de libro",
    sourceFile: "book/loanBookPage_build",
    injectedJsCode: _.join([bookIdCode, action], "\n"),
    injectedCssCode: fixTypeaheadCssClass,
    additionalBodyLibs: ""
  })
})

app.get('/returnLentBook/:bookId', (request, response) => {
  const bookIdCode = "const bookId = \"" + request.params.bookId + "\""
  const action = "const actionToPerform = \"returnLentBook\""

  response.render("simpleReactPageBody", {
    titlePage: "Devolución de libro",
    sourceFile: "book/loanBookPage_build",
    injectedJsCode: _.join([bookIdCode, action], "\n"),
    injectedCssCode: fixTypeaheadCssClass,
    additionalBodyLibs: ""
  })
})

app.get('/listBooks', (request, response) => {
  response.render("simpleReactPageBody", {
    titlePage: "Ver libros",
    sourceFile: "book/listBooks_build",
    injectedJsCode: "",
    injectedCssCode: "",
    additionalBodyLibs: ""
  })  
})

app.get('/testPage', (request, response) => { 
  response.render("simpleReactPageBody", {
    titlePage: "Página de prueba",
    sourceFile: "test/testPage_build",
    injectedJsCode: "",
    injectedCssCode: fixTypeaheadCssClass,
    additionalBodyLibs: ""
  })  
})

app.get('/fixedTestPage', (request, response) => {
  response.send(
    "<html>" +
    "<head><title>Pagina estatiquisima</title></head>" +
    "<body>" + 
    "<p>" +
    "  <button onclick='setShortLifeCookie()'>Set 15-sec Cookie</button>" +
    "  <button style='margin-left: 20px' onclick='setLongLifeCookie()'>Set 10-min Cookie</button>" +
    "</p>" +
    "<p style='margin-top: 20px'>" +
    "  <button onclick='showFullCookie()'>Show complete cookie string</button>" +
    "  <span style='margin-left: 30px' id='completeCookieString'></span>" +
    "</p>" +
    "<p style='margin-top: 20px'>" +
    "  <button onclick='showEachCookie()'>Show each cookie value</button>" +
    "  <span style='margin-left: 30px' id='shortLifeCookieStatus'></span>" +
    "  <span style='margin-left: 30px' id='longLifeCookieStatus'></span>" +
    "</p>" +
    '<script>' + 
    '  function setShortLifeCookie() {' + 
    '    document.cookie = "shortLiveCookie=true;max-age=15";' + 
    ' };' + 
    '  function setLongLifeCookie() {' + 
    '    document.cookie = "longLiveCookie=true;max-age=600";' + 
    '  };' + 
    '  function showFullCookie() {' + 
    '    document.getElementById("completeCookieString").innerHTML = document.cookie;' + 
    '  };' + 
    '  function showEachCookie() {' +
    '    document.getElementById("shortLifeCookieStatus").innerHTML = ' +
    '        "15-sec cookie: " + (valueForKey(document.cookie, "shortLiveCookie") ? "set" : "not set") ;' +
    '    document.getElementById("longLifeCookieStatus").innerHTML = ' +
    '        "10-min cookie: " + (valueForKey(document.cookie, "longLiveCookie") ? "set" : "not set") ;' +
    '  };' + 
    'function comps(cok) { return cok.split(";"); };' + 
    'function decompose(cok) { ' + 
    '  if (!cok.length) { return [] }' + 
    '  return comps(cok).map(rawComp => {' + 
    '    let kv = rawComp.split("=");' + 
    '    return { key: kv[0].trim(), value: kv[1].trim() }' + 
    '  }) ' + 
    '};' + 
    'function valueForKey(cok, theKey) {' + 
    '  let kvs = decompose(cok);' + 
    '  let theComp = kvs.find(kv => kv.key == theKey);' + 
    '  return theComp ? theComp.value : null' + 
    '};' + 
    '</script>' + 
    "</body>" +
    "<html>")
})


/* 
   bibliography app interacting with React and Bootstrap -- services 
 */
bookApi.appendBookRetrievalEndpoints(app)
bookApi.appendBookOperationEndpoints(app)
smallEntityApi.appendSmallEntityRetrievalEndpoints(app)
bookLoanApi.appendBookLoanOperationEndpoints(app)

/*
  misc
 */
app.get('/lastlyEditedBook', (request, response) => {
    appModel.bookStore.fetchLastlyEditedBook().then( function (book) {
        response.json(book ? book.basicUIJSON() : null) 
    })
})

/*
  config data
 */
app.get('/conf/defaultPageSize', (request, response) => {
  response.json({ pageSize: config.get('defaultPageSize') })
})

/*
  search for book info given an ISBN number
 */
app.get('/bookInfoByIsbn/:isbn', function (request, response) {
    bookSearch.fetchBookInfoByIsbnFromHtmlPage(request.params.isbn)
    .then((bookInfo) => {
        response.status(200)
        response.json(bookInfo)        
    })
    .catch((error) => {
        response.status(error.isExternalError ? 500 : 404)
        console.log(error)
        response.json({ message: error.message })
    })
})


/*
  Create a spreadsheet with book info and upload it to Google Cloud Services
 */
const excel = require('./app/excelDownload')

app.post('/fullBookXlsx', (request, response) => {
  const generator = new excel.BookExcelJsFileGenerator(
    (theGenerator) => {
      if (theGenerator.hasErrors()) {
        response.status(500)
        response.json({ 'errorMessage': 'error en la generacion' })
      } else {
        response.status(200)
        response.json({
          'filename': theGenerator.filename(),
          'fileUrl': theGenerator.generatedFileUrl()
        })
      }
    }, (request.body.separateMultipleValues == "true" || request.body.separateMultipleValues == true)
  )
  generator.generate()
})  


/* 
  Copy of the database in the same Firebase account
 */

app.post('/makeCopy', function(request, response) {
  databaseCopy.copyNamedEntityStore(appModel.publisherStore, 'testCopy')
    .then(() => databaseCopy.copyNamedEntityStore(appModel.languageStore, 'testCopy'))
    .then(() => databaseCopy.copyNamedEntityStore(appModel.genreStore, 'testCopy'))
    .then(() => databaseCopy.copyNamedEntityStore(appModel.authorStore, 'testCopy'))
    .then(() => databaseCopy.copyBooks('testCopy'))
    .then(() => databaseCopy.copyLastlyEditedBookId('testCopy'))
    .then(() => response.json({ 'resultado': 'datos guardados en testCopy' }))    
})


/* 
  Image management
 */

const fileProcesses = require('./app/fileProcesses')

app.get('/imageExists/:filename', (request, response) => {
    new fileProcesses.FileDownloader("bookImages", request.params.filename, ["jpg", "png"])
        .fetchFileExists()
        .then(fileExists => { 
            response.status(200)
            response.json({ exists: fileExists }) 
        })
})

app.get('/imageContents/:filename', (request, response) => {
    const downloader = new fileProcesses.FileDownloader("bookImages", request.params.filename, ["jpg", "png"])
    downloader.fetchBinaryDownload()
    .then(downloadResponse => {
      response.status(200)
      response.type('image/' + downloadResponse.extension)
      response.send(downloadResponse.contents)
    })
    .catch( err => {
      console.log(err)
      response.status(500)
      response.json({ errorMessage: "Problemas al cargar la imagen" })
    })
})

app.post('/bookImage', upload.single("fileContents"), (request, response) => {
    const generator = new fileProcesses.ImageFileGenerator(
        request.file.buffer, request.body.filename, request.file.mimetype,
        (theGenerator) => {
            if (theGenerator.hasErrors()) {
                response.status(500)
                response.json({ 'errorMessage': 'error en el registro de la imagen' })
            } else {
                response.status(200)
                response.json({ 'fileUrl': theGenerator.generatedFileUrl() })
            }
        }
    )
    generator.setFolder("bookImages")
    generator.generate()
})


/* 
  Tests for file processes 
 */

 app.get('/saveTextFile/:text', (request, response) => {
  const generator = new fileProcesses.SimpleTextFileGenerator(
    request.params.text,
    (theGenerator) => response.json({ 'resultado': 'string guardado en ' + theGenerator.filename() })
  )
  generator.generate()
})  

app.get('/saveTextFileUsingBuffer/:text', (request, response) => {
  const generator = new fileProcesses.SimpleTextFromBufferFileGenerator(
    request.params.text,
    (theGenerator) => { 
      if (theGenerator.hasErrors()) {
        response.status(500)
        response.json({ 'errorMessage': 'error en la generacion' })
      } else {
        response.status(200)
        response.json({ 'resultado': 'string guardado en ' + theGenerator.filename() })
      }
    }
  )
  generator.generate()
})  

/*
  Test to create a spreadsheet using xlsx, and save it to google cloud
 */
app.post('/exampleSpreadsheet', (request, response) => {
  const generator = new excel.ExampleExcelJsFileGenerator(
    (theGenerator) => {
      if (theGenerator.hasErrors()) {
        response.status(500)
        response.json({ 'errorMessage': 'error en la generacion' })
      } else {
        response.status(200)
        response.json({ 
          'filename': theGenerator.filename(), 
          'fileUrl': theGenerator.generatedFileUrl()
        })
      }
    }
  )
  generator.generate()
})  


/* 
  Information test service
 */
app.get('/firstBookInfo', function (request, response) {
  let theBook = appModel.bookStore.books()[0]
  let theJson = theBook.persistenceJSON()
  Object.keys(theJson).forEach(key => { if (theJson[key] == undefined) { delete theJson[key] } })
  response.json( {
    name: theBook.title(), 
    persistentId: theBook.persistentId(),
    keys: Object.keys(theJson),
    rawAuthors: theBook.rawAuthors(),
    rawLanguages: theBook.rawLanguages(),
    rawOriginalLanguages: theBook.rawOriginalLanguages(),
    info: theJson
  } )
})

app.listen(process.env.PORT || 3000, null, null, () => console.log("Book app started"))


